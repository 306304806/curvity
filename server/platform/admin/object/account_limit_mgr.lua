--account_limit_mgr.lua
local otime                 = os.time
local log_err               = logger.err
local log_debug             = logger.debug
local tmapv2array           = table_ext.mapv2array
local tarray2map            = table_ext.array2map

local account_limit_dao     = quanta.get("account_limit_dao")
local timer_mgr             = quanta.get("timer_mgr")

local DBLoading             = enum("DBLoading")
local PeriodTime            = enum("PeriodTime")

local AccountLimitMgr = singleton()
local prop = property(AccountLimitMgr)
prop:accessor("data_loading", DBLoading.INIT)
prop:accessor("limit_data", {})
prop:accessor("indexs", {})
prop:accessor("dirty_key_map", {})

function AccountLimitMgr:__init()
    self:setup()
end

-- 初始化函数
function AccountLimitMgr:setup()
    -- 延迟从数据库加载排行数据
    self.load_timer = timer_mgr:loop(PeriodTime.SECOND_5_MS, function()
        self:load_account_limit_data()
    end)

    -- 定时检查存储排行索引数据
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:update_data_2db()
    end)
end

-- 序列化为db数据
local function db_data_serialize(data)
    local db_data = {}
    db_data.open_id = data.open_id
    db_data.reason = data.reason
    db_data.lock_time = data.lock_time
    db_data.players = tmapv2array(data.players)
    return db_data
end

-- 序列化为内存数据
local function db_data_deserialize(db_data)
    local data = {}
    data.open_id = data.open_id
    data.reason = data.reason
    data.lock_time = data.lock_time
    data.players = tarray2map(data.players)
    return data
end

-- 检查时间是否生效
local function check_valid_time(time)
    if not time or time == 0 then
        return false
    end

    if time == - 1 then
        return true
    end

    return time >= otime()
end

-- 生成有效时间
local function build_valid_time(time)
    if time > 0 then
        return otime() + time
    end

    return time
end

-- 从数据库加载数据
function AccountLimitMgr:load_account_limit_data()
    log_debug("[AccountLimitMgr][load_account_limit_data]")
    if self.data_loading ~= DBLoading.INIT then
        return
    end
    self.data_loading = DBLoading.LOADING
    local ok, result = account_limit_dao:load_account_limit()
    if not ok then
        self.data_loading = DBLoading.INIT
        log_err("[AccountLimitMgr][load_account_limit_data]->load failed!")
        return
    end

    self.data_loading = DBLoading.SUCCESS
    for open_id, account in pairs(result) do
        self.limit_data[open_id] = db_data_deserialize(account)
        for player_id, _ in pairs(self.limit_data[open_id].players) do
            self.indexs[player_id] = open_id
        end
    end
    timer_mgr:unregister(self.load_timer)

    return true
end

-- 数据落地
function AccountLimitMgr:update_data_2db()
    if self.data_loading ~= DBLoading.SUCCESS then
        return
    end
    for open_id, _ in pairs(self.dirty_key_map) do
        local data = self.limit_data[open_id]
        if not data then
            local ok = account_limit_dao:delete_account_limit(open_id)
            if ok then
                self.dirty_key_map[open_id] = nil
            end
        else
            local ok = account_limit_dao:update_account_limit(open_id, db_data_serialize(data))
            if ok then
                self.dirty_key_map[open_id] = nil
            end
        end
    end
end

-- 封/解封账号
function AccountLimitMgr:lock_account(open_id, time, reason)
    if self.data_loading ~= DBLoading.SUCCESS then
        logger("[AccountLimitMgr][lock_account] open_id = %s, reason = %s", open_id, reason)
        return false
    end

    time = build_valid_time(time)
    local account = self.limit_data[open_id] or {open_id = open_id, players = {}}
    -- 解封没被封的账号
    if not check_valid_time(time) and not check_valid_time(account.lock_time) then
        return false
    end
    account.reason = reason
    account.lock_time = time
    self.limit_data[open_id] = account
    self.dirty_key_map[open_id] = true
    self:check_vaild_limit(open_id)
    return true
end

-- 封/解封角色
function AccountLimitMgr:lock_player(open_id, player_id, time, reason)
    if self.data_loading ~= DBLoading.SUCCESS then
        logger("[AccountLimitMgr][lock_player] open_id = %s, player_id, time = %s, reason = %s",
            open_id, player_id, time, reason)
        return false
    end

    time = build_valid_time(time)
    local account = self.limit_data[open_id] or {open_id = open_id, players = {}}
    local player = account.players[player_id] or {key = player_id}
    -- 解封没被封的角色
    if not check_valid_time(time) and not check_valid_time(player.lock_time) then
        return false
    end
    player.lock_time = time
    player.reason = reason
    account.players[player_id] = player
    self.limit_data[open_id] = account
    self.indexs[player_id] = open_id
    self.dirty_key_map[open_id] = true
    self:check_vaild_limit(open_id)
    return true
end

function AccountLimitMgr:ban_player_chat(open_id, player_id, time, reason)
    if self.data_loading ~= DBLoading.SUCCESS then
        logger("[AccountLimitMgr][ban_player_chat] open_id = %s, player_id, time = %s, reason = %s",
            open_id, player_id, time, reason)
        return false
    end

    time = build_valid_time(time)
    local account = self.limit_data[open_id] or {open_id = open_id, players = {}}
    local player = account.players[player_id] or {key = player_id}
    -- 解禁没被禁言的角色
    if not check_valid_time(time) and not check_valid_time(player.ban_chat_time) then
        return false
    end
    player.ban_chat_time = time
    player.ban_chat_reason = reason
    account.players[player_id] = player
    self.limit_data[open_id] = account
    self.indexs[player_id] = open_id
    self.dirty_key_map[open_id] = true
    self:check_vaild_limit(open_id)
    return true
end

-- 检查账号限制信息是否有效
function AccountLimitMgr:check_vaild_limit(open_id)
    local account = self.limit_data[open_id]
    if check_valid_time(account.lock_time) then
        return
    end
    for _, player in pairs(account.players) do
        if check_valid_time(player.lock_time) or check_valid_time(player.ban_chat_time) then
            return
        end
    end
    self.limit_data[open_id] = nil
end

function AccountLimitMgr:account_is_lock(open_id)
    local account = self.limit_data[open_id]
    if not account then
        return false
    end
    return check_valid_time(account.lock_time)
end

function AccountLimitMgr:player_is_lock(open_id, player_id)
    local account = self.limit_data[open_id]
    if not account then
        return false
    end
    if check_valid_time(account.lock_time) then
        return true
    end
    local player = account.players[player_id]
    if not player then
        return false
    end
    return check_valid_time(player.lock_time)
end

function AccountLimitMgr:is_ban_chat(player_id)
    local open_id = self.indexs[player_id]
    if not open_id then
        return false
    end

    local account = self.limit_data[open_id]
    if not account then
        return false
    end

    local player = account.players and account.players[player_id]
    if not player then
        return false
    end

    return check_valid_time(player.ban_chat_time)
end

function AccountLimitMgr:get_account_limit(open_id)
    return self.limit_data[open_id]
end

-- export
quanta.account_limit_mgr = AccountLimitMgr()

return AccountLimitMgr
