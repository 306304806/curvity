--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_game_friend_acts_klbq.lua
--好友事件流水
--luacheck: ignore 631

local sformat    = string.format
local log_err    = logger.err
local DlogGameFriendActsKlbq = class()
local prop = property(DlogGameFriendActsKlbq)
prop:accessor("table_name", "dlog_game_friend_acts_klbq")

--!!!以下是公共字段，调用时自动填充!!!
prop:accessor("has_public", true)
--[[(公共字段) 格式：YYYY-MM-DD HH:MM:SS (必填)]]
prop:accessor("event_time", "")
--[[(公共字段) 游戏申请的乐逗SDK APPKEY (必填)]]
prop:accessor("game_appkey", "")
--[[(公共字段) 游戏全局唯一区服ID (必填)]]
prop:accessor("zone_id", "")
--[[(公共字段) 对外玩家Id(注：数据来自乐逗SDK中的对外OpenId)，也要兼容非乐逗体系用户id (必填)]]
prop:accessor("open_id", "")
--[[(公共字段) SDK设备ID（SDK端可获取） (必填)]]
prop:accessor("sdk_device_id", "")
--[[(公共字段) android=1、ios=2、h5=3，pc=4 (必填)]]
prop:accessor("platform", "")
--[[(公共字段) 比如：华为、OPPO、VIVO、应用宝等,接了乐逗SDK则必须是从乐逗SDK获取的渠道商ID (必填)]]
prop:accessor("channel_group_id", "")
--[[(公共字段) 各渠道下的子渠道ID，比如华为1包、华为2包…接了乐逗SDK则必须是从乐逗SDK获取的channel_id (必填)]]
prop:accessor("channel_id", "")
--[[(公共字段) 游戏版本号 (必填)]]
prop:accessor("game_version", "")
--[[(公共字段) 全局唯一 (必填)]]
prop:accessor("character_id", "")
--[[(公共字段) 角色等级 (必填)]]
prop:accessor("character_level", "")
--[[(公共字段) 当前用户VIP等级 (必填)]]
prop:accessor("vip_level", "")


--!!!以下是特有字段，需调用方填充ctx!!!
--[[(独立字段) 1-成功添加，2-添加申请，3-删除，4-赠送金币 (必填)]]
prop:accessor("opt_type", "")
--[[(独立字段) 好友角色ID (必填)]]
prop:accessor("friends_character_id", "")
--[[(独立字段) 好友角色名称 (必填)]]
prop:accessor("friends_character_name", "")
--[[(独立字段) 好友角色等级 (必填)]]
prop:accessor("friends_character_level", "")
--[[(独立字段) (当前好友列表人数） (必填)]]
prop:accessor("friends_num", "")

function DlogGameFriendActsKlbq:__init()
    self.special_fields = { ["opt_type"] = true, ["friends_character_id"] = true, ["friends_character_name"] = true, ["friends_character_level"] = true, ["friends_num"] = true,}

    self.filter_fields = { ["friends_character_name"] = true,}

end

function DlogGameFriendActsKlbq:contruct_report_str(ctx)
    if not ctx then
        log_err("DlogGameFriendActsKlbq report param error!")
        return
    end

    for field_name in pairs(self.special_fields) do
        if self.filter_fields and self.filter_fields[field_name] then
            self[field_name] = string.gsub(ctx.special_fields[field_name] or "", "|", "*")
        else
            self[field_name] = ctx.special_fields[field_name] or ""
        end
    end

    return sformat("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s", self.table_name,  self.event_time, self.game_appkey, self.zone_id, self.open_id, self.sdk_device_id, self.platform, self.channel_group_id, self.channel_id, self.game_version, self.character_id, self.character_level, self.vip_level, self.opt_type, self.friends_character_id, self.friends_character_name, self.friends_character_level, self.friends_num)
end

return DlogGameFriendActsKlbq