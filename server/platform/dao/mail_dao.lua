--mail_dao.lua
local log_err       = logger.err
local check_failed  = utility.check_failed

local DBGroup       = enum("DBGroup")
local PeriodTime    = enum("PeriodTime")

local db_agent      = quanta.get("db_agent")

local MailDao = singleton()
function MailDao:__init()
end

-- 加载全局邮件
function MailDao:load_global_mails(area_id)
    --默认全局邮件最大保存4周
    local expire_time = quanta.now - PeriodTime.WEEK_S * 4
    local query = {"plat_global_mail", { send_time={ ["$gt"] = expire_time }}, {_id = 0}}
    local ok, code, gmails = db_agent:find(area_id, query, DBGroup.GLOBAL, 1)
    if not ok or check_failed(code) then
        log_err("[MailDao][load_global_mails] failed: ok:%s, code: %s, gmails: %s", ok, code, gmails)
        return false
    end
    return true, gmails
end

-- 插入全局邮件
function MailDao:insert_global_mail(area_id, gmail)
    local query = {"plat_global_mail", gmail }
    local ok, code, res = db_agent:insert(area_id, query, DBGroup.GLOBAL, 1)
    if not ok or check_failed(code) then
        log_err("[MailDao][insert_global_mail] failed: ok:%s code: %s, res: %s", ok, code, res)
        return false
    end
    return true
end

-- 删除全局邮件
function MailDao:delete_global_mail(area_id, uuid)
    local query = {"plat_global_mail", { uuid = uuid }}
    local ok, code, res = db_agent:delete(area_id, query, DBGroup.GLOBAL, 1)
    if not ok or check_failed(code) then
        log_err("[MailDao][delete_global_mail] failed: ok:%s code: %s, res: %s", ok, code, res)
        return false
    end
    return true
end

quanta.mail_dao = MailDao()

return MailDao
