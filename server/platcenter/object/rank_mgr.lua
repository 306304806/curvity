--rank_mgr.lua
local log_err       = logger.err
local log_debug     = logger.debug

local config_mgr    = quanta.get("config_mgr")

local rank_db       = config_mgr:init_table("rank", "id")

local RankObj       = import("platcenter/object/rank_obj.lua")

local RankMgr = singleton()
local prop = property(RankMgr)
prop:accessor("rank_objs", {})

function RankMgr:__init()
    self:setup()
end

function RankMgr:setup()
    for _, rank_cfg in rank_db:iterator() do
        local rank_obj = RankObj()
        local rank_type = rank_cfg.rank_type
        if rank_obj:setup(rank_cfg) then
            self.rank_objs[rank_type] = rank_obj
            log_debug("[RankMgr][create_rank_obj] success! rank_type:%s", rank_type)
        else
            log_err("[RankMgr][create_rank_obj failed! rank_type:%s", rank_type)
        end
    end
end

-- 获取排行榜对象
function RankMgr:get_rank_obj(rank_type)
    local rank_obj = self.rank_objs[rank_type or 0]
    return rank_obj
end

-- export
quanta.rank_mgr = RankMgr()

return RankMgr
