--white_list_gm.lua
local log_info          = logger.info
local serialize         = logger.serialize

local gm_agent          = quanta.get("gm_agent")
local event_mgr         = quanta.get("event_mgr")
local white_list_mgr    = quanta.get("white_list_mgr")

local GMType            = enum("GMType")

local WhiteListGM = singleton()
function WhiteListGM:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_import_white_list")
    event_mgr:add_listener(self, "gm_search_white_list")
    event_mgr:add_listener(self, "gm_delete_white_list")

    --注册GM
    self:register()
end

function WhiteListGM:register()
    local cmd_list = {
        {gm_type = GMType.AREA, name = "gm_import_white_list", desc = "导入白名单GM", args = "area_id|number white_lists|table"},
        {gm_type = GMType.AREA, name = "gm_search_white_list", desc = "搜索白名单GM", args = "area_id|number"},
        {gm_type = GMType.AREA, name = "gm_delete_white_list", desc = "删除白名单GM", args = "area_id|number white_lists|table"},
    }
    gm_agent:insert_cmd(cmd_list)
end

function WhiteListGM:gm_import_white_list(area_id, white_lists)
    log_info("[WhiteListGM][gm_import_white_list]area_id:%s, white_lists:%s", area_id, serialize(white_lists))
    local ok, fail_lists = white_list_mgr:import_white_list(white_lists)
    if not ok then
        return { code = 1, msg = "import_white_list failed" }
    end
    return { code = 0, data = { fail_lists = fail_lists } }
end

function WhiteListGM:gm_search_white_list(area_id)
    log_info("[WhiteListGM][gm_search_white_list]area_id:%s", area_id)
    local ok, data = white_list_mgr:search_white_list()
    if not ok then
        return { code = 1, msg = "gm_search_white_list failed" }
    end
    return { code = 0, data = data }
end

function WhiteListGM:gm_delete_white_list(area_id, white_lists)
    log_info("[WhiteListGM][gm_delete_white_list]area_id:%s, white_lists:%s", area_id, serialize(white_lists))
    local ok, fail_lists = white_list_mgr:delete_white_list(white_lists)
    if not ok then
        return { code = 1, msg = "delete_white_list failed" }
    end
    return { code = 0, data = { fail_lists = fail_lists } }
end

-- export
quanta.white_list_gm = WhiteListGM()

return WhiteListGM