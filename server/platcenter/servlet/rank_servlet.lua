--rank_servlet.lua

local otime          = os.time
local odate          = os.date
local log_err        = logger.err
local log_debug      = logger.debug

local KernCode      = enum("KernCode")
local RankType      = enum("RankType")
local SUCCESS       = KernCode.SUCCESS

local event_mgr     = quanta.get("event_mgr")
local rank_mgr      = quanta.get("rank_mgr")
local config_mgr    = quanta.get("config_mgr")
local dlog_mgr      = quanta.get("dlog_mgr")
local utility_db    = config_mgr:init_table("utility", "id")

local RankServlet = singleton()
function RankServlet:__init()
    event_mgr:add_listener(self, "rpc_pull_rank_data")
    event_mgr:add_listener(self, "rpc_report_rank_record")

    event_mgr:add_trigger(self, "evt_update_stable_rank")
end

-- 拉取排行榜数据
function RankServlet:rpc_pull_rank_data(rank_type, area_id, custom)
    log_debug("[RankServlet][rpc_pull_rank_data] rank_type:%s, area_id:%s, custom:%s", rank_type, area_id, custom)
    local rank_obj = rank_mgr:get_rank_obj(rank_type)
    if not rank_obj then
        log_err("[RankServlet][rpc_pull_rank_data] get rank obj failed! rank_type:%s", rank_type)
        return KernCode.PARAM_ERROR
    end

    local stable_rank_id, _ = rank_obj:get_rank_id(area_id, custom)
    if not stable_rank_id then
        log_err("[RankServlet][rpc_pull_rank_data] get stable_rank_id failed! area_id:%s, custom:%s", area_id, custom)
        return KernCode.PARAM_ERROR
    end

    return SUCCESS, rank_obj:get_rank_data(stable_rank_id)
end

-- 上报排行record
function RankServlet:rpc_report_rank_record(rank_type, area_id, record)
    --log_debug("[RankServlet][rpc_report_rank_record] rank_type:%s, area_id:%s, record:%s", rank_type, area_id, serialize(record))
    local rank_obj = rank_mgr:get_rank_obj(rank_type)
    if not rank_obj then
        log_err("[RankServlet][rpc_report_rank_record] get rank obj failed! rank_type:%s", rank_type)
        return KernCode.PARAM_ERROR
    end

    rank_obj:insert_record_to_latest(area_id, record)

    return SUCCESS
end

function RankServlet:evt_update_stable_rank(rank_type, area_id, rank_rows)
    local special_fields = {}
    special_fields.event_time  = odate("%Y-%m-%d %H:%M:%S", otime())
    special_fields.game_appkey = utility_db:find_one("dlog_app_key").value
    special_fields.zone_id     = area_id
    special_fields.rank_type   = rank_type
    for _, record in pairs(rank_rows) do
        if rank_type == RankType.STARS then
            special_fields.rank              = record.rank_pos
            special_fields.rank_value        = record.stars
            special_fields.character_id      = record.player_id
            special_fields.character_name    = record.nick
        end
        dlog_mgr:send_dlog_game_rank_klbq({special_fields = special_fields})
    end
end

-- export
quanta.rank_servlet     = RankServlet()

return RankServlet
