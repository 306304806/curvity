--player_dao.lua

local log_err       = logger.err
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local db_agent      = quanta.get("db_agent")

local PlayerDao = singleton()
function PlayerDao:__init()
end

function PlayerDao:count_player(selector)
    local ok, code, res = db_agent:count(1, {"plat_player", selector})
    if not ok or check_failed(code) then
        log_err("[PlayerDao][update_player_data] failed: code: %s", code, serialize(res))
        return false
    end

    return true, res
end

quanta.player_dao = PlayerDao()

return PlayerDao
