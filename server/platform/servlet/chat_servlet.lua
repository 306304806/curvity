--chat_servlet.lua
-- 全局邮件管理器,用于即使推送

local guid_index        = guid.index
local smake_id          = service.make_id
local log_debug         = logger.debug
local log_info          = logger.info
local log_warn          = logger.warn
local serialize         = logger.serialize
local check_failed      = utility.check_failed

local RmsgPlat          = enum("RmsgPlat")
local KernCode          = enum("KernCode")
local ChatCode          = enum("ChatCode")
local FriendCode        = enum("FriendCode")
local SSCmdID           = enum("PlatSSCmdID")
local DlogChatState     = enum("DlogChatState")

local SUCCESS           = KernCode.SUCCESS
local CSCmdID           = ncmd_plat.CCmdId

local data_mgr          = quanta.get("data_mgr")
local dlog_mgr          = quanta.get("dlog_mgr")
local chat_mgr          = quanta.get("chat_mgr")
local rmsg_chat         = quanta.get("rmsg_chat")
local event_mgr         = quanta.get("event_mgr")
local fword_mgr         = quanta.get("fword_mgr")
local router_mgr        = quanta.get("router_mgr")
local player_mgr        = quanta.get("player_mgr")
local account_limit_mgr = quanta.get("account_limit_mgr")

local ChatServlet = singleton()
function ChatServlet:__init()
    -- rpc
    event_mgr:add_trigger(self, "on_client_login")
    event_mgr:add_listener(self, "do_chat_recv")
    event_mgr:add_listener(self, "do_chat_message")
    event_mgr:add_listener(self, "rpc_chat_group_send")
    -- net
    event_mgr:add_cmd_listener(self, CSCmdID.NID_CHAT_SEND_REQ, "on_chat_send_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_CHAT_GROUP_SEND_REQ, "on_chat_group_send_req")
    -- ss
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_CHAT_GROUP_CREATE_REQ, "on_ss_chat_group_create_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_CHAT_GROUP_DESTORY_REQ, "on_ss_chat_group_destory_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_CHAT_GROUP_MEMBER_JOIN_REQ, "on_ss_chat_group_member_add_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_CHAT_GROUP_MEMBER_QUIT_REQ, "on_ss_chat_group_member_quit_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_CHAT_GROUP_MEMBER_UPDATE_REQ, "on_ss_chat_group_member_update_req")
end

-- 玩家登录事件
function ChatServlet:on_client_login(player, player_id)
    self:do_chat_message(player_id)
    -- 同步群列表
    local groups = chat_mgr:get_player_groups(player_id)
    for _, group in pairs(groups) do
        player:send(CSCmdID.NID_CHAT_GROUP_INFO_NTF, group:package())
    end
end

--处理消息发送
function ChatServlet:rpc_chat_group_send(player_id, group_id, team_id, msg, nick)
    local group = chat_mgr:get_chat_group(group_id)
    if not group then
        log_warn("[ChatServlet][on_chat_group_send_req] group not find: player_id=%s,group_id=%s", player_id, group_id)
        return ChatCode.ERR_CHAT_GROUP_NOT_EXIST
    end
    -- 检查队伍是否匹配
    if not group:check_team(player_id, team_id) then
        log_warn("[ChatServlet][on_chat_group_send_req] team not match: player_id=%s,team_id=%s", player_id, team_id)
        return ChatCode.ERR_CHAT_GROUP_NOT_IN_TEAM
    end
    -- 检查是否能发送（频率检测）
    if not group:check_limit(player_id) then
        log_warn("[ChatServlet][on_chat_group_send_req] player is limit: player_id=%s,group_id=%s", player_id, group_id)
        return ChatCode.ERR_CHAT_GROUP_HZ_LIMIT
    end
    -- 处理敏感词
    local _, real_msg = fword_mgr:trans(msg, "口")
    -- 群内广播发送内容
    group:static_chat(player_id)
    local chat_data = { group_id = group_id, player_id = player_id, msg = real_msg, team_id = team_id, nick = nick }
    if group:is_public() then
        --公共群
        player_mgr:boardcast_message(CSCmdID.NID_CHAT_GROUP_RECV_NTF, chat_data, group:get_area_id())
    else
        --私有群
        local member_ids = group:get_team_ids(team_id)
        player_mgr:send_group_message(member_ids, CSCmdID.NID_CHAT_GROUP_RECV_NTF, chat_data)
    end
    return SUCCESS
end

-- 玩家请求发送私聊
function ChatServlet:on_chat_send_req(player, player_id, net_req)
    -- 检查是否好友
    local tar_player_id = net_req.tar_player_id
    local pinfo = player:get_friend(net_req.tar_player_id)
    if not pinfo then
        log_warn("[ChatServlet][on_chat_send_req] tar is not friend: src=%s,tar=%s", player_id, tar_player_id)
        return { code = FriendCode.ERR_FRIEND_NOT_IS_FRIEND }
    end
    -- 处理敏感词
    local _, msg = fword_mgr:trans(net_req.msg, "口")
    local rmsg = {
        msg = "do_chat_recv",
        data = { src_player_id = player_id, src_nick = player:get_nick(), msg = msg }
    }
    local ok = rmsg_chat:send_message(quanta.name, net_req.tar_player_id, RmsgPlat.PLAT_CHAT, rmsg)
    if not ok then
        log_warn("[ChatServlet][on_chat_send_req] rmsg send faild: player_id=%s", net_req.src_player_id)
        return  { code = FriendCode.ERR_FRIEND_RMSG_FAILED }
    end
    log_info("[ChatServlet][on_chat_send_req] src=%s,tar=%s", player_id, tar_player_id)
    -- 通知对端处理
    data_mgr:notify_player(tar_player_id, "do_chat_message")
    self:report_dlog_chat(player, net_req.msg, DlogChatState.PRIVATE)
    -- 回传客户端发送的消息
    return { code = SUCCESS, context_id = net_req.context_id, msg = msg, tar_player_id = tar_player_id }
end

-- 玩家请求发送群聊
function ChatServlet:on_chat_group_send_req(player, player_id, net_req)
    -- 检查群是否存在
    local group_id, team_id = net_req.group_id, net_req.team_id
    log_info("[ChatServlet][on_chat_group_send_req] player_id=%s,group_id=%s,team_id=%s", player_id, group_id, team_id)
    -- 检查是否被禁言
    if account_limit_mgr:is_ban_chat(player_id) then
        log_warn("[ChatServlet][on_chat_group_send_req] player is ban: player_id=%s,group_id=%s", player_id, group_id)
        return { code = ChatCode.ERR_CHAT_PLAYER_IS_BAN }
    end
    local nick = player:get_nick()
    local index = guid_index(group_id)
    if index == quanta.index then
        local code = self:rpc_chat_group_send(player_id, group_id, team_id, net_req.msg, nick)
        return { code = code, context_id = net_req.context_id }
    end
    local platform_sid = smake_id("platform", index)
    local ok, code = router_mgr:call_target(platform_sid, "rpc_chat_group_send", player_id, group_id, team_id, net_req.msg, nick)
    if not ok or check_failed(code) then
        log_warn("[ChatServlet][on_chat_group_send_req] rpc_chat_group_send: ok=%s,code=%s", ok, code)
        return { code = ok and code or KernCode.RPC_FAILED }
    end
    return { code = code, context_id = net_req.context_id }
end

-- 内网rpc请求创建聊天群
function ChatServlet:on_ss_chat_group_create_req(rpc_req)
    log_debug("[ChatServlet][on_ss_chat_group_create_req] %s", serialize(rpc_req))
    local group_type = rpc_req.group_type
    local group = chat_mgr:create_chat_group(group_type)
    if group then
        -- 添加初始成员
        group:add_members(rpc_req.members)
        -- 填充生成的群ID
        local group_id = group:get_group_id()
        -- 广播群信息
        local group_info = group:package()
        local member_ids = group:get_team_ids()
        player_mgr:send_group_message(member_ids, CSCmdID.NID_CHAT_GROUP_INFO_NTF, group_info)
        log_info("[ChatServlet][on_ss_chat_group_create_req] group_id=%s,player_ids=%s", group_id, serialize(member_ids))
        return { code = SUCCESS, group_id = group_id }
    end
    return { code = ChatCode.ERR_CHAT_GROUP_CREATE_FAILD }
end

-- 内网rpc请求销毁聊天群
function ChatServlet:on_ss_chat_group_destory_req(rpc_req)
    local group_id = rpc_req.group_id
    log_debug("[ChatServlet][on_ss_chat_group_destory_req] group_id :%s", group_id)
    local group = chat_mgr:get_chat_group(group_id)
    if not group then
        log_warn("[ChatServlet][on_ss_chat_group_destory_req] group not exist: group_id=%s", group_id)
        return { code = ChatCode.ERR_CHAT_GROUP_NOT_EXIST }
    end
    -- 推送销毁通知
    local member_ids = group:get_team_ids()
    player_mgr:send_group_message(member_ids, CSCmdID.NID_CHAT_GROUP_DESTORY_NTF, { group_id = group_id })
    log_info("[ChatServlet][on_ss_chat_group_destory_req] group_id=%s", group_id)
    -- 销毁群
    chat_mgr:destory_chat_group(group_id)
    return { code = SUCCESS }
end

-- 内网rpc请求添加玩家到群
function ChatServlet:on_ss_chat_group_member_add_req(rpc_req)
    -- 查询目标群
    local group_id, member = rpc_req.group_id, rpc_req.member
    log_debug("[ChatServlet][on_ss_chat_group_member_add_req] group_id :%s, player_id :%s", group_id, member.player_id)
    local group = chat_mgr:get_chat_group(group_id)
    if not group then
        log_warn("[ChatServlet][on_ss_chat_group_member_add_req] group not exist: group_id=%s,member=%s", group_id, serialize(member))
        return { code = ChatCode.ERR_CHAT_GROUP_NOT_EXIST }
    end
    -- 玩家加入目标群
    local player_id = member.player_id
    if not group:has_member(player_id) then
        log_info("[ChatServlet][on_ss_chat_group_member_add_req] group_id=%s,player_id=%s", group_id, player_id)
        --新人通知
        local member_ids = group:get_team_ids()
        local join_ntf = {group_id = group_id, member = member }
        player_mgr:send_group_message(member_ids, CSCmdID.NID_CHAT_GROUP_MEMBER_NTF, join_ntf)
        --入群通知
        group:add_member(player_id, member)
        player_mgr:send_group_message({player_id}, CSCmdID.NID_CHAT_GROUP_INFO_NTF, group:package())
    end
    return { code = SUCCESS }
end

-- 内网rpc请求把玩家移出聊天群
function ChatServlet:on_ss_chat_group_member_quit_req(rpc_req)
    -- 查询目标群
    local group_id, player_id = rpc_req.group_id, rpc_req.player_id
    log_debug("[ChatServlet][on_ss_chat_group_member_quit_req] group_id :%s, player_id :%s", group_id, player_id)
    local group = chat_mgr:get_chat_group(group_id)
    if not group then
        log_warn("[ChatServlet][on_ss_chat_group_member_quit_req] group not exist: group_id=%s,player_id=%s", group_id, player_id)
        return { code = ChatCode.ERR_CHAT_GROUP_NOT_EXIST }
    end
    -- 玩家移出目标群
    if group:has_member(player_id) then
        local member_ids = group:get_team_ids()
        local quit_ntf = { group_id = group_id, player_id = player_id }
        player_mgr:send_group_message(member_ids, CSCmdID.NID_CHAT_GROUP_QUIT_NTF, quit_ntf)
        group:del_member(player_id)
    end
    return { code = SUCCESS }
end

-- 聊天群成员更换频道
function ChatServlet:on_ss_chat_group_member_update_req(rpc_req)
    -- 查询目标群
    local group_id, player_id, team_id = rpc_req.group_id, rpc_req.player_id, rpc_req.team_id
    log_debug("[ChatServlet][on_ss_chat_group_member_update_req] group_id :%s, player_id :%s, team_id :%s", group_id, player_id, team_id)
    local group = chat_mgr:get_chat_group(group_id)
    if not group then
        log_warn("[ChatServlet][on_ss_chat_group_member_update_req] group not exist: group_id=%s,player_id=%s", group_id, player_id)
        return { code = ChatCode.ERR_CHAT_GROUP_NOT_EXIST }
    end
    -- 校验玩家是否在群内
    if not group:has_member(player_id) then
        log_warn("[ChatServlet][on_ss_chat_group_member_update_req] player not int group: group_id=%s,player_id=%s", group_id, player_id)
        return { ChatCode.ERR_CHAT_GROUP_NOT_MEMBER }
    end
    local member_ids = group:get_team_ids()
    local member = group:update_team(player_id, team_id)
    local member_ntf = {group_id = group_id, member = member }
    player_mgr:send_group_message(member_ids, CSCmdID.NID_CHAT_GROUP_MEMBER_NTF, member_ntf)
    return { code = SUCCESS }
end

-- 处理离线聊天消息
function ChatServlet:do_chat_recv(player, rmsg_data)
    local net_ntf = {
        src_player_id = rmsg_data.src_player_id,
        src_nick      = rmsg_data.src_nick,
        msg           = rmsg_data.msg,
    }
    player:send(CSCmdID.NID_CHAT_RECV_NTF, net_ntf)
end

-- 处理离线消息
function ChatServlet:do_chat_message(player_id)
    local player = player_mgr:get_player(player_id)
    if player then
        local records = rmsg_chat:list_message(player_id)
        for _, record in pairs(records or {}) do
            local body = record.body
            event_mgr:notify_listener(body.msg, player, body.data)
            rmsg_chat:delete_message(player_id, record.uuid)
        end
    end
end

-- 精分上报-聊天
function ChatServlet:report_dlog_chat(player, content, chat_type)
    local special_fields = {}
    special_fields.chat_content      = content
    special_fields.chat_type         = chat_type
    special_fields.to_openid         = player.open_id
    special_fields.to_character_id   = player.player_id
    special_fields.to_character_name = player.data.nick
    special_fields.ip                = player.ip
    -- 精分上报-聊天
    dlog_mgr:send_dlog_game_chat({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
end

-- export
quanta.chat_servlet     = ChatServlet()

return ChatServlet
