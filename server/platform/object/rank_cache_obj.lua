--rank_cache_obj.lua

local guid_index    = guid.index
local tinsert       = table.insert
local check_success = utility.check_success

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local config_mgr    = quanta.get("config_mgr")

local PlatCommon     = enum("PlatCommon")

local RankCacheObj = class()
local prop = property(RankCacheObj)
-- 排行配置相关数据
prop:accessor("rank_type", nil)      -- 排行榜类型
prop:accessor("config", {})          -- 排行榜配置
prop:accessor("rank_algorithm", nil) -- 排行算法
prop:accessor("cmp_fields", {})      -- 排行算法字段集

prop:accessor("cache_rank_list", {}) -- 缓存排行榜数据

function RankCacheObj:__init()
end

-- 初始化函数
function RankCacheObj:setup(config)
    self.config = config
    self.rank_type = config.rank_type
    self.show_client_capacity = config.show_capacity
    self.rank_cfg_db = config_mgr:init_table(config.cfg_name, "id")
    if not self.rank_cfg_db then
        return false
    end

    -- 筛选字段
    self.rank_fields  = {}
    self.cmp_fields   = {}
    self.sync_client_fields = {}
    for _, data in self.rank_cfg_db:iterator() do
        if data.rank_factor then
            self.cmp_fields[data.priority] = {field_name = data.field_name, cmp = data.compare}
            self.rank_fields[data.field_name] = { flag = true, }
            local factor_ranks = self.rank_fields[data.field_name]
            if data.rank_lower and data.rank_lower > 0 then
                factor_ranks.rank_lower = data.rank_lower
            end
            if data.rank_upper and data.rank_upper > 0 then
                factor_ranks.rank_upper = data.rank_upper
            end
        end

        if data.sync_client then
            self.sync_client_fields[data.field_name] = data.id
        end
    end

    -- 构建排行算法
    self:build_rank_algorithm()

    -- 最新排行最后一名信息
    self.last_one = {}

    event_mgr:add_trigger(self, "ntf_rank_player_update")

    return true
end

-- 构建排行算法
function RankCacheObj:build_rank_algorithm()
    self.rank_algorithm = function (cmp_fields, lhs, rhs)
        --log_debug("RankCacheObj:build_rank_algorithm->cmp_fields:%s, lhs:%s, rhs:%s)", serialize(cmp_fields), serialize(lhs), serialize(rhs))
        for _, data in ipairs(cmp_fields) do
            local field = data.field_name
            if lhs[field] and rhs[field] and lhs[field] ~= rhs[field] then
                if data.cmp == "max" then
                    return lhs[field] > rhs[field]
                else
                    return lhs[field] < rhs[field]
                end
            end
        end

        return false
    end
end

-- 更新排行数据
function RankCacheObj:update_rank_data(area_id, custom, rank_data)
    for _, data in pairs(self.cache_rank_list) do
        if data.area_id == area_id and custom == data.custom then
            data.rank_data = rank_data
            return
        end
    end

    tinsert(self.cache_rank_list, {area_id = area_id, custom = custom, rank_data = rank_data})
end

--- 获取排行数据
function RankCacheObj:get_rank_data(area_id, custom)
    if self:is_whole_rank() then
        area_id = PlatCommon.GLOBAL_DEFAULT_AREA_ID
    end
    for _, data in pairs(self.cache_rank_list) do
        if data.area_id == area_id and custom == data.custom then
            return data.rank_data
        end
    end

    local ok, code, rank_data = router_mgr:call_platcenter_master("rpc_pull_rank_data", self.rank_type, area_id, custom)
    if ok and check_success(code) then
        self:update_rank_data(rank_data)
        return rank_data
    end
end

-- 设置最后一名数据
function RankCacheObj:set_last_one(area_id, data)
    self.last_one[area_id] = data
end

-- 检查是否更新玩家排行字段
function RankCacheObj:check_player_rank_fields(player_data, old_data, update_data)
    local satisfy = false
    for field_name, new_var in pairs(update_data) do
        local field_info = self.rank_fields[field_name]
        if field_name ~= "player_id" and field_info then
            satisfy = true

            local old_var = old_data[field_name] or 0
            local rank_lower = field_info.rank_lower
            if rank_lower and (old_var < rank_lower and new_var < rank_lower) then
                satisfy = false
                break
            end

            local rank_upper = field_info.rank_upper
            if rank_upper and (old_var > rank_upper and new_var > rank_upper) then
                satisfy = false
                break
            end
        end
    end

    return satisfy
end

-- 内部通知玩家数据变化
function RankCacheObj:ntf_rank_player_update(player_id, player_data, old_data, update_data)
    --log_debug("ntf_rank_player_update->old_data:%s, update_data:%s", logger.serialize(old_data), logger.serialize(update_data))
    if not self:check_player_rank_fields(player_data, old_data, update_data) then
        return
    end

    -- 构造record
    local new_record = {}
    new_record["player_id"] = player_id
    for _, data in self.rank_cfg_db:iterator() do
        local field_name = data.field_name
        if player_data[field_name] then
            new_record[field_name] = player_data[field_name]
        end
    end

    local area_id = guid_index(player_id)
    if self:is_whole_rank() then
        area_id = PlatCommon.GLOBAL_DEFAULT_AREA_ID
    end
    local last_one = self.last_one[area_id]
    if not last_one or not next(last_one) or self.rank_algorithm(self.cmp_fields, new_record, last_one) then
        router_mgr:call_platcenter_master("rpc_report_rank_record", self.rank_type, area_id, new_record)
    end
end

-- 是否全区排行
function RankCacheObj:is_whole_rank()
    return self.config.global_rank
end

return RankCacheObj
