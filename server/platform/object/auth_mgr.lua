--auth_mgr.lua

local odate         = os.date
local otime         = os.time
local ssub          = string.sub
local log_err       = logger.err
local log_debug     = logger.debug

local timer_mgr     = quanta.get("timer_mgr")
local player_mgr    = quanta.get("player_mgr")
local config_mgr    = quanta.get("config_mgr")
local auth_dao      = quanta.get("auth_dao")
local event_mgr     = quanta.get("event_mgr")

local PeriodTime    = enum("PeriodTime")
local CertType      = enum("CertType")

local CSCmdID       = ncmd_plat.CCmdId

local utility_db    = config_mgr:init_table("utility", "id")
local officialholiday_db = config_mgr:init_table("officialholiday", "id")

config_mgr:get_table("")

local function is_offical_holiday(year, month, day)
    local cur_date = otime({day = day, month = month, year = year,})
    for _, cfg in officialholiday_db:iterator() do
        if cur_date == cfg.holiday then
            return true
        end
    end

    return false
end

local AuthMgr = singleton()
local prop = property(AuthMgr)
-- 未认证玩家信息
prop:accessor("unauth_records", {})
-- 未成年玩家信息
prop:accessor("teenage_datas", {})
-- 玩家认证数据
prop:accessor("auth_datas", {})

function AuthMgr:__init()
    self:setup()
end

function AuthMgr:setup()
    timer_mgr:loop(PeriodTime.SECOND_5_MS, function()
        self:timer_update_game_time()
    end)

    event_mgr:add_trigger(self, "evt_pay_recharge_order")
end

-- 是否开放认证系统
function AuthMgr:is_open_auth()
    local open_check_id = utility_db:find_one("auth_open").value
    if not open_check_id or open_check_id <= 0 then
        return false
    end

    return true
end

-- 玩家是否已认证
function AuthMgr:is_authed(open_id)
    if not self:is_open_auth() then
        return true
    end

    local auth_info = self:get_auth_info(open_id)
    if auth_info and auth_info.auth_type > 0 then
        return true
    end

    return false
end

-- 检查青少年玩家登录防沉迷
function AuthMgr:is_teenager_player(open_id)
    if not self:is_open_auth() then
        return false
    end

    local auth_info = self:get_auth_info(open_id)
    if not auth_info then
        return false
    end

    if auth_info.auth_type == CertType.ID_CARD then
        return self:check_certid_age(auth_info.auth_cert_id or "", 18)
    end

    return false
end

-- 是否未成年禁止登录时段
function AuthMgr:is_teen_forbid_login()
    local cur_date = odate("*t", otime())
    if cur_date.hour >= 22 or cur_date.hour < 8 then
        return true
    end

    return false
end

-- 检查未成年今日累计游戏时间
function AuthMgr:today_gametime_limit(open_id)
    local auth_info = self:get_auth_info(open_id)
    local today_game_time = auth_info.today_game_time or 0
    if today_game_time < 1.5 * PeriodTime.HOUR_S then
        return false
    end

    local cur_date = odate("*t", otime())
    if is_offical_holiday(cur_date.year, cur_date.month, cur_date.day) then
        if today_game_time < 3 * PeriodTime.HOUR_S then
            return false
        end
    end

    return true
end

-- 添加未认证玩家信息
function AuthMgr:add_unauth_record(open_id, player_id)
    self.unauth_records[open_id] = { player_id = player_id, }
end

-- 删除未认证玩家信息
function AuthMgr:rm_unauth_record(open_id)
    self.unauth_records[open_id] = nil
end

-- 添加玩家认证数据
function AuthMgr:update_auth_info(info)
    self.auth_datas[info.open_id] = info
end

-- 删除玩家认证数据
function AuthMgr:rm_auth_info(open_id)
    self.auth_datas[open_id] = nil
end

-- 添加未成年玩家认证数据
function AuthMgr:add_teen_info(open_id, cert_id, player_id, today_game_time, today_update_time)
    if self:check_certid_age(cert_id, 18) then
        log_debug("[AuthMgr][add_teen_info]->open_id:%s, cert_id:%s", open_id, cert_id)
        self.teenage_datas[open_id] = {player_id = player_id, today_game_time = today_game_time, today_update_time = today_update_time}
    end
end

-- 删除未成年玩家认证数据
function AuthMgr:rm_teen_info(open_id)
    self.teenage_datas[open_id] = nil
end

-- 获取玩家认证数据
function AuthMgr:get_auth_info(open_id)
    if not self.auth_datas[open_id] then
        local ok, info = auth_dao:load_auth_info(open_id)
        if not ok then
            log_err("[AuthRealNameService][on_client_login] load auth info failed! open_id:%s", open_id)
            return
        end

        if not (info and next(info)) then
            log_err("[AuthRealNameService][on_client_login] load auth info nil! may be old account! open_id:%s", open_id)
            return
        end

        self.auth_datas[open_id] = info
    end

    return self.auth_datas[open_id]
end

-- 获取未认证玩家累计游戏时间
function AuthMgr:get_unauth_game_time(open_id)
    local record = self.unauth_records[open_id]
    if record then
        return record.unauth_game_time
    end
    return 0
end

-- 检查身份证号是否未成年
function AuthMgr:check_certid_age(cert_id, age)
    if not cert_id or cert_id == "" or cert_id == 0 then
        return false
    end

    local cert_year = tonumber(ssub(cert_id, 7, 10))
    local cert_mon  = tonumber(ssub(cert_id, 11, 12))
    local cert_day  = tonumber(ssub(cert_id, 13, 14))

    local cur_time = otime()
    local cur_date = odate("*t", cur_time)
    local dif_year = cur_date.year - cert_year
    local cmp_age = age or 18
    if dif_year < cmp_age then
        return true
    elseif dif_year == cmp_age then
        if cert_mon > cur_date.month then
            return true
        elseif cert_mon == cur_date.month then
            if cert_day > cur_date.day then
                return true
            end
        end
    end

    return false
end

-- 玩家充值完成事件
function AuthMgr:evt_pay_recharge_order(order_info)
    local cur_time = otime()
    local cur_date = odate("*t", cur_time)
    local player = player_mgr:get_player(order_info.player_id)
    if not player then
        return
    end

    local auth_info = self:get_auth_info(player:get_open_id())
    if auth_info and self:check_certid_age(auth_info.auth_cert_id, 18) then
        local last_recharge_time = auth_info.last_recharge_time
        if last_recharge_time and last_recharge_time > 0 then
            local last_date = odate("*t", last_recharge_time)
            if last_date.month ~= cur_date.cur_date then
                auth_info.month_recharge_total = 0
            end
        end
        auth_info.last_recharge_time = cur_time
        auth_info.month_recharge_total = auth_info.month_recharge_total + order_info.amount / 100

        auth_dao:update_auth_info(auth_info)
    end
end

-- 检查玩家单次充值额度
function AuthMgr:check_recharge_amount(open_id, amount)
    local auth_info = self:get_auth_info(open_id)
    if not auth_info then
        return true, 0
    end

    if self:check_certid_age(auth_info.auth_cert_id, 8) then
        return false, 1
    end

    if self:check_certid_age(auth_info.auth_cert_id, 16) then
        if amount / 100 >= 50 then
            return false, 2
        end
    end

    if self:check_certid_age(auth_info.auth_cert_id, 18) then
        if amount / 100 >= 100 then
            return false, 2
        end
    end

    return true, 0
end

-- 获取认证年龄
function AuthMgr:get_auth_age(open_id)
    local auth_info = self:get_auth_info(open_id)
    if not auth_info or not auth_info.auth_cert_id or auth_info.auth_cert_id == "" then
        return 0
    end

    local cert_year = tonumber(ssub(auth_info.auth_cert_id, 7, 10))
    local cur_time  = otime()
    local cur_date  = odate("*t", cur_time)
    return cur_date.year - cert_year
end

-- 检查玩家月充值额度
function AuthMgr:check_month_recharge_amount(open_id, amount)
    local auth_info = self:get_auth_info(open_id)
    if not auth_info then
        return true, 0
    end

    local cur_time = otime()
    local cur_date = odate("*t", cur_time)
    local last_recharge_time = auth_info.last_recharge_time
    if last_recharge_time > 0 then
        local last_date = odate("*t", last_recharge_time)
        if last_date.month ~= cur_date.cur_date then
            return true, 0
        end

        if self:check_certid_age(auth_info.auth_cert_id, 16) then
            if auth_info.month_recharge_total >= 200 then
                return false, 4
            end
        end

        if self:check_certid_age(auth_info.auth_cert_id, 18) then
            if auth_info.month_recharge_total >= 400 then
                return false, 5
            end
        end
    end

    return true, 0
end

-- 定时更新
function AuthMgr:timer_update_game_time()
    if not self:is_open_auth() then
        return
    end

    self:timer_check_unauth()
    self:timer_check_teenager()
end

-- 检查未认证玩家累计时间
function AuthMgr:timer_check_unauth()
    local cur_time = otime()
    local reset_time_var = utility_db:find_one("unauth_reset_time").value
    local para_game_time = utility_db:find_one("unauth_game_time").value
    local ntf_list = {}

    for open_id, data in pairs(self.unauth_records) do
        local auth_info = self:get_auth_info(open_id)
        if auth_info and auth_info.unauth_game_time and auth_info.unauth_reset_time then
            if auth_info.unauth_game_time > 0 and auth_info.unauth_reset_time and cur_time > auth_info.unauth_reset_time + reset_time_var then
                auth_info.unauth_reset_time = cur_time
                auth_info.unauth_game_time = 0
            else
                auth_info.unauth_game_time = auth_info.unauth_game_time + 5
                if auth_info.unauth_game_time >=  para_game_time then
                    ntf_list[open_id] = { player_id = data.player_id }
                end
            end
        end
    end

    local ntf = {}
    for open_id, data in pairs(ntf_list) do
        local player = player_mgr:get_player(data.player_id)
        local auth_info = self:get_auth_info(open_id)
        if player and auth_info then
            ntf.auth_type          = auth_info.auth_type
            ntf.unauth_game_time   = auth_info.unauth_game_time
            ntf.unauth_reset_time  = auth_info.unauth_reset_time
            ntf.config_game_time   = para_game_time
            ntf.auth_age           = self:get_auth_age(open_id)
            player:send(CSCmdID.NID_AUTH_REAL_NAME_NTF, ntf)
            log_debug("[AuthMgr][timer_check_unauth] game time overdue! player_id:%s", data.player_id)
        end
    end
end

-- 检查未成年玩家今日累计游戏时间
function AuthMgr:timer_check_teenager()
    local ntf_list = {}
    if self:is_teen_forbid_login() then
        for open_id, data in pairs(self.teenage_datas) do
            ntf_list[data.player_id] =  { is_forbid_time = true, today_game_time = 0 }
        end
    end

    local cur_date = odate("*t", otime())
    local cur_time = otime()
    for open_id, data in pairs(self.teenage_datas) do
        local update_date = odate("*t", data.today_update_time or 0)
        local auth_info = self:get_auth_info(open_id)
        if cur_date.year == update_date.year and cur_date.month == update_date.month and cur_date.day == update_date.day then
            auth_info.today_game_time = auth_info.today_game_time + 5
            if self:today_gametime_limit(open_id) then
                ntf_list[data.player_id] =  { is_forbid_time = false, today_game_time = auth_info.today_game_time }
            end
        else
            auth_info.today_game_time = 0
            auth_info.today_update_time = cur_time
        end
    end

    for player_id, ntf in pairs(ntf_list) do
        local player = player_mgr:get_player(player_id)
        if player then
            player:send(CSCmdID.NID_AUTH_TEEN_OVER_NTF, ntf)
            log_debug("[AuthMgr][timer_check_teenager] game time overdue! player_id:%s", player_id)
        end
    end
end

-- export
quanta.auth_mgr = AuthMgr()

return AuthMgr
