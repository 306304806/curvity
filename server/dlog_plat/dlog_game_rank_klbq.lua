--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_game_rank_klbq.lua
--排行榜
--luacheck: ignore 631

local sformat    = string.format
local log_err    = logger.err
local DlogGameRankKlbq = class()
local prop = property(DlogGameRankKlbq)
prop:accessor("table_name", "dlog_game_rank_klbq")



--!!!以下是特有字段，需调用方填充ctx!!!
--[[(独立字段) 格式：YYYY-MM-DD HH:MM:SS (必填)]]
prop:accessor("event_time", "")
--[[(独立字段) 游戏申请的乐逗SDK APPKEY (必填)]]
prop:accessor("game_appkey", "")
--[[(独立字段) 游戏全局唯一区服ID (必填)]]
prop:accessor("zone_id", "")
--[[(独立字段) 比如，1-排位赛段位排行榜，2-公会榜 (必填)]]
prop:accessor("rank_type", "")
--[[(独立字段) 预留 (必填)]]
prop:accessor("rank_subtype", "")
--[[(独立字段) 预留 (必填)]]
prop:accessor("rank_subtype_tab", "")
--[[(独立字段) 1,2…. (必填)]]
prop:accessor("rank", "")
--[[(独立字段) 段位排行榜为段位id (必填)]]
prop:accessor("rank_value", "")
--[[(独立字段) 段位排行榜为星数 (必填)]]
prop:accessor("rank_subvalue", "")
--[[(独立字段) 对外玩家Id(注：数据来自乐逗SDK中的对外OpenId)，也要兼容非乐逗体系用户id (选填)]]
prop:accessor("open_id", "")
--[[(独立字段) 行会榜单，角色信息可为空 (选填)]]
prop:accessor("character_id", "")
--[[(独立字段) 上榜用户角色名称 (选填)]]
prop:accessor("character_name", "")
--[[(独立字段) 上榜用户角色等级 (选填)]]
prop:accessor("character_level", "")
--[[(独立字段) 所处行会ID (必填)]]
prop:accessor("group_id", "")
--[[(独立字段) 行会名称 (必填)]]
prop:accessor("group_name", "")

function DlogGameRankKlbq:__init()
    self.special_fields = { ["event_time"] = true, ["game_appkey"] = true, ["zone_id"] = true, ["rank_type"] = true, ["rank_subtype"] = true, ["rank_subtype_tab"] = true, ["rank"] = true, ["rank_value"] = true, ["rank_subvalue"] = true, ["open_id"] = true, ["character_id"] = true, ["character_name"] = true, ["character_level"] = true, ["group_id"] = true, ["group_name"] = true,}

end

function DlogGameRankKlbq:contruct_report_str(ctx)
    if not ctx then
        log_err("DlogGameRankKlbq report param error!")
        return
    end

    for field_name in pairs(self.special_fields) do
        if self.filter_fields and self.filter_fields[field_name] then
            self[field_name] = string.gsub(ctx.special_fields[field_name] or "", "|", "*")
        else
            self[field_name] = ctx.special_fields[field_name] or ""
        end
    end

    return sformat("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s", self.table_name,  self.event_time, self.game_appkey, self.zone_id, self.rank_type, self.rank_subtype, self.rank_subtype_tab, self.rank, self.rank_value, self.rank_subvalue, self.open_id, self.character_id, self.character_name, self.character_level, self.group_id, self.group_name)
end

return DlogGameRankKlbq