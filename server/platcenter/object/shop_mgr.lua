--shop_mgr.lua
local log_debug     = logger.debug

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local config_mgr    = quanta.get("config_mgr")

local store_db      = config_mgr:init_table("store", "id")

local ShopMgr = singleton()

function ShopMgr:__init()
    --- 订阅配置热更
    event_mgr:add_listener(self, "reload_config")
    -- 订阅服务器启动
    router_mgr:watch_service_ready(self, "platform")
end

-- 服务器启动准备事件
function ShopMgr:on_service_ready(id, service_name)
    router_mgr:call_target(id, "rpc_update_shop_cfg", store_db:get_version(), store_db:get_rows())
    log_debug("[ShopMgr][on_service_ready] send shop config to platform %s!", id)
end

-- 配置热更回调
function ShopMgr:reload_config()
    -- 通知各platform新商城配置
    router_mgr:call_platform_all("rpc_update_shop_cfg", store_db:get_version(), store_db:get_rows())
    log_debug("[ShopMgr][reload_config]->update shop config to all platform!")
end

-- export
quanta.shop_mgr = ShopMgr()

return ShopMgr
