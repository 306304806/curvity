--utility_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local utility = config_mgr:get_table("utility")

--导出版本号
utility:set_version(10000)

--导出配置内容
utility:upsert({
    id = 'home_website',
    desc = '官网地址',
    value = 'day1game.com',
})

utility:upsert({
    id = 'offline_timeout',
    desc = '掉线超时时长(秒)',
    key = 'offline_timeout',
    value_type = 'int',
    value = 30,
})

utility:upsert({
    id = 'steam_app_id',
    desc = 'steam开发者账号',
    key = 'steam_app_id',
    value_type = 'string',
    value = '1282270',
})

utility:upsert({
    id = 'steam_web_key',
    desc = 'steam web开发者访问秘钥',
    key = 'steam_web_key',
    value_type = 'string',
    value = '64E7DB6AAC242AC93DADE1C65593FBFA',
})

utility:upsert({
    id = 'steam_web_publish_key',
    desc = 'steam web发行商秘钥',
    key = 'steam_web_publish_key',
    value_type = 'string',
    value = 'ED3CBDD01A6AA0A3079B5522D8391EB1',
})

utility:upsert({
    id = 'steam_sdk_key',
    desc = 'steam客户端秘钥',
    key = 'steam_sdk_key',
    value_type = 'string',
    value = '7087e2782a1a39621b77f1274ab20c9588aeed488450b44f82dc227810ae2a28',
})

utility:upsert({
    id = 'dlog_app_id',
    desc = '精分上报app id',
    key = 'dlog_app_id',
    value_type = 'string',
    value = '600041',
})

utility:upsert({
    id = 'dlog_app_key',
    desc = '精分上报app key',
    key = 'dlog_app_key',
    value_type = 'string',
    value = 'hjXkxwzsQFSezfHo2BgF',
})

utility:upsert({
    id = 'dlog_app_secret',
    desc = '精分上报app secret',
    key = 'dlog_app_secret',
    value_type = 'string',
    value = 'AOS2cWLB9DYbiubF8JfQ',
})

utility:upsert({
    id = 'dlog_plat_id',
    desc = '精分上报平台ID',
    key = 'dlog_plat_id',
    value_type = 'string',
    value = '4',
})

utility:upsert({
    id = 'dlog_report_url',
    desc = '精分上报url',
    key = 'dlog_report_url',
    value_type = 'string',
    value = 'http://dlog-test.uu.cc',
})

utility:upsert({
    id = 'auth_app_id',
    desc = '实名认证app id',
    key = 'auth_app_id',
    value_type = 'string',
    value = 'hpb8b7c61bc464fde4',
})

utility:upsert({
    id = 'auth_secret',
    desc = '实名认证 秘钥',
    key = 'auth_secret',
    value_type = 'string',
    value = 'ab4ba081cbf45f1f',
})

utility:upsert({
    id = 'auth_test_url',
    desc = '实名认证测试环境url',
    key = 'auth_test_url',
    value_type = 'string',
    value = 'https://testapi.hope.qq.com/cgi-bin/Hope.fcgi',
})

utility:upsert({
    id = 'auth_url',
    desc = '实名认证url',
    key = 'auth_url',
    value_type = 'string',
    value = 'https://api.hope.qq.com/cgi-bin/Hope.fcgi',
})

utility:upsert({
    id = 'auth_cmd',
    desc = '实名认证命令码',
    key = 'auth_cmd',
    value_type = 'string',
    value = 'zxCheckIdCard',
})

utility:upsert({
    id = 'auth_open',
    desc = '开放实名认证功能',
    key = 'auth_open',
    value_type = 'int',
    value = 0,
})

utility:upsert({
    id = 'unauth_game_time',
    desc = '未认证可玩游戏时间',
    key = 'unauth_game_time',
    value_type = 'int',
    value = 300,
})

utility:upsert({
    id = 'unauth_reset_time',
    desc = '未认证重置间隔',
    key = 'unauth_reset_time',
    value_type = 'int',
    value = 600,
})

utility:upsert({
    id = 'battle_pass_day_task_cnt',
    desc = '特别行动日任务数量',
    key = 'battle_pass_day_task_cnt',
    value_type = 'int',
    value = 3,
})

utility:upsert({
    id = 'battle_pass_week_task_cnt',
    desc = '特别行动周任务数量',
    key = 'battle_pass_week_task_cnt',
    value_type = 'int',
    value = 5,
})

utility:upsert({
    id = 'battle_pass_loop_task_cnt',
    desc = '特别行动循环任务数量',
    key = 'battle_pass_loop_task_cnt',
    value_type = 'int',
    value = 1,
})

utility:upsert({
    id = 'day_flush_time',
    desc = '(UTC时间)日刷新时间(值时分秒，不包含年月日),需要每日刷新的相关资源，统一的刷新时间(默认早上六点)',
    key = 'day_flush_time',
    value_type = 'int',
    value = 21600,
})

utility:upsert({
    id = 'battlepass_senior_explores',
    desc = '高级搜查令赠送探索点数',
    key = 'battlepass_senior_explores',
    value_type = 'int',
    value = 1200,
})

utility:upsert({
    id = 'week_exp_limit',
    desc = '经验周上限',
    key = 'week_exp_limit',
    value_type = 'int',
    value = 400,
})

utility:upsert({
    id = 'week_ideal_limit',
    desc = '理想币周上限',
    key = 'week_ideal_limit',
    value_type = 'int',
    value = 400,
})

utility:upsert({
    id = 'settle_exp_param',
    desc = '结算经验参数',
    key = 'settle_exp_param',
    value_type = 'int',
    value = 1,
})

utility:upsert({
    id = 'settle_ideal_param',
    desc = '结算理想币参数',
    key = 'settle_ideal_param',
    value_type = 'int',
    value = 0.8,
})

utility:upsert({
    id = 'settle_rank_win_param',
    desc = '结算排位胜利参数',
    key = 'settle_rank_win_param',
    value_type = 'int',
    value = 1,
})

utility:upsert({
    id = 'settle_rank_lose_param',
    desc = '结算排位失败参数',
    key = 'settle_rank_lose_param',
    value_type = 'int',
    value = 0.7,
})

utility:upsert({
    id = 'settle_room_win_param',
    desc = '结算房间胜利参数',
    key = 'settle_room_win_param',
    value_type = 'int',
    value = 0,
})

utility:upsert({
    id = 'settle_room_lose_param',
    desc = '结算房间失败参数',
    key = 'settle_room_lose_param',
    value_type = 'int',
    value = 0,
})
