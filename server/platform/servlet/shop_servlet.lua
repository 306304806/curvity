--shop_servlet.lua

local otime             = os.time
local tinsert           = table.insert
local log_debug         = logger.debug
local log_err           = logger.err
local serialize         = logger.serialize
local check_failed      = utility.check_failed

local CSCmdID           = ncmd_plat.CCmdId
local SSCmdID           = enum("PlatSSCmdID")

local FriendType        = enum("FriendType")
local OrderType         = enum("OrderType")
local OrderStatus       = enum("OrderStatus")
local LimitPurchaseType = enum("LimitPurchaseType")
local MailType          = enum("MailType")
local SubGiftType       = enum("SubGiftType")
local ShopCode          = enum("ShopCode")
local MailCode          = enum("MailCode")
local FriendCode        = enum("FriendCode")
local KernCode          = enum("KernCode")
local FuncUnlockID      = enum("FuncUnlockID")
local PeriodTime        = enum("PeriodTime")
local SUCCESS           = KernCode.SUCCESS

local event_mgr         = quanta.get("event_mgr")
local player_mgr        = quanta.get("player_mgr")
local order_mgr         = quanta.get("order_mgr")
local shop_mgr          = quanta.get("shop_mgr")
local data_mgr          = quanta.get("data_mgr")
local mail_mgr          = quanta.get("mail_mgr")
local config_mgr        = quanta.get("config_mgr")

local functionunlock_db = config_mgr:init_table("functionunlock", "id")

local CLAIM_CNT_MAX     = 2
local FRIEND_GIVE_DAYS  = 7

local ShopServlet = singleton()
function ShopServlet:__init()
    -- RPC
    event_mgr:add_listener(self, "rpc_update_shop_cfg")
    -- CS
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_ALL_CFG_DATA_REQ,     "on_player_all_shop_data_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_CHANGE_CFG_DATA_REQ,  "on_player_change_shop_data_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_BUY_COMMODITY_REQ,    "on_player_buy_commodity_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_GIVING_REQ,           "on_player_giving_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_CLAIMING_REQ,         "on_player_claiming_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_SETTING_CLAIMING_REQ, "on_player_setting_claiming_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_CLAIM_GIVE_REQ,       "on_player_claim_give_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_SHOP_CLAIM_REFUSE_REQ,     "on_player_refuse_claim_req")
    -- SP
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_ORDER_PAY_INFO_REQ,     "on_order_pay_info_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_ORDER_PAY_FINISH_REQ,   "on_order_pay_finish_req")
end

--------------------------------------------------------------
local ERROR_CODE = {
    [LimitPurchaseType.DAY]     = ShopCode.COMMODITY_DAY_LIMIT,
    [LimitPurchaseType.WEEK]    = ShopCode.COMMODITY_WEEK_LIMIT,
    [LimitPurchaseType.MONTH]   = ShopCode.COMMODITY_MONTH_LIMIT,
    [LimitPurchaseType.FORVER]  = ShopCode.COMMODITY_FOREVER_LIMIT,
    [LimitPurchaseType.BATTLEPASS] = ShopCode.COMMODITY_BATTLEPASS_LIMIT,
}

local function check_self_buy_commodity(player, shop_index, count, pay_currency_id)
    log_debug("[check_self_buy_commodity]->player_id:%s, shop_index:%s, count:%s", player.player_id, shop_index, count)
    local shop_cfg_data = shop_mgr:get_shop_cfg_data(shop_index)
    if not shop_cfg_data then
        return ShopCode.COMMODITY_NOT_EXIST
    end
    local cur_time = otime()
    if shop_cfg_data.up_time and shop_cfg_data.up_time > 0 and cur_time < shop_cfg_data.up_time then
        return ShopCode.COMMODITY_NOT_SHELVES
    end
    if shop_cfg_data.down_time and shop_cfg_data.down_time > 0 and cur_time >= shop_cfg_data.down_time then
        return ShopCode.COMMODITY_OFF_SHELVES
    end
    local goods_cfg = shop_mgr:get_goods_cfg(shop_cfg_data.goods_id)
    if player:is_limit_purchase(goods_cfg, count) then
        return ERROR_CODE[goods_cfg.limit_type]
    end
    local flag = false
    for _, data in pairs(shop_cfg_data.now_price) do
        if data.currency_id == pay_currency_id then
            flag = true
            break
        end
    end
    if not flag then
        return KernCode.PARAM_ERROR
    end
    return SUCCESS
end

--玩家发起自购请求
function ShopServlet:on_player_buy_commodity_req(player, player_id, data)
    log_debug("[ShopServlet][on_player_buy_commodity_req]->player_id:%s, data:%s", player_id, serialize(data))
    local shop_index, buy_count, pay_currency_id  = data.shop_index, data.count, data.pay_currency_id
    if not shop_index or not buy_count or not pay_currency_id or buy_count <= 0 or not pay_currency_id then
        return { code = KernCode.PARAM_ERROR }
    end
    local pay_order_id = order_mgr:check_paying_order(player_id, shop_index, buy_count, pay_currency_id)
    if pay_order_id then
        return { code = SUCCESS, order_id = pay_order_id }
    end
    local code = check_self_buy_commodity(player, shop_index, buy_count, pay_currency_id)
    if check_failed(code) then
        log_err("[ShopServlet][on_player_buy_commodity_req]->check failed! player_id:%s, code:%s", player_id, code)
        return { code = code }
    end
    local order_cxt = {
        order_type      = OrderType.SELF_BUY,
        payer_id        = player_id,
        receiver_id     = player_id,
        shop_index      = shop_index,
        buy_count       = buy_count,
        pay_currency_id = pay_currency_id,
    }
    local order_id = order_mgr:create_order(order_cxt)
    log_debug("[ShopServlet][on_player_buy_commodity_req]->player_id:%s, order_id:%s", player_id, order_id)
    return { code = SUCCESS, order_id = order_id }
end

--玩家发起赠送请求
function ShopServlet:on_player_giving_req(player, player_id, data)
    log_debug("[ShopServlet][on_player_giving_req]->player_id:%s, data:%s", player_id, serialize(data))
    local shop_index = data.shop_index
    local count = data.count
    local resp = { code = SUCCESS, order_id = 0, }
    local shop_cfg_data = shop_mgr:get_shop_cfg_data(shop_index)
    if not shop_cfg_data then
        resp.code = ShopCode.COMMODITY_NOT_EXIST
        return resp
    end
    local goods_cfg = shop_mgr:get_goods_cfg(shop_cfg_data.goods_id)
    if not count or count <= 0 or not goods_cfg.present_available then
        resp.code = KernCode.PARAM_ERROR
        return resp
    end
    local receiver_id = data.receiver_id
    local friend = player:get_friend(receiver_id)
    if not friend then
        log_err("[ShopServlet][on_player_giving_req]->player_id:%s, receiver_id:%s find friend failed!", player_id, receiver_id)
        resp.code = FriendCode.ERR_FRIEND_NOT_IS_FRIEND
        return resp
    end
    if friend.friend_type ~= FriendType.FRIEND then
        log_err("[ShopServlet][on_player_giving_req]->failed! player_id:%s, receiver_id:%s friend_type:%s!!", player_id, receiver_id, friend.friend_type)
        resp.code = FriendCode.ERR_FRIEND_NOT_IS_FRIEND
        return resp
    end
    if friend.friend_time and friend.friend_time + PeriodTime.DAY_S * FRIEND_GIVE_DAYS > otime() then
        log_err("[ShopServlet][on_player_giving_req]->failed! player_id:%s, receiver_id:%s friend_time:%s error!!", player_id, receiver_id, friend.friend_time)
        resp.code = ShopCode.GIVE_TIME_NOT_ENOUGH
        return resp
    end
    local player_data = player:get_data()
    if player_data.level < functionunlock_db:find_one(FuncUnlockID.GIVING).player_level then
        log_err("[ShopServlet][on_player_giving_req]->player level not enough! player_id:%s, cur_level:%s!", player_id, player_data.level)
        resp.code = ShopCode.GIVE_LV_NOT_ENOUGH
        return resp
    end
    local friend_player_data = data_mgr:get_player_data(receiver_id)
    local order_cxt = {
        order_type      = OrderType.GIVING,
        payer_id        = player_id,
        receiver_id     = player_id,
        shop_index      = shop_index,
        buy_count       = count,
        pay_currency_id = goods_cfg.present_price[1].currency_id,
        receiver_name   = friend.nick,
        payer_name      = friend_player_data.nick,
    }
    resp.order_id = order_mgr:create_order(order_cxt)
    return resp
end

-- 玩家发起索要请求
function ShopServlet:on_player_claiming_req(player, player_id, data)
    log_debug("[ShopServlet][on_player_claiming_req]->player_id:%s, data:%s", player_id, serialize(data))
    local id = data.shop_index
    local count = data.count
    local target_id = data.target_id
    local friend = player:get_friend(target_id)
    if not friend or friend.friend_type ~= FriendType.FRIEND then
        return { code = FriendCode.ERR_FRIEND_NOT_IS_FRIEND }
    end
    if not count or count <= 0 then
        return { KernCode.PARAM_ERROR }
    end
    local shop_cfg_data = shop_mgr:get_shop_cfg_data(id)
    if not shop_cfg_data then
        return { code = ShopCode.COMMODITY_NOT_EXIST }
    end
    local goods_cfg  = shop_mgr:get_goods_cfg(shop_cfg_data.goods_id)
    if not goods_cfg then
        return { code = ShopCode.COMMODITY_NOT_EXIST }
    end
    local friend_player_data = data_mgr:get_player_data(target_id)
    if friend_player_data.refuse_claim then
        return { code = ShopCode.REFUSE_CLAIM }
    end
    --等级限制
    local player_data = player:get_data()
    if player_data.level < functionunlock_db:find_one(FuncUnlockID.CLAIM).player_level then
        return { code = ShopCode.CLAIM_LV_NOT_ENOUGH }
    end
    --索要申请上限约束，一个好友一天最多索要两次
    if player:get_claim_count(target_id) >= CLAIM_CNT_MAX then
        return { code = ShopCode.CLAIM_CNT_MAX }
    end
    --邮件通知好友索要申请
    local pay_currency_id = goods_cfg.present_price[1].currency_id
    local mail_data = {
        attach_items    = {},
        src_player_id   = player_id,
        tar_player_id   = target_id,
        src_player_name = player:get_nick(),
        sub_type        = SubGiftType.GIFT_CLAIM,
        gift_item       = { shop_index = id, count = count, pay_currency_id = pay_currency_id },
    }
    mail_mgr:send_mail(mail_data, MailType.GIFT_MAIL)
    return { code = SUCCESS }
end

--玩家请求商城所有数据
function ShopServlet:on_player_all_shop_data_req(player, player_id, data)
    log_debug("[ShopServlet][on_player_all_shop_data_req]->player_id:%s", player_id)
    local shop_cfg_list = {}
    local cur_time = quanta.now
    for _, cfg in pairs(shop_mgr:get_shop_cfg()) do
        if cur_time < cfg.down_time then
            tinsert(shop_cfg_list, cfg)
        end
    end
    return {
        code            = SUCCESS,
        shop_cfg_list   = shop_cfg_list,
        version         = shop_mgr:get_shop_version(),
        limitation_list = player:pack_client_limit_buy(),
    }
end

-- 玩家请求商城变化数据
function ShopServlet:on_player_change_shop_data_req(player, player_id, data)
    log_debug("[ShopServlet][on_player_change_shop_data_req]->player_id:%s, data:%s", player_id, serialize(data))
    local req_version = data.version
    local cur_version = shop_mgr:get_shop_version()
    if not cur_version or req_version > cur_version then
        log_err("[ShopServlet][on_player_change_shop_data_req]->version error!player_id:%s, version:%s", player_id, req_version)
        return { code = KernCode.PARAM_ERROR }
    end
    local change_list = {}
    if req_version < cur_version then
        for _, cfg in pairs(shop_mgr:get_shop_cfg()) do
            if req_version < cfg.version then
                tinsert(change_list, cfg)
            end
        end
    end
    local limit_list = player:pack_client_limit_buy()
    return { code = SUCCESS, version = cur_version, change_list = change_list, limitation_list = limit_list,}
end

-- 设置索要接受标识
function ShopServlet:on_player_setting_claiming_req(player, player_id, data)
    player:update_data({ refuse_claim = data.flag })
    return { code = SUCCESS }
end

-- 赠送索要道具请求
function ShopServlet:on_player_claim_give_req(player, player_id, data)
    local mail = player:get_mail(data.mail_id)
    if not mail then
        return { code = MailCode.ERR_MAIL_NOT_EXIST }
    end
    local gift_item = mail.gift_item
    local order_cxt = {
        payer_id        = player_id,
        order_type      = OrderType.CLAIMING,
        receiver_id     = mail.src_player_id,
        receiver_name   = mail.src_player_name,
        shop_index      = gift_item.shop_index,
        buy_count       = gift_item.count,
        pay_currency_id = gift_item.pay_currency_id,
        payer_name      = player:get_data().nick,
    }
    local order_id = order_mgr:create_order(order_cxt)
    return { code = SUCCESS, order_id = order_id }
end

-- 拒绝索要道具请求
function ShopServlet:on_player_refuse_claim_req(player, player_id, data)
    local mail_id = data.mail_id
    local mail = player:get_mail(mail_id)
    if not mail then
        return { code = MailCode.ERR_MAIL_NOT_EXIST }
    end
    player:delete_mail(mail_id)
    return { code = SUCCESS, mail_id = mail_id }
end

--请求订单支付信息
function ShopServlet:on_order_pay_info_req(data)
    local player_id, order_id = data.player_id, data.order_id
    local order = order_mgr:get_order(player_id, order_id)
    if not order then
        log_err("[ShopServlet][on_order_pay_info_req]order not exist!->player_id:%s, order_id:%s", player_id, order_id)
        return { code = ShopCode.ORDRE_NOT_EXIST }
    end
    local pay_info = order:get_pay_info()
    log_debug("[ShopServlet][on_order_pay_info_req]->player_id:%s, pay_info:%s", player_id, serialize(pay_info))
    return {code = SUCCESS, pay_info = pay_info }
end

-- 订单支付完成
function ShopServlet:on_order_pay_finish_req(data)
    local player_id, order_id = data.player_id, data.order_id
    log_debug("[ShopServlet][on_order_pay_finish_req] player_id:%s, order_id:%s", player_id, order_id)
    local order = order_mgr:get_order(player_id, order_id)
    if not order then
        log_err("[ShopServlet][on_order_pay_finish_req] get order failed!player_id:%s, order_id:%s", player_id, order_id)
        return { code = ShopCode.ORDRE_NOT_EXIST }
    end
    local resp  = {code = SUCCESS, shipment_info = {}}
    local shop_index = order.shop_index
    local shop_cfg_data = shop_mgr:get_shop_cfg_data(shop_index)
    local goods_id = shop_cfg_data.goods_id
    local goods_cfg = shop_mgr:get_goods_cfg(goods_id)
    local player = player_mgr:get_player(player_id)
    if player and goods_cfg and goods_cfg.limit_type ~= LimitPurchaseType.NULL then
        player:purchase_limit_goods(goods_cfg, order:get_buy_count())
        local ntf = {limit_info = {goods_id = goods_id, bought_cnt = player:get_goods_bought_cnt(goods_id)}}
        player:send(CSCmdID.NID_SHOP_LIMIT_BUY_NTF, ntf)
        log_debug("[ShopServlet][on_order_pay_finish_req]->ntf limit buy info! ntf:%s", serialize(ntf))
    end
    local order_type = order.order_type
    if order_type == OrderType.SELF_BUY then
        resp.shipment_info = order:get_shipment_info()
    else
        -- 礼物中心-收到礼物
        local gift_mail = {
            src_player_id   = player_id,
            tar_player_id   = order.receiver_id,
            src_player_name = player:get_nick(),
            sub_type        = SubGiftType.GIFT_RECEIVE,
            attach_items    = {{item_id = goods_id, item_count = order.buy_count }},
        }
        mail_mgr:send_mail(gift_mail, MailType.GIFT_MAIL)
        -- 赠送记录
        local record_mail = {
            attach_items    = {},
            src_player_id   = 0,
            tar_player_id   = player_id,
            sub_type        = SubGiftType.GIFT_RECORD,
            gift_item       = { tar_player_name = order.receiver_name },
        }
        mail_mgr:send_mail(record_mail, MailType.GIFT_MAIL)
        log_debug("[ShopServlet][on_order_pay_finish_req]->recive_gift_mail:%s", serialize(gift_mail))
    end
    order:set_status(OrderStatus.RECIVED)
    return resp
end

-- 更新商品信息
function ShopServlet:rpc_update_shop_cfg(shop_verison, shop_cfg)
    shop_mgr:update_shop_cfg(shop_verison, shop_cfg)
    return SUCCESS
end

-- export
quanta.shop_servlet     = ShopServlet()

return ShopServlet
