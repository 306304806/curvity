--shop_mgr.lua
local log_info      = logger.info

local config_mgr    = quanta.get("config_mgr")

local goods_db      = config_mgr:init_table("goods", "id")

local ShopMgr = singleton()
local prop = property(ShopMgr)
prop:accessor("shop_cfg", {})           -- 最新商城配置
prop:accessor("shop_version", 0)        -- 最新商城配置版本号

function ShopMgr:__init()
end

-- 更新商城配置
function ShopMgr:update_shop_cfg(version, shop_cfg)
    -- 更新数据
    self.shop_cfg = {}
    for _, cfg in pairs(shop_cfg) do
        self.shop_cfg[cfg.id] = cfg
    end
    self.shop_version = version
    log_info("[ShopMgr][update_shop_cfg]->update cfg success!")
end

-- 获取商城配置
function ShopMgr:get_shop_cfg_data(id)
    return self.shop_cfg[id]
end

-- 获取商品配置
function ShopMgr:get_goods_cfg(goods_id)
    if goods_id then
        return goods_db:find_one(goods_id)
    end
end

-- export
quanta.shop_mgr = ShopMgr()

return ShopMgr
