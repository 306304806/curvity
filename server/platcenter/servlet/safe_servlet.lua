--safe_servlet.lua
local SafeCode      = enum("SafeCode")
local KernCode      = enum("KernCode")
local PeriodTime    = enum("PeriodTime")
local SUCCESS       = KernCode.SUCCESS

local timer_mgr     = quanta.get("timer_mgr")
local event_mgr     = quanta.get("event_mgr")

local SafeServlet = singleton()
local prop = property(SafeServlet)
prop:accessor("locked_nick_map", {})

function SafeServlet:__init()
    event_mgr:add_listener(self, "rpc_lock_nick")
    -- 定时器
    timer_mgr:loop(PeriodTime.SECOND_10_MS, function ()
        self:on_timer()
    end)
end

function SafeServlet:on_timer()
    local cur_time = quanta.now
    for nick, lock_time in pairs(self.locked_nick_map) do
        -- 超过
        if lock_time >= cur_time then
            self.locked_nick_map[nick] = nil
        end
    end
end

-- 锁定昵称
function SafeServlet:rpc_lock_nick(rpc_req)
    local nick = rpc_req.nick
    if self.locked_nick_map[nick] then
        return { code = SafeCode.NICK_USED }
    end
    -- 先在内存锁定在查询数据库
    self.locked_nick_map[nick] = quanta.now + PeriodTime.MINUTE_5_S
    return { code = SUCCESS }
end

-- export
quanta.safe_servlet = SafeServlet()

return SafeServlet