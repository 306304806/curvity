taskkill /f /fi "windowtitle eq plat_*"

start "plat_monitor"     ./quanta.exe   ./publish/monitor.conf    --index=1
start "plat_router1"     ./quanta.exe   ./publish/router.conf     --index=1
start "plat_router2"     ./quanta.exe   ./publish/router.conf     --index=2
start "plat_dbsvr1"      ./quanta.exe   ./publish/dbsvr.conf      --index=1
start "plat_gateway1"    ./quanta.exe   ./publish/gateway.conf    --index=1
start "plat_platform1"   ./quanta.exe   ./publish/platform.conf   --index=1
start "plat_platcenter1" ./quanta.exe   ./publish/platcenter.conf --index=1
start "plat_proxy"       ./quanta.exe   ./publish/proxy.conf      --index=1
start "plat_cachesvr"    ./quanta.exe   ./publish/cachesvr.conf   --index=1

exit
