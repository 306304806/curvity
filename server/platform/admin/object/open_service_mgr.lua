--open_service_mgr.lua
local log_err               = logger.err
local log_info              = logger.info

local timer_mgr             = quanta.get("timer_mgr")
local thread_mgr            = quanta.get("thread_mgr")
local open_service_dao      = quanta.get("open_service_dao")

local DBLoading             = enum("DBLoading")
local PeriodTime            = enum("PeriodTime")

local OpenServiceMgr = singleton()
local prop = property(OpenServiceMgr)
prop:accessor("data_loading", DBLoading.INIT)
prop:accessor("data", {})
prop:accessor("dirty", nil)

function OpenServiceMgr:__init()
    self:setup()
end

-- 初始化函数
function OpenServiceMgr:setup()
    -- 定时检查存储排行索引数据
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:update_data_2db()
    end)
    --从数据库加载数据
    self.data_loading = DBLoading.LOADING
    thread_mgr:success_call(PeriodTime.SECOND_MS, function()
        local ok, result = open_service_dao:load_open_service()
        if ok then
            self.data = result
            self.data_loading = DBLoading.SUCCESS
            log_info("[OpenServiceMgr][setup]->load server setting success!")
            return true
        end
        log_err("[OpenServiceMgr][setup]->load server setting failed!")
        return false
    end)
end

-- 数据落地
function OpenServiceMgr:update_data_2db()
    if self.data_loading ~= DBLoading.SUCCESS then
        return
    end
    if self.dirty and open_service_dao:update_open_service(self.data) then
        self.dirty = false
    end
end

-- 设置属性值
function OpenServiceMgr:set_filed_value(name, value)
    if self.data[name] == value then
        return true
    end
    self.data[name] = value
    self.dirty = true
    return true
end

-- 获取属性值
function OpenServiceMgr:get_field_value(name)
    return self.data[name]
end

-- export
quanta.open_service_mgr = OpenServiceMgr()

return OpenServiceMgr