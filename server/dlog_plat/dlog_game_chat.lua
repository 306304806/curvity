--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_game_chat.lua
--聊天记录
--luacheck: ignore 631

local sformat    = string.format
local log_err    = logger.err
local DlogGameChat = class()
local prop = property(DlogGameChat)
prop:accessor("table_name", "dlog_game_chat")

--!!!以下是公共字段，调用时自动填充!!!
prop:accessor("has_public", true)
--[[(公共字段) 格式：YYYY-MM-DD HH:MM:SS (必填)]]
prop:accessor("event_time", "")
--[[(公共字段) 游戏申请的乐逗SDK APPKEY (必填)]]
prop:accessor("game_appkey", "")
--[[(公共字段) 游戏全局唯一区服ID (必填)]]
prop:accessor("zone_id", "")
--[[(公共字段) 对外玩家Id(注：数据来自乐逗SDK中的对外OpenId)，也要兼容非乐逗体系用户id (必填)]]
prop:accessor("open_id", "")
--[[(公共字段) SDK设备ID（SDK端可获取） (必填)]]
prop:accessor("sdk_device_id", "")
--[[(公共字段) android=1、ios=2、h5=3，pc=4 (必填)]]
prop:accessor("platform", "")
--[[(公共字段) 比如：华为、OPPO、VIVO、应用宝等,接了乐逗SDK则必须是从乐逗SDK获取的渠道商ID (必填)]]
prop:accessor("channel_group_id", "")
--[[(公共字段) 各渠道下的子渠道ID，比如华为1包、华为2包…接了乐逗SDK则必须是从乐逗SDK获取的channel_id (必填)]]
prop:accessor("channel_id", "")
--[[(公共字段) 游戏版本号 (必填)]]
prop:accessor("game_version", "")
--[[(公共字段) 全局唯一 (必填)]]
prop:accessor("character_id", "")
--[[(公共字段) 角色等级 (必填)]]
prop:accessor("character_level", "")
--[[(公共字段) 当前用户VIP等级 (必填)]]
prop:accessor("vip_level", "")


--!!!以下是特有字段，需调用方填充ctx!!!
--[[(独立字段) 聊天内容,上报的聊天内容中如有 | 或 换行符，需替换为其他字符，以免影响后端入库 (必填)]]
prop:accessor("chat_content", "")
--[[(独立字段) 频道类型,如：0私聊,1行会,2组队,3附近,4世界,5小喇叭、6弹幕 (选填)]]
prop:accessor("chat_type", "")
--[[(独立字段) 是否被禁言:0否,1是 (必填)]]
prop:accessor("is_banned", "")
--[[(独立字段) 聊天对象用户openID (必填)]]
prop:accessor("to_openid", "")
--[[(独立字段) 聊天对象角色ID (必填)]]
prop:accessor("to_character_id", "")
--[[(独立字段) 聊天对象角色名 (选填)]]
prop:accessor("to_character_name", "")
--[[(独立字段) 聊天对象渠道id (选填)]]
prop:accessor("to_channel_id", "")
--[[(独立字段) 玩家ip地址 (选填)]]
prop:accessor("ip", "")

function DlogGameChat:__init()
    self.special_fields = { ["chat_content"] = true, ["chat_type"] = true, ["is_banned"] = true, ["to_openid"] = true, ["to_character_id"] = true, ["to_character_name"] = true, ["to_channel_id"] = true, ["ip"] = true,}

    self.filter_fields = { ["chat_content"] = true,}

end

function DlogGameChat:contruct_report_str(ctx)
    if not ctx then
        log_err("DlogGameChat report param error!")
        return
    end

    for field_name in pairs(self.special_fields) do
        if self.filter_fields and self.filter_fields[field_name] then
            self[field_name] = string.gsub(ctx.special_fields[field_name] or "", "|", "*")
        else
            self[field_name] = ctx.special_fields[field_name] or ""
        end
    end

    return sformat("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s", self.table_name,  self.event_time, self.game_appkey, self.zone_id, self.open_id, self.sdk_device_id, self.platform, self.channel_group_id, self.channel_id, self.game_version, self.character_id, self.character_level, self.vip_level, self.chat_content, self.chat_type, self.is_banned, self.to_openid, self.to_character_id, self.to_character_name, self.to_channel_id, self.ip)
end

return DlogGameChat