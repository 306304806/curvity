--store_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local store = config_mgr:get_table("store")

--导出版本号
store:set_version(10000)

--导出配置内容
store:upsert({
    id = 1,
    store_type = 1,
    store_param = 1,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 20101002,
    now_price = {{currency_id=3,currency_amount=588},{currency_id=1,currency_amount=888}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 2,
    store_type = 1,
    store_param = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 20105002,
    now_price = {{currency_id=3,currency_amount=588},{currency_id=1,currency_amount=888}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 3,
    store_type = 1,
    store_param = 3,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 8,
    now_price = {{currency_id=1,currency_amount=388}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 4,
    store_type = 1,
    store_param = 3,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 14,
    now_price = {{currency_id=1,currency_amount=388}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 5,
    store_type = 1,
    store_param = 4,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 9,
    now_price = {{currency_id=1,currency_amount=388}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 6,
    store_type = 1,
    store_param = 4,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 15,
    now_price = {{currency_id=1,currency_amount=388}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 7,
    store_type = 1,
    store_param = 5,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 10,
    now_price = {{currency_id=1,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 8,
    store_type = 1,
    store_param = 6,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 11,
    now_price = {{currency_id=1,currency_amount=188}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 9,
    store_type = 1,
    store_param = 7,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 16,
    now_price = {{currency_id=1,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 10,
    store_type = 1,
    store_param = 8,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 11,
    now_price = {{currency_id=1,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 11,
    store_type = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 1,
    now_price = {{currency_id=1,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 12,
    store_type = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 2,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 13,
    store_type = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 18,
    now_price = {{currency_id=1,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 14,
    store_type = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 3,
    now_price = {{currency_id=1,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 15,
    store_type = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 4,
    now_price = {{currency_id=1,currency_amount=588},{currency_id=3,currency_amount=88}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 16,
    store_type = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 5,
    now_price = {{currency_id=1,currency_amount=588},{currency_id=3,currency_amount=88}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 17,
    store_type = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 6,
    now_price = {{currency_id=1,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 18,
    store_type = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 7,
    now_price = {{currency_id=1,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 19,
    store_type = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 6,
    now_price = {{currency_id=1,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 2,
})

store:upsert({
    id = 20,
    store_type = 2,
    up_time = 1584482400,
    down_time = 1637186400,
    goods_id = 7,
    now_price = {{currency_id=1,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 3,
})

store:upsert({
    id = 101,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 101,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 105,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 105,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 132,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 132,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 107,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 107,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 128,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 128,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 108,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 108,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20101002,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20101002,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20101003,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20101003,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20101004,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20101004,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20105002,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20105002,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20105003,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20105003,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20105004,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20105004,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20105005,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20105005,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20105006,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20105006,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20105007,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20105007,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20105008,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20105008,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 20105009,
    store_type = 0,
    up_time = 1584482400,
    down_time = 4098636000,
    goods_id = 20105009,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 19001,
    store_type = 0,
    store_param = 0,
    up_time = 1606255200,
    down_time = 4098636000,
    goods_id = 19001,
    now_price = {{currency_id=3,currency_amount=288}},
    present_price = {{currency_id=3,currency_amount=288}},
    goods_num = 1,
})

store:upsert({
    id = 19101,
    store_type = 0,
    store_param = 0,
    up_time = 1606255201,
    down_time = 4098636001,
    goods_id = 19101,
    now_price = {{currency_id=3,currency_amount=588}},
    present_price = {{currency_id=3,currency_amount=588}},
    goods_num = 1,
})

store:upsert({
    id = 19201,
    store_type = 0,
    store_param = 0,
    up_time = 1606255202,
    down_time = 4098636002,
    goods_id = 19201,
    now_price = {{currency_id=3,currency_amount=888}},
    present_price = {{currency_id=3,currency_amount=1788}},
    goods_num = 1,
})

store:upsert({
    id = 19301,
    store_type = 0,
    store_param = 0,
    up_time = 1606255203,
    down_time = 4098636003,
    goods_id = 19301,
    now_price = {{currency_id=3,currency_amount=1}},
    present_price = {{currency_id=3,currency_amount=1}},
    goods_num = 1,
})
