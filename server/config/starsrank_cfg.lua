--starsrank_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local starsrank = config_mgr:get_table("starsrank")

--导出版本号
starsrank:set_version(10000)

--导出配置内容
starsrank:upsert({
    id = 1,
    field_name = 'player_id',
    sync_client = true,
    rank_factor = true,
    priority = 5,
    compare = 'max',
})

starsrank:upsert({
    id = 2,
    field_name = 'rank_pos',
    sync_client = true,
})

starsrank:upsert({
    id = 3,
    field_name = 'nick',
    sync_client = true,
})

starsrank:upsert({
    id = 4,
    field_name = 'icon',
    sync_client = true,
})

starsrank:upsert({
    id = 5,
    field_name = 'stars',
    sync_client = true,
    rank_factor = true,
    priority = 1,
    compare = 'max',
    rank_lower = 124,
})

starsrank:upsert({
    id = 6,
    field_name = 'total_games',
    sync_client = true,
})

starsrank:upsert({
    id = 7,
    field_name = 'freq_roles',
    sync_client = true,
})

starsrank:upsert({
    id = 8,
    field_name = 'sex',
})

starsrank:upsert({
    id = 9,
    field_name = 'win_games',
    rank_factor = true,
    priority = 3,
    compare = 'max',
})

starsrank:upsert({
    id = 10,
    field_name = 'win_rate',
    rank_factor = true,
    priority = 2,
    compare = 'max',
})

starsrank:upsert({
    id = 11,
    field_name = 'avr_kill',
    rank_factor = true,
    priority = 4,
    compare = 'max',
})
