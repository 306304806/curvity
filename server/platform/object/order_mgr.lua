--order_mgr.lua

local  Order = import("platform/object/order.lua")
local new_guid      = guid.new
local group_index   = guid.group_index
local log_err       = logger.err
local log_debug     = logger.debug
local serialize     = logger.serialize

local timer_mgr     = quanta.get("timer_mgr")

local OrderType     = enum("OrderType")
local PeriodTime    = enum("PeriodTime")

local OrderMgr = singleton()

function OrderMgr:__init()
    self.all_orders    = {}                 -- 订单列表
    self:setup()
end

function OrderMgr:setup()
    --启动定时器扫描过期订单
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:on_timer_check()
    end)
end

--创建订单
function OrderMgr:create_order(order_cxt)
    log_debug("[OrderMgr][create_order]->order_cxt:%s", serialize(order_cxt))
    -- 支付者作为索引
    local payer_id = order_cxt.payer_id
    local player_orders = self.all_orders[payer_id]
    if not player_orders then
        player_orders = {}
        self.all_orders[payer_id] = player_orders
    end
    -- 创建订单
    local order = Order()
    local order_id = new_guid(quanta.index, order_cxt.order_type)
    if order:setup(order_cxt) then
        player_orders[order_id] = order
    end
    order_cxt.order_id = order_id
    return order_id
end

-- 获取订单
function OrderMgr:get_order(player_id, order_id)
    if self.all_orders[player_id] then
        return self.all_orders[player_id][order_id]
    end
    log_err("[OrderMgr][get_order] failed! player_id:%s, order_id:%s", player_id, order_id)
end

-- 检查自购有重复订单
function OrderMgr:check_paying_order(player_id, shop_index, buy_count, pay_currency_id)
    for order_id, order in pairs(self.all_orders[player_id] or {}) do
        if group_index(order_id) == OrderType.SELF_BUY then
            if order.shop_index == shop_index and order.buy_count == buy_count and order.pay_currency_id == pay_currency_id then
                return order_id
            end
        end
    end
end

-- 定时检查
function OrderMgr:on_timer_check()
    for player_id, player_orders in pairs(self.all_orders) do
        for order_id, order in pairs(player_orders) do
            if order:is_invalid() then
                log_debug("[OrderMgr][on_timer_check]->order_type:%s, player_id:%s, order_id:%s", player_id, order_id)
                self.all_orders[player_id][order_id] = nil
            end
        end
    end
end

-- export
quanta.order_mgr = OrderMgr()

return OrderMgr
