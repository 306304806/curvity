--cache_row_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local cache_row = config_mgr:get_table("cache_row")

--导出版本号
cache_row:set_version(10000)

--导出配置内容
cache_row:upsert({
    cache_name = 'account',
    cache_table = 'account',
    cache_key = 'open_id',
})

cache_row:upsert({
    cache_name = 'player_name',
    cache_table = 'player_name',
    cache_key = 'name',
})

cache_row:upsert({
    cache_name = 'image',
    cache_table = 'plat_player',
    cache_key = 'player_id',
})

cache_row:upsert({
    cache_name = 'player',
    cache_table = 'plat_player_friend',
    cache_key = 'player_id',
})

cache_row:upsert({
    cache_name = 'player',
    cache_table = 'plat_player_reddot',
    cache_key = 'player_id',
})

cache_row:upsert({
    cache_name = 'player',
    cache_table = 'plat_player_mail',
    cache_key = 'player_id',
})

cache_row:upsert({
    cache_name = 'player',
    cache_table = 'plat_player_shop',
    cache_key = 'player_id',
})

cache_row:upsert({
    cache_name = 'player',
    cache_table = 'plat_player_auth',
    cache_key = 'player_id',
})
