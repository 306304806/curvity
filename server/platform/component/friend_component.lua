--friend_component.lua
local new_guid      = guid.new
local tinsert       = table.insert
local tsize         = table_ext.size
local uedition      = utility.edition

local data_mgr      = quanta.get("data_mgr")
local player_dao    = quanta.get("player_dao")

local FriendType    = enum("FriendType")
local SocialSecret  = enum("SocialSecret")

local FriendComponent = mixin(
    "add_group",
    "get_group",
    "del_group",
    "swap_group",
    "pack_groups",
    "modify_group",
    "del_friend",
    "add_friend",
    "get_friend",
    "get_black",
    "del_request",
    "add_request",
    "get_far_member",
    "pack_friends",
    "move2group",
    "add_intimacy",
    "set_remarks",
    "change_secret"
)

local prop = property(FriendComponent)
prop:accessor("groups", {})
prop:accessor("nears", {})
prop:accessor("applys", {})
prop:accessor("blacks", {})
prop:accessor("friends", {})
prop:accessor("requests", {})
prop:accessor("near_count", 0)
prop:accessor("apply_count", 0)
prop:accessor("friend_count", 0)
prop:accessor("request_count", 0)
prop:accessor("day_request", 0)
prop:accessor("friend_dirty", false)
prop:accessor("social_secret", SocialSecret.PUBLIC)

local function insert_friend(self, friend)
    self.friends[friend.friend_id] = friend
    if friend.friend_type == FriendType.BLACK then
        self.blacks[friend.friend_id] = friend
    elseif friend.friend_type == FriendType.APPLY then
        self.applys[friend.friend_id] = friend
        self.apply_count = self.apply_count + 1
    elseif friend.friend_type == FriendType.NEAR then
        self.nears[friend.friend_id] = friend
        self.near_count = self.near_count + 1
    elseif friend.friend_type == FriendType.FRIEND then
        self.friend_count = self.friend_count + 1
    end
end

local function remove_friend(self, friend)
    self.friends[friend.friend_id] = nil
    if friend.friend_type == FriendType.BLACK then
        self.blacks[friend.friend_id] = nil
    elseif friend.friend_type == FriendType.APPLY then
        self.applys[friend.friend_id] = nil
        self.apply_count = self.apply_count - 1
    elseif friend.friend_type == FriendType.NEAR then
        self.nears[friend.friend_id] = nil
        self.near_count = self.near_count - 1
    elseif friend.friend_type == FriendType.FRIEND then
        self.friend_count = self.friend_count - 1
    end
end

local function load_friend_data(self, friend_res)
    if friend_res then
        for _, friend in pairs(friend_res.friends or {}) do
            insert_friend(self, friend)
        end
        for _, group in pairs(friend_res.groups or {}) do
            self.groups[group.group_id] = group
        end
        for _, request in pairs(friend_res.requests or {}) do
            self.requests[request.friend_id] = request
            self.request_count = self.request_count + 1
        end
        local timestamp = friend_res.timestamp
        self.day_request = friend_res.day_request or 0
        if timestamp and timestamp ~= uedition("day") then
            self.day_request = 0
        end
        self.social_secret = friend_res.social_secret or SocialSecret.PUBLIC
    end
end

function FriendComponent:__init(player_id)
end

function FriendComponent:setup(db_res)
    local player_friend = db_res.plat_player_friend
    if not player_friend then
        return false
    end
    load_friend_data(self, player_friend.friend)
    return true
end

function FriendComponent:update_data_2db()
    if not self.friend_dirty then
        return true
    end
    local friend_res = {
        groups = {},
        friends = {},
        requests = {},
        player_id = self.player_id,
        timestamp = uedition("day"),
        day_request = self.day_request,
        social_secret = self.social_secret,
    }
    for _, friend in pairs(self.friends) do
        tinsert(friend_res.friends, friend)
    end
    for _, group in pairs(self.groups) do
        tinsert(friend_res.groups, group)
    end
    for _, request in pairs(self.requests) do
        tinsert(friend_res.requests, request)
    end
    if player_dao:update_player_friend(self.player_id, friend_res) then
        self.friend_dirty = false
        return true
    end
    return false
end

function FriendComponent:pack_groups()
    local groups = { }
    for _, group in pairs(self.groups) do
        tinsert(groups, group)
    end
    return { groups = groups }
end

function FriendComponent:pack_friends()
    local friends = {}
    for friend_id, friend in pairs(self.friends) do
        local friend_data = data_mgr:get_player_data(friend_id)
        if friend_data then
            local friend_info = {
                player = friend_data,
                remarks = friend.remarks,
                intimacy = friend.intimacy,
                group_id = friend.group_id,
                friend_type = friend.friend_type,
                social_secret = self.social_secret,
            }
            tinsert(friends, friend_info)
        end
    end
    return { friends = friends, social_secret = self.social_secret }
end

function FriendComponent:get_far_member(friend_type)
    local list = self.requests
    if friend_type then
        if friend_type == FriendType.NEAR then
            list = self.nears
        elseif friend_type == FriendType.APPLY then
            list = self.applys
        end
    end
    local target_id, friend_time = nil, quanta.now
    for friend_id, member in pairs(list) do
        if member.friend_time < friend_time then
            friend_time = member.friend_time
            target_id = friend_id
        end
    end
    return target_id
end

function FriendComponent:add_intimacy(target_id, intimacy)
    local friend = self.friends[target_id]
    friend.intimacy = friend.intimacy + intimacy
    self.friend_dirty = true
end

function FriendComponent:set_remarks(target_id, remarks)
    local friend = self.friends[target_id]
    friend.remarks = remarks
    self.friend_dirty = true
end

function FriendComponent:change_secret(secret)
    self.social_secret = secret
    self.friend_dirty = true
end

function FriendComponent:swap_group(group, index)
    local sindex = group.index
    if sindex > index then
        for gid, grp in pairs(self.groups) do
            local gindex = grp.index
            if gindex < sindex and gindex >= index then
                grp.index = gindex + 1
            end
        end
        group.index = index
    else
        for gid, grp in pairs(self.groups) do
            local gindex = grp.index
            if gindex > sindex and gindex < index then
                grp.index = gindex - 1
            end
        end
        local gsize = tsize(self.groups)
        if index > gsize then
            group.index = gsize
        else
            group.index = index - 1
        end
    end
    self.friend_dirty = true
end

function FriendComponent:modify_group(group, group_name)
    self.friend_dirty = true
    group.group_name = group_name
end

function FriendComponent:get_group(group_id)
    if group_id then
        return self.groups[group_id]
    end
end

function FriendComponent:add_group(group_name)
    local group_id = new_guid()
    local group = {
        group_id    = group_id,
        group_name  = group_name,
        index = tsize(self.groups) + 1,
    }
    self.friend_dirty = true
    self.groups[group_id] = group
end

function FriendComponent:del_group(dgroup, group_id)
    local move_friends = {}
    self.friend_dirty = true
    self.groups[group_id] = nil
    for _, group in pairs(self.groups) do
        if group.index > dgroup.index then
            group.index = group.index - 1
        end
    end
    for friend_id, friend in pairs(self.friends) do
        if friend.group_id == group_id then
            friend.group_id = 0
            move_friends[friend_id] = friend
        end
    end
    return move_friends
end

function FriendComponent:add_request(target_id)
    self.friend_dirty = true
    self.day_request = self.day_request + 1
    self.request_count = self.request_count + 1
    self.requests[target_id] = { friend_id = target_id, friend_time = quanta.now }
end

function FriendComponent:del_request(target_id)
    local request = self.requests[target_id]
    if request then
        self.friend_dirty = true
        self.requests[target_id] = nil
        self.request_count = self.request_count - 1
    end
end

function FriendComponent:add_friend(target_id, friend_type)
    local friend = {
        group_id = 0,
        intimacy = 0,
        remarks = "",
        friend_id = target_id,
        friend_time = quanta.now,
        friend_type = friend_type,
    }
    --先删除好友类型
    self:del_friend(target_id)
    self:del_request(target_id)
    insert_friend(self, friend)
    self.friend_dirty = true
    return friend, self.friend_count
end

function FriendComponent:del_friend(target_id)
    local friend = self.friends[target_id]
    if friend then
        self.friend_dirty = true
        remove_friend(self, friend)
    end
end

function FriendComponent:get_friend(target_id)
    if target_id then
        return self.friends[target_id]
    end
end

function FriendComponent:get_black(target_id)
    if target_id then
        return self.blacks[target_id]
    end
end

function FriendComponent:move2group(target_id, group_id)
    local friend = self.friends[target_id]
    if friend then
        self.friend_dirty = true
        friend.group_id = group_id
    end
end

return FriendComponent
