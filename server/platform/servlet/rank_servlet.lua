--rank_servlet.lua

local tinsert       = table.insert
local guid_index    = guid.index
local log_err       = logger.err
local log_debug     = logger.debug
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local CSCmdID       = ncmd_plat.CCmdId

local KernCode      = enum("KernCode")
local RankType      = enum("RankType")
local SUCCESS       = KernCode.SUCCESS

local event_mgr       = quanta.get("event_mgr")
local rank_cache_mgr  = quanta.get("rank_cache_mgr")

local OPEN_STAR_RANK_RANDOM = true     -- 方便测试，增加排行插入开关

local RankServlet = singleton()
function RankServlet:__init()
    -- CS
    event_mgr:add_cmd_listener(self, CSCmdID.NID_RANK_DATA_REQ, "on_client_rank_data_req")

    -- SS
    -- 通知更新排行数据
    event_mgr:add_listener(self, "rpc_ntf_rank_update")
    -- 更新最新排行最后一名数据
    event_mgr:add_listener(self, "rpc_rank_update_last_one")

    event_mgr:add_trigger(self, "on_client_login")
end

-- 通知更新排行数据
function RankServlet:rpc_ntf_rank_update(rank_type, area_id, custom, rank_data)
    log_debug("[RankServlet][rpc_ntf_rank_update]->rank_type:%s, area_id:%s", rank_type, area_id)
    local rank_cache_obj = rank_cache_mgr:get_rank_obj(rank_type)
    if not rank_cache_obj then
        log_err("[RankServlet][rpc_ntf_rank_update]->get rank_cache_obj failed! rank_type:%s", rank_type)
        return KernCode.PARAM_ERROR
    end

    rank_cache_obj:update_rank_data(area_id, custom, rank_data)

    return SUCCESS
end

-- 全区排行通知平行节点更新最后一名数据
function RankServlet:rpc_rank_update_last_one(rank_type, area_id, last_one)
    local rank_cache_obj = rank_cache_mgr:get_rank_obj(rank_type)
    if not rank_cache_obj then
        log_err("[RankServlet][rpc_rank_update_last_one]->get rank_cache_obj failed! rank_type:%s", rank_type)
        return KernCode.PARAM_ERROR
    end

    log_debug("[RankServlet][rpc_rank_update_last_one]->rank_type:%s, last_one:%s", rank_type, serialize(last_one))
    rank_cache_obj:set_last_one(area_id, last_one)

    return SUCCESS
end

local function check_client_req_param(start_pos, end_pos, show_client_capacity)
    if start_pos <= 0 or start_pos >= end_pos or end_pos > show_client_capacity then
        return KernCode.PARAM_ERROR
    end

    return SUCCESS
end

-- 客户端请求排行榜数据
function RankServlet:on_client_rank_data_req(player, player_id, data)
    log_debug("[RankServlet][on_client_rank_data_req]->player_id:%s, data:%s", player_id, serialize(data))
    local resp = { code = SUCCESS, rank_type = data.rank_type, start_pos = data.start_pos, end_pos = data.end_pos, custom = data.custom, }
    for idx, var in pairs(data) do
        resp[idx] = var
    end
    local rank_type = data.rank_type
    local rank_cache_obj = rank_cache_mgr:get_rank_obj(rank_type)
    if not rank_cache_obj then
        log_err("[RankServlet][on_client_rank_data_req] get rank obj failed! ->player_id:%s", player_id)
        resp.code = KernCode.PARAM_ERROR
        return resp
    end

    local code = check_client_req_param(data.start_pos, data.end_pos, rank_cache_obj.show_client_capacity)
    if check_failed(code)  then
        log_err("[RankServlet][on_client_rank_data_req] pos failed! ->player_id:%s, start_pos:%s, end_pos:%s", player_id, data.start_pos, data.end_pos)
        return code
    end

    local rank_data = rank_cache_obj:get_rank_data(guid_index(player_id), data.custom)
    if not rank_data then
        log_err("[RankServlet][on_client_rank_data_req] get rank data failed! ->player_id:%s", player_id)
        resp.code = KernCode.PARAM_ERROR
        return resp
    end

    resp.rows_info = { }
    if not data.version or rank_data.version > data.version then
        for pos = data.start_pos, data.end_pos do
            local row = {fields_info = {}}
            local row_data = rank_data.rank_rows[pos]
            if row_data then
                for field_name, field_id in pairs(rank_cache_obj.sync_client_fields) do
                    local single_field_info = { field_id = field_id, field_var = tostring(row_data[field_name]) }
                    tinsert(row.fields_info, single_field_info)
                end
                tinsert(resp.rows_info, row)
            end
        end
    end

    local total =  #rank_data.rank_rows
    if total > rank_cache_obj.show_client_capacity then
        total = rank_cache_obj.show_client_capacity
    end
    resp.rank_total   = total
    resp.version      = rank_data.version

    return resp
end

function RankServlet:on_client_login(player, player_id)
    -- 方便测试，构造排行数据
    if OPEN_STAR_RANK_RANDOM then
        local player_data = player:get_data()
        local record = {
            ["player_id"] = player_id,
            ["nick"]      = player_data["nick"],
            ["icon"]      = player_data["icon"],
            ["sex"]       = player_data["sex"],

            ["stars"]       = math.random(30, 500),
            ["total_games"] = math.random(8000, 12000),
            ["win_games"]   = math.random(5000, 8000),
            ["avr_kill"]    = math.random(1, 10),
            ["freq_roles"]  = "101,102,103",
        }

        local win_rate = record.win_games / record.total_games
        if win_rate > 0 then
            win_rate = win_rate - win_rate % 0.01
        end
        record["win_rate"] = win_rate

        quanta.router_mgr:call_platcenter_master("rpc_report_rank_record", RankType.STARS, guid_index(player_id), record)
    end
end

-- export
quanta.rank_servlet     = RankServlet()

return RankServlet
