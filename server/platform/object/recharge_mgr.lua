-- recharge_mgr.lua
-- 充值订单管理
local log_info          = logger.info
local log_warn          = logger.warn
local tinsert           = table.insert

local timer_mgr         = quanta.get("timer_mgr")
local recharge_dao      = quanta.get("recharge_dao")
local event_mgr         = quanta.get("event_mgr")

local PeriodTime        = enum("PeriodTime")
local RechargeOrderState= enum("RechargeOrderState")
local RechargeCode      = enum("RechargeCode")
local KernCode          = enum("KernCode")

local ORDER_UNLOAD_SEC  = PeriodTime.HOUR_MS  -- 订单在内存中保存一个小时

local RechargeMgr = singleton()
local prop = property(RechargeMgr)
prop:accessor("oid2oinfo", {})   -- 内存中的订单信息
prop:accessor("oid2minfo", {})   -- 内存中的订单管理信息 map<order_id, struct(load_time)>
prop:accessor("dirty_map", {})   -- 脏id
prop:accessor("unconfirmed_order_map", {})  -- map<order_id,struct(pay_platform_id,player_id,retry_times,last_retry_time)> 未确认订单(需要重建的订单)
prop:accessor("unfinish_order_map", {})     -- map<order_id,struct(retry_times,last_retry_time)>发起的结束请求，但是未能在支付平台结束的订单
prop:accessor("pid_2_wsend_oids", {})    -- map<player_id,oids>  玩家以付款，但是游戏系统未发货的订单id

function RechargeMgr:__init()
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:on_timer()
    end)
end

function RechargeMgr:on_timer()
    local cur_time = quanta.now
    -- 脏订单写入数据库
    for oid, _ in pairs(self.dirty_map or {}) do
        local oinfo = self.oid2oinfo[oid]
        if oinfo and recharge_dao:update_order(oid, oinfo) then
            self.dirty_map[oid] = nil
            self:_remove_wait_send_order_id(oinfo.player_id, oinfo.order_id)
        end
    end
    -- 加载超过一个小时的订单，卸载掉
    for oid, minfo in pairs(self.oid2minfo or {}) do
        if (cur_time > ORDER_UNLOAD_SEC + minfo.load_time) and (not self.dirty_map[oid]) then
            log_info("[RechargeMgr][on_timer] unload order: order_id=%s", oid)
            self.oid2oinfo[oid] = nil
            self.oid2minfo[oid] = nil
            self.dirty_map[oid] = nil
        end
    end
end

-- 创建订单
function RechargeMgr:create(pay_platform_id, pay_open_id, pay_order_id, player_id, order_id, commodity_infos, amount)
    if self.oid2oinfo[order_id] then
        return RechargeCode.ORDER_ID_EXIST
    end
    -- 创建新订单
    local order_info = {
        pay_platform_id = pay_platform_id,             -- 支付平台
        pay_open_id     = pay_open_id,                 -- 支付平台上的用户标识
        pay_order_id    = pay_order_id,                -- 支付平台订单号
        player_id       = player_id,
        order_id        = order_id,                    -- 订单号
        commodity_infos = commodity_infos,             -- 商品信息(购物车(包含商品sku))
        amount          = amount,                      -- 总价
        state           = RechargeOrderState.WAIT_PAY, -- 待支付
        insert_time     = quanta.now,
        update_time     = quanta.now,
    }
    log_info("[RechargeMgr][create] pay_platform_id=%s,order_id=%s,pay_order_id=%s,player_id=%s",
       pay_platform_id, order_id, pay_order_id, player_id)
    self.dirty_map[order_id] = true
    self.oid2oinfo[order_id] = order_info
    self.oid2minfo[order_id] = {load_time = quanta.now}
    event_mgr:notify_trigger("evt_dlog_rechage_update", order_id)
    return KernCode.SUCCESS
end

-- 查询订单
function RechargeMgr:query(order_id)
    return self.oid2oinfo[order_id]
end

-- 支付
function RechargeMgr:pay(order_id)
    local oinfo = self.oid2oinfo[order_id]
    if not oinfo then
        log_warn("[RechargeMgr][pay] order not exist: order_id=%s", order_id)
        return RechargeCode.ORDER_ID_NOT_EXIST
    end
    -- 标记已支付待发货
    log_info("[RechargeMgr][pay] order_id=%s", order_id)
    oinfo.state = RechargeOrderState.WAIT_SEND
    event_mgr:notify_trigger("evt_pay_recharge_order", oinfo)

    oinfo.update_time = quanta.now
    -- 缓存未发货订单id
    self:_save_wait_send_order_id(oinfo.player_id, oinfo.order_id)
    self.dirty_map[order_id] = true

    event_mgr:notify_trigger("evt_dlog_rechage_update", order_id)
    return KernCode.SUCCESS
end

-- 发货
function RechargeMgr:send(order_id)
    local oinfo = self.oid2oinfo[order_id]
    if not oinfo then
        return RechargeCode.ORDER_ID_NOT_EXIST
    end
    -- 未支付
    if oinfo.state < RechargeOrderState.WAIT_SEND then
        return RechargeCode.ORDER_NOT_PAY
    end
    -- 已经发货
    if oinfo.state > RechargeOrderState.WAIT_SEND then
        return RechargeCode.ORDER_ALREADY_SEND
    end
    -- 标记为已发货
    log_info("[RechargeMgr][send] order_id=%s", order_id)
    oinfo.state = RechargeOrderState.FINISH
    oinfo.update_time = quanta.now
    -- 移除未发货订单缓存
    self:_remove_wait_send_order_id(oinfo.player_id, oinfo.order_id)
    self.dirty_map[order_id] = true

    event_mgr:notify_trigger("evt_dlog_rechage_update", order_id)
    return KernCode.SUCCESS, oinfo
end

-- 更新未能确认的订单(有支付完成回调，但是本系统找不到订单，需要重建)
function RechargeMgr:update_unconfirmed_order(order_id, player_id, pay_platform_id)
    -- 确认是否已存在订单
    if self.oid2oinfo[order_id] then
        self:remove_unconfirmed_order(order_id)
        return RechargeCode.ORDER_ID_EXIST
    end

    -- 添加到未确认订单，并更新重试信息
    local unconfirmed_info = self.unconfirmed_order_map[order_id]
    if not unconfirmed_info then
        unconfirmed_info = {
            order_id        = order_id,
            pay_platform_id = pay_platform_id,
            player_id       = player_id,
            retry_times     = 0,
            last_retry_time = quanta.now,
        }
        self.unconfirmed_order_map[order_id] = unconfirmed_info
    else
        unconfirmed_info.retry_times     = unconfirmed_info.retry_times + 1
        unconfirmed_info.last_retry_time = quanta.now
    end

    return KernCode.SUCCESS
end

-- 删除未确认订单
function RechargeMgr:remove_unconfirmed_order(order_id)
    self.unconfirmed_order_map[order_id] = nil
end

-- 更新未结束订单
function RechargeMgr:update_unfinish_order(order_id, player_id)
    log_warn("[RechargeMgr][update_unfinish_order] order_id=%s,player_id=%s", order_id, player_id)
    if not self.oid2oinfo[order_id] then
        log_warn("[RechargeMgr][update_unfinish_order] player_id=%s,order_id=%s", player_id, order_id)
        return RechargeCode.ORDER_ID_NOT_EXIST
    end

    local unfinish_info = self.unfinish_order_map[order_id]
    if not unfinish_info then
        unfinish_info = {
            player_id       = player_id,
            order_id        = order_id,
            retry_times     = 0,
            last_retry_time = quanta.now,
        }
        self.unfinish_order_map[order_id] = unfinish_info
    else
        unfinish_info.retry_times     = unfinish_info.retry_times + 1
        unfinish_info.last_retry_time = quanta.now
    end

    return KernCode.SUCCESS
end

-- 删除未完成订单
function RechargeMgr:remove_unfinish_order(order_id)
    log_warn("[RechargeMgr][remove_unfinish_order] order_id=%s", order_id)
    self.unfinish_order_map[order_id] = nil
end

-- 是否未完成订单
function RechargeMgr:is_unfinish_order(order_id)
    return self.unfinish_order_map[order_id] and true or false
end

-- 获取玩家未发货订单id列表
function RechargeMgr:get_player_ws_oids(player_id)
    local order_ids = {}
    for order_id in pairs(self.pid_2_wsend_oids[player_id] or {}) do
        tinsert(order_ids, order_id)
    end
    return order_ids
end

-- 从数据库加载代发货订单
function RechargeMgr:load_player_wait_send_orders(player_id)
    local selector = { player_id = player_id, state = RechargeOrderState.WAIT_SEND }
    local ok, result = recharge_dao:load_order(selector)
    if not ok then
        log_warn("[RechargeMgr][load_player_wait_send_orders] load player order from db faild: player_id=%s,result=%s", player_id, result)
    end
    for _, order_info in pairs(result or {}) do
        local order_id = order_info.order_id
        self.oid2oinfo[order_id] = order_info
        self.oid2minfo[order_id] = {load_time = quanta.now}
        self:_save_wait_send_order_id(player_id, order_id)
    end
end

-- 添加未发货订单id缓存
function RechargeMgr:_save_wait_send_order_id(player_id, order_id)
    local oids = self.pid_2_wsend_oids[player_id]
    if not oids then
        oids = {}
        self.pid_2_wsend_oids[player_id] = oids
    end
    oids[order_id] = true
end

-- 删除未发货订单id缓存
function RechargeMgr:_remove_wait_send_order_id(player_id, order_id)
    local oids = self.pid_2_wsend_oids[player_id]
    if oids then
        oids[order_id] = nil
    end
end

-- export
quanta.recharge_mgr = RechargeMgr()

return RechargeMgr
