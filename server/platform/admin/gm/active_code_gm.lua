--active_code_gm.lua
local log_info          = logger.info
local serialize         = logger.serialize

local gm_agent          = quanta.get("gm_agent")
local event_mgr         = quanta.get("event_mgr")
local active_code_mgr   = quanta.get("active_code_mgr")

local GMType            = enum("GMType")

local ActiveCodeGM = singleton()
function ActiveCodeGM:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_import_active_code")
    event_mgr:add_listener(self, "gm_search_active_code")
    event_mgr:add_listener(self, "gm_delete_active_code")

    --注册GM
    self:register()
end

function ActiveCodeGM:register()
    local cmd_list = {
        {gm_type = GMType.AREA, name = "gm_import_active_code", desc = "导入激活码GM", args = "area_id|number active_codes|table"},
        {gm_type = GMType.AREA, name = "gm_search_active_code", desc = "搜索激活码GM", args = "area_id|number state|number"},
        {gm_type = GMType.AREA, name = "gm_delete_active_code", desc = "删除激活码GM", args = "area_id|number active_codes|table"},
    }
    gm_agent:insert_cmd(cmd_list)
end

function ActiveCodeGM:gm_import_active_code(area_id, active_codes)
    log_info("[ActiveCodeGM][gm_import_active_code]area_id:%s, active_codes:%s", area_id, serialize(active_codes))
    local ok, fail_codes = active_code_mgr:import_active_code(active_codes)
    if not ok then
        return { code = 1, msg = "import_active_code failed" }
    end
    return { code = 0, data = { fail_codes = fail_codes } }
end

function ActiveCodeGM:gm_search_active_code(area_id, state)
    log_info("[ActiveCodeGM][gm_search_active_code]area_id:%s, state:%s", area_id, state)
    local ok, data = active_code_mgr:search_active_code(state)
    if not ok then
        return { code = 1, msg = "gm_search_active_code failed" }
    end
    return { code = 0, data = data }
end

function ActiveCodeGM:gm_delete_active_code(area_id, active_codes)
    log_info("[ActiveCodeGM][gm_delete_active_code]area_id:%s, active_codes:%s", area_id, serialize(active_codes))
    local ok, fail_codes = active_code_mgr:delete_active_code(active_codes)
    if not ok then
        return { code = 1, msg = "delete_active_code failed" }
    end
    return { code = 0, data = { fail_codes = fail_codes } }
end

-- export
quanta.active_code_gm = ActiveCodeGM()

return ActiveCodeGM