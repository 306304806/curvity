--account_limit_dao.lua

local log_err       = logger.err
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local db_agent      = quanta.get("db_agent")

local AccountlimitDao = singleton()
function AccountlimitDao:__init()
end

function AccountlimitDao:load_account_limit()
    local ok, code, res = db_agent:find(quanta.id, { "plat_account_limit", {}, {_id = 0}})
    if not ok or check_failed(code) then
        log_err("[AccountlimitDao][find_account_limit] failed: code: %s", code, serialize(res))
        return false
    end

    return true, res
end

function AccountlimitDao:update_account_limit(open_id, data)
    local ok, code, res = db_agent:update(quanta.id, { "plat_account_limit", data, {open_id = open_id}, true})
    if not ok or check_failed(code) then
        log_err("[AccountlimitDao][update_account_limit] failed! code: %s, res: %s", code, serialize(res))
        return false
    end

    return true
end

function AccountlimitDao:delete_account_limit(open_id)
    local ok, code, res = db_agent:delete(quanta.id, { "plat_account_limit", {open_id = open_id}, true})
    if not ok or check_failed(code) then
        log_err("[AccountlimitDao][delete_account_limit] failed! code: %s, res: %s", code, serialize(res))
        return false
    end

    return true
end

quanta.account_limit_dao = AccountlimitDao()

return AccountlimitDao
