--admin.lua

import("platform/admin/dao/account_limit_dao.lua")
import("platform/admin/dao/active_code_dao.lua")
import("platform/admin/dao/white_list_dao.lua")
import("platform/admin/dao/open_service_dao.lua")

import("platform/admin/object/account_limit_mgr.lua")
import("platform/admin/object/active_code_mgr.lua")
import("platform/admin/object/white_list_mgr.lua")
import("platform/admin/object/open_service_mgr.lua")

import("platform/admin/gm/mail_gm.lua")
import("platform/admin/gm/account_gm.lua")
import("platform/admin/gm/active_code_gm.lua")
import("platform/admin/gm/white_list_gm.lua")
import("platform/admin/gm/open_service_gm.lua")