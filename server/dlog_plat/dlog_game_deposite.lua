--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_game_deposite.lua
--付费充值
--luacheck: ignore 631

local sformat    = string.format
local log_err    = logger.err
local DlogGameDeposite = class()
local prop = property(DlogGameDeposite)
prop:accessor("table_name", "dlog_game_deposite")

--!!!以下是公共字段，调用时自动填充!!!
prop:accessor("has_public", true)
--[[(公共字段) 格式：YYYY-MM-DD HH:MM:SS (必填)]]
prop:accessor("event_time", "")
--[[(公共字段) 游戏申请的乐逗SDK APPKEY (必填)]]
prop:accessor("game_appkey", "")
--[[(公共字段) 游戏全局唯一区服ID (必填)]]
prop:accessor("zone_id", "")
--[[(公共字段) 对外玩家Id(注：数据来自乐逗SDK中的对外OpenId)，也要兼容非乐逗体系用户id (必填)]]
prop:accessor("open_id", "")
--[[(公共字段) SDK设备ID（SDK端可获取） (必填)]]
prop:accessor("sdk_device_id", "")
--[[(公共字段) android=1、ios=2、h5=3，pc=4 (必填)]]
prop:accessor("platform", "")
--[[(公共字段) 比如：华为、OPPO、VIVO、应用宝等,接了乐逗SDK则必须是从乐逗SDK获取的渠道商ID (必填)]]
prop:accessor("channel_group_id", "")
--[[(公共字段) 各渠道下的子渠道ID，比如华为1包、华为2包…接了乐逗SDK则必须是从乐逗SDK获取的channel_id (必填)]]
prop:accessor("channel_id", "")
--[[(公共字段) 游戏版本号 (必填)]]
prop:accessor("game_version", "")
--[[(公共字段) 全局唯一 (必填)]]
prop:accessor("character_id", "")
--[[(公共字段) 角色等级 (必填)]]
prop:accessor("character_level", "")
--[[(公共字段) 当前用户VIP等级 (必填)]]
prop:accessor("vip_level", "")


--!!!以下是特有字段，需调用方填充ctx!!!
--[[(独立字段) 所购买商品编号,  (必填)]]
prop:accessor("product_id", "")
--[[(独立字段) 所购买商品名称,全局唯一:1元宝,2金币,3钻石…  (选填)]]
prop:accessor("product_name", "")
--[[(独立字段) 所购买商品数量,  (必填)]]
prop:accessor("product_num", "")
--[[(独立字段) 折扣后实际充值金额,RMB,单位元，统计收入以此字段为准  (必填)]]
prop:accessor("deposit_amount", "")
--[[(独立字段) 折扣前金额,RMB,单位元  (必填)]]
prop:accessor("deposit_money", "")
--[[(独立字段) 内部订单号,乐逗订单号,与支付中心对账使用  (选填)]]
prop:accessor("inner_orderid", "")
--[[(独立字段) 外部订单号,方便外部对账用(一般指CP回传的订单号)  (选填)]]
prop:accessor("outer_orderid", "")
--[[(独立字段) 注意提取收入时，选取为3，支付成功的数据 (选填)]]
prop:accessor("order_status", "")
--[[(独立字段) 订单其它Json信息,如有  (选填)]]
prop:accessor("order_json_value", "")
--[[(独立字段) 累计付费总额,人民币，单位：元  (选填)]]
prop:accessor("total_cash", "")
--[[(独立字段) IMEI或IDFA,安卓：IMEI / IOS:IDFA  (必填)]]
prop:accessor("imei_idfa", "")
--[[(独立字段) IP,玩家支付时客户端IP地址  (必填)]]
prop:accessor("ip", "")
--[[(独立字段) 操作ID号,匹配同一操作产生的多条不同类型日志的操作ID(全局唯一)  (选填)]]
prop:accessor("opt_id", "")
--[[(独立字段) 上报商品ID (必填)]]
prop:accessor("exstr1", "")

function DlogGameDeposite:__init()
    self.special_fields = { ["product_id"] = true, ["product_name"] = true, ["product_num"] = true, ["deposit_amount"] = true, ["deposit_money"] = true, ["inner_orderid"] = true, ["outer_orderid"] = true, ["order_status"] = true, ["order_json_value"] = true, ["total_cash"] = true, ["imei_idfa"] = true, ["ip"] = true, ["opt_id"] = true, ["exstr1"] = true,}

end

function DlogGameDeposite:contruct_report_str(ctx)
    if not ctx then
        log_err("DlogGameDeposite report param error!")
        return
    end

    for field_name in pairs(self.special_fields) do
        if self.filter_fields and self.filter_fields[field_name] then
            self[field_name] = string.gsub(ctx.special_fields[field_name] or "", "|", "*")
        else
            self[field_name] = ctx.special_fields[field_name] or ""
        end
    end

    return sformat("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s", self.table_name,  self.event_time, self.game_appkey, self.zone_id, self.open_id, self.sdk_device_id, self.platform, self.channel_group_id, self.channel_id, self.game_version, self.character_id, self.character_level, self.vip_level, self.product_id, self.product_name, self.product_num, self.deposit_amount, self.deposit_money, self.inner_orderid, self.outer_orderid, self.order_status, self.order_json_value, self.total_cash, self.imei_idfa, self.ip, self.opt_id, self.exstr1)
end

return DlogGameDeposite