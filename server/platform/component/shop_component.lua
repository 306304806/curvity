--shop_component.lua


local otime             = os.time
local odate             = os.date
local tinsert           = table.insert
local uedition_utc      = utility.edition_utc

local player_dao        = quanta.get("player_dao")
local shop_mgr          = quanta.get("shop_mgr")
local config_mgr        = quanta.get("config_mgr")
local season_cache_mgr  = quanta.get("season_cache_mgr")

local LimitPurchaseType = enum("LimitPurchaseType")

local utility_db        = config_mgr:init_table("utility", "id")
local FLUSH_TIME        = utility_db:find_one("day_flush_time").value

local ShopComponent = mixin(
    "get_claim_count",              -- 获取本日索要次数
    "add_claim_apply_count",        -- 增加索要次数
    "is_limit_purchase",            -- 限购商品检查
    "pack_client_limit_buy",        -- 同步客户端限购数据
    "get_goods_bought_cnt",         -- 获取商品已购次数
    "purchase_limit_goods"          -- 购买限购商品
)

local prop = property(ShopComponent)
prop:accessor("limitation_purchase", {})            -- 玩家限购信息
prop:accessor("apply_claims",   {})                 -- 索要申请信息
prop:accessor("shop_dirty",     false)

-- 计算截止时间
local function calc_end_time(limit_type)
    if limit_type == LimitPurchaseType.DAY then
        return uedition_utc("day", otime(), FLUSH_TIME)
    elseif limit_type == LimitPurchaseType.WEEK then
        return uedition_utc("week", otime(), FLUSH_TIME)
    elseif limit_type == LimitPurchaseType.MONTH then
        return uedition_utc("month", otime(), FLUSH_TIME)
    elseif limit_type == LimitPurchaseType.BATTLEPASS then
        return season_cache_mgr:get_battlepass_season()
    end
    return -1
end

-- 检查截止时间
local function check_end_time(limit_type, uedition)
    if limit_type == LimitPurchaseType.DAY then
        return uedition == uedition_utc("day", otime(), FLUSH_TIME)
    elseif limit_type == LimitPurchaseType.WEEK then
        return uedition == uedition_utc("week", otime(), FLUSH_TIME)
    elseif limit_type == LimitPurchaseType.MONTH then
        return uedition == uedition_utc("month", otime(), FLUSH_TIME)
    elseif limit_type == LimitPurchaseType.BATTLEPASS then
        return uedition == season_cache_mgr:get_battlepass_season()
    end
    return uedition < 0
end

-- 加载玩家商城相关数据数据
local function load_shop_data(self, shop_res)
    local cur_time = otime()
    if shop_res and shop_res.shop_data then
        local limitation_purchase = shop_res.shop_data.limitation_purchase or {}
        for _, data in pairs(limitation_purchase) do
            local goods_cfg = shop_mgr:get_goods_cfg(data.goods_id)
            if goods_cfg then
                if check_end_time(goods_cfg.limit_type, data.end_time) then
                    data.limit_type = goods_cfg.limit_type
                    self.limitation_purchase[data.goods_id] = data
                end
            end
        end
        local date_info = odate("*t", cur_time)
        local mid_date = date_info.year * 10000 + date_info.month * 100 + date_info.day
        for _, data in pairs(shop_res.shop_data.apply_claims or {}) do
            for _, info in pairs(data.claims) do
                if info.mid_date == mid_date then
                    if not self.apply_claims[data.target_id] then
                        self.apply_claims[data.target_id] = {}
                    end
                    self.apply_claims[data.target_id][mid_date] = info.count
                end
            end
        end
    end
end

function ShopComponent:__init(player_id)
end

function ShopComponent:setup(db_res)
    local player_shop = db_res.plat_player_shop
    if not player_shop then
        return false
    end
    load_shop_data(self, player_shop.shop)
    return true
end

-- 检查商品是否限购
function ShopComponent:is_limit_purchase(goods_cfg, count)
    local limit_type = goods_cfg.limit_type
    if not limit_type or limit_type == LimitPurchaseType.NULL then
        return false
    end
    local id = goods_cfg.id
    local data = self.limitation_purchase[id]
    if data then
        if not check_end_time(limit_type, data.end_time) then
            self.limitation_purchase[id] = nil
            self.shop_dirty = true
            return goods_cfg.limits < count
        end
        return (goods_cfg.limits < data.count + count)
    else
        return goods_cfg.limits < count
    end
end

-- 购买限购商品
function ShopComponent:purchase_limit_goods(goods_cfg, count)
    local limit_type = goods_cfg.limit_type
    if not limit_type or limit_type == LimitPurchaseType.NULL then
        return
    end
    local id = goods_cfg.id
    local data = self.limitation_purchase[id]
    if not data then
        self.limitation_purchase[id] = {
            end_time = calc_end_time(limit_type),
            limit_type = limit_type,
            count = count
        }
    else
        data.count = data.count + count
    end
    self.shop_dirty = true
end

-- 同步客户端限购数据
function ShopComponent:pack_client_limit_buy()
    local pack_info = {}
    for goods_id, data in pairs(self.limitation_purchase) do
        if not check_end_time(data.limit_type, data.end_time) then
            self.limitation_purchase[goods_id] = nil
            self.shop_dirty = true
            tinsert(pack_info, {goods_id = goods_id, bought_cnt = 0})
        else
            tinsert(pack_info, {goods_id = goods_id, bought_cnt = data.count})
        end
    end
    return pack_info
end

-- 获取索要次数
function ShopComponent:get_claim_count(player_id)
    if not self.apply_claims[player_id] then
        return 0
    end
    local player_apply_claims = self.apply_claims[player_id]
    local date_info = odate("*t", otime())
    local mid_date = date_info.year * 10000 + date_info.month * 100 + date_info.day
    return player_apply_claims[mid_date] or 0
end

-- 增加索要次数
function ShopComponent:add_claim_apply_count(player_id)
    local date_info = odate("*t", otime())
    local mid_date = date_info.year * 10000 + date_info.month * 100 + date_info.day
    if not self.apply_claims[player_id] then
        self.apply_claims[player_id] = {}
    end
    local player_apply_claims = self.apply_claims[player_id]
    if not player_apply_claims[mid_date] then
        player_apply_claims[mid_date] = 1
    else
        player_apply_claims[mid_date] = player_apply_claims[mid_date] + 1
    end
    self.shop_dirty = true
end

-- 获取已购次数
function ShopComponent:get_goods_bought_cnt(goods_id)
    local data = self.limitation_purchase[goods_id]
    if data then
        return data.count
    else
        return 0
    end
end

function ShopComponent:update_data_2db()
    if not self.shop_dirty then
        return true
    end
    local row_data = {
        player_id = self.player_id,
        shop_data = {
            limitation_purchase = {},
            apply_claims = {},
        },
    }
    local shop_data = row_data.shop_data
    for goods_id, data in pairs(self.limitation_purchase) do
        tinsert(shop_data.limitation_purchase, {goods_id = goods_id, end_time = data.end_time, count = data.count})
    end
    for target_id, data in pairs(self.apply_claims) do
        local temp = {target_id = target_id, claims = {} }
        for mid_date, count in pairs(data) do
            tinsert(temp.claims, {mid_date = mid_date, count = count})
        end
        tinsert(shop_data.apply_claims, temp)
    end
    if player_dao:update_player_shop(self.player_id, row_data) then
        self.shop_dirty = false
        return true
    end
    return false
end

return ShopComponent
