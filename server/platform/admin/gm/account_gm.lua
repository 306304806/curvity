--account_gm.lua

local log_info          = logger.info

local gm_agent          = quanta.get("gm_agent")
local event_mgr         = quanta.get("event_mgr")
local account_limit_mgr = quanta.get("account_limit_mgr")

local GMType            = enum("GMType")

local AccountGM = singleton()
function AccountGM:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_account_lock")
    event_mgr:add_listener(self, "gm_get_account_limit")
    event_mgr:add_listener(self, "gm_player_lock")
    event_mgr:add_listener(self, "gm_chat_ban")

    --注册GM
    self:register()
end

function AccountGM:register()
    local cmd_list = {
        {gm_type = GMType.AREA, name = "gm_account_lock", desc = "封/解封账号GM", args = "area_id|number open_id|string time|number reason|string"},
        {gm_type = GMType.AREA, name = "gm_get_account_limit", desc = "查询账号限制信息GM", args = "area_id|number open_id|string"},
        {name = "gm_player_lock", desc = "封/解封角色GM", args = "player_id|number open_id|string time|number reason|string"},
        {name = "gm_chat_ban", desc = "禁言GM", args = "player_id|number open_id|string time|number reason|string"},
    }
    gm_agent:insert_cmd(cmd_list)
end

function AccountGM:gm_get_account_limit(area_id, open_id)
    log_info("[AccountGM][gm_get_account_limit]area_id:%s, open_id:%s", area_id, open_id)
    local data = account_limit_mgr:get_account_limit(open_id) or {}
    return { code = 0, data = data }
end

function AccountGM:gm_account_lock(area_id, open_id, time, reason)
    log_info("[AccountGM][gm_player_lock]area_id:%s, open_id:%s, time:%s, reason:%s", area_id, open_id, time, reason)
    local ok = account_limit_mgr:lock_account(open_id, time, reason)
    if not ok then
        return { code = 1, msg = "lock_account failed!"}
    end
    return { code = 0 }
end

function AccountGM:gm_player_lock(player_id, open_id, time, reason)
    log_info("[AccountGM][gm_player_lock]player_id:%s, open_id:%s, time:%s, reason:%s", player_id, open_id, time, reason)
    local ok = account_limit_mgr:lock_player(open_id, player_id, time, reason)
    if not ok then
        return { code = 1, msg = "lock_player failed!"}
    end
    return { code = 0 }
end

function AccountGM:gm_chat_ban(player_id, open_id, time, reason)
    log_info("[AccountGM][gm_chat_test]player_id:%s, open_id:%s, time:%s, reason:%s", player_id, open_id, time, reason)
    local ok = account_limit_mgr:ban_player_chat(open_id, player_id, time, reason)
    if not ok then
        return { code = 1, msg = "ban_player_chat failed!"}
    end
    return { code = 0 }
end

-- export
quanta.account_gm = AccountGM()

return AccountGM