--data_mgr.lua
local guid_group    = guid.group
local log_err       = logger.err
local log_info      = logger.info
local log_debug     = logger.debug
local serialize     = logger.serialize
local tunpack       = table.unpack
local env_number    = environ.number
local check_failed  = utility.check_failed

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local player_dao    = quanta.get("player_dao")
local thread_mgr    = quanta.get("thread_mgr")
local player_mgr    = quanta.get("player_mgr")

local PlatCommon    = enum("PlatCommon")
local PeriodTime    = enum("PeriodTime")

local DataMgr = singleton()
local prop = property(DataMgr)
prop:accessor("platforms", {})

function DataMgr:__init()
    self.area_id = env_number("QUANTA_AREA_ID")
    --监听gateway
    router_mgr:watch_service_ready(self, "gateway")
end

--节点注册
function DataMgr:on_service_ready(gate_id, service_name, router_id)
    thread_mgr:success_call(PeriodTime.SECOND_MS, function()
        local ok, code = router_mgr:router_call(router_id, gate_id, "rpc_platform_register", self.area_id, quanta.id)
        if ok and not check_failed(code) then
            log_info("DataMgr:on_server_ready->gate_id:%d, service_name:%s, %s", gate_id, service_name, router_id)
            return true
        end
        log_err("[DataMgr][on_service_ready] platform register faild: ok=%s, code=%s", ok, code)
        return false
    end)
end

--请求加载玩家数据
function DataMgr:get_player_data(player_id)
    local player = player_mgr:get_player(player_id)
    if player then
        return player:get_data()
    end
    local ok, data = player_dao:load_player_image(player_id)
    if ok and data and data.plat_player then
        return data.plat_player.player
    end
end

--搜索可添加好友信息
function DataMgr:search_friends(player_id, nick, target_id)
    if target_id > 0 then
        local player_data = self:get_player_data(target_id)
        return { player_data }
    else
        local search_res = player_dao:search_friends(player_id, nick, PlatCommon.SEARCH_COUNT)
        log_debug("[DataMgr][search_friend_images] search_res:%s", serialize(search_res))
        return search_res
    end
end

function DataMgr:find_platform_id(area_id, player_id)
    local platform_id = self.platforms[area_id]
    if not platform_id then
        local ok, quanta_id = router_mgr:call_gateway_hash(player_id, "rpc_platform_query", area_id)
        if not ok or not quanta_id then
            return
        end
        self.platforms[area_id] = quanta_id
        platform_id = quanta_id
    end
    return platform_id
end

--通知玩家处理消息
function DataMgr:notify_player(player_id, rpc, ...)
    local area_id = guid_group(player_id)
    if area_id == self.area_id then
        event_mgr:notify_listener(rpc, player_id, ...)
        return
    end
    local platform_id = self:find_platform_id(area_id, player_id)
    if platform_id then
        router_mgr:send_target(platform_id, rpc, player_id, ...)
    end
end

function DataMgr:call_player(player_id, rpc, ...)
    local area_id = guid_group(player_id)
    if area_id == self.area_id then
        local result = event_mgr:notify_listener(rpc, player_id, ...)
        return tunpack(result)
    end
    local platform_id = self:find_platform_id(area_id, player_id)
    if platform_id then
        return router_mgr:call_target(platform_id, rpc, player_id, ...)
    end
    return false
end

-- export
quanta.data_mgr = DataMgr()

return DataMgr
