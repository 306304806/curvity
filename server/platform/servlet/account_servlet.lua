-- account_servlet.lua
local new_guid          = guid.new
local env_number        = environ.number
local check_failed      = utility.check_failed

local auth_mgr          = quanta.get("auth_mgr")
local steam_api         = quanta.get("steam_api")
local event_mgr         = quanta.get("event_mgr")
local thread_mgr        = quanta.get("thread_mgr")
local account_dao       = quanta.get("account_dao")
local account_limit_mgr = quanta.get("account_limit_mgr")

local AccountPlatform   = enum("AccountPlatform")
local AccountCode       = enum("AccountCode")
local SSCmdID           = enum("PlatSSCmdID")
local KernCode          = enum("KernCode")

local SUCCESS           = KernCode.SUCCESS

local AccountServlet = singleton()
function AccountServlet:__init()
    self.area_id = env_number("QUANTA_AREA_ID")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_LOGIN_ACCOUNT_REQ, "on_ss_login_account_req")

end

-- 生成新的open_id
function AccountServlet:new_account_id()
    return new_guid(quanta.index, self.area_id)
end

function AccountServlet:on_ss_login_account_req(rpc_req)
    local platform_id, open_id, area_id, access_token = rpc_req.platform_id, rpc_req.open_id, rpc_req.area_id, rpc_req.access_token
    -- 黑名单永远返回被ban
    if account_limit_mgr:account_is_lock(open_id) then
        return AccountCode.ACCOUNT_BAN_ERR
    end
    -- 外部平台验证
    local code1 = self:verify_open_id(platform_id, open_id, access_token)
    if check_failed(code1) then
        return { code = code1 }
    end
    -- 尝试加载本地账号
    local ok, account = account_dao:load_account(open_id)
    if not ok then
        return { code = KernCode.MONGO_FAILED }
    end
    -- 需要创建新账号
    if not account then
        local code, info = self:register_account(platform_id, open_id, area_id, access_token, rpc_req.active_code)
        if check_failed(code) then
            return { code = code }
        end
        account = info
    else
        if account.platform_id ~= platform_id then
            return { code = AccountCode.PLAT_OPEN_ID_ERR }
        end
    end
    -- 未成年防沉迷相关检查
    if auth_mgr:is_teenager_player(open_id) then
        if auth_mgr:is_teen_forbid_login() then
            return { code = AccountCode.TEEN_FORBID_LOGIN }
        end
        -- 今日累计时间是否达到上限
        if auth_mgr:today_gametime_limit(open_id) then
            return { code = AccountCode.TODAY_GAME_TIME_OVER }
        end
    end
    return { code = SUCCESS, account_id = account.account_id }
end

function AccountServlet:register_account(platform_id, open_id, area_id, access_token, active_code)
    local account = {
        area_id     = area_id,
        open_id     = open_id,
        create_time = quanta.now,
        platform_id = platform_id,
        account_id  = self:new_account_id(),
    }
    -- 完成本地数据库账号落库
    local ok = account_dao:upset_account(open_id, account)
    if not ok then
        return KernCode.MONGO_FAILED
    end
    thread_mgr:fork(function()
        event_mgr:notify_trigger("evt_register_account", account)
    end)
    return SUCCESS, account
end

-- 验证open_id和access_token
function AccountServlet:verify_open_id(platform_id, open_id, access_token)
    if platform_id == AccountPlatform.GUEST then
        return self:verify_guest_open_id(open_id)
    elseif platform_id == AccountPlatform.STEAM then
        return steam_api:auth_user_token(open_id, access_token)
    end
    return KernCode.LOGIC_FAILED
end

-- 游客ID验证
function AccountServlet:verify_guest_open_id(open_id)
    return SUCCESS
end

-- Steam验证
function AccountServlet:verify_steam_open_id(open_id, token)
    return steam_api:auth_user_token(open_id, token)
end

-- export
quanta.account_servlet = AccountServlet()

return AccountServlet