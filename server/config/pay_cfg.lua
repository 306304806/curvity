--pay_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local pay = config_mgr:get_table("pay")

--导出版本号
pay:set_version(10000)

--导出配置内容
pay:upsert({
    commodity_id = 1,
    name = '250理想币',
    icon_pay = '1',
    amount = 2500,
    currency = 'CNY',
    items = {{item_id=1,item_count=250}},
})

pay:upsert({
    commodity_id = 2,
    name = '500理想币',
    desc = '+50理想币',
    icon_pay = '2',
    amount = 5000,
    currency = 'CNY',
    items = {{item_id=1,item_count=550}},
})

pay:upsert({
    commodity_id = 3,
    name = '1000理想币',
    desc = '+120理想币',
    icon_pay = '3',
    amount = 10000,
    currency = 'CNY',
    items = {{item_id=1,item_count=1120}},
})

pay:upsert({
    commodity_id = 4,
    name = '2000理想币',
    desc = '+250理想币',
    icon_pay = '4',
    amount = 20000,
    currency = 'CNY',
    items = {{item_id=1,item_count=2250}},
})

pay:upsert({
    commodity_id = 5,
    name = '5000理想币',
    desc = '+700理想币',
    icon_pay = '5',
    amount = 50000,
    currency = 'CNY',
    items = {{item_id=1,item_count=5700}},
})
