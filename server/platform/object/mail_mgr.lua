--mail_mgr.lua
local log_err       = logger.err
local log_info      = logger.info
local guid_new      = guid.new
local tinsert       = table.insert
local tcopy         = table_ext.copy
local env_number    = environ.number

local mail_dao      = quanta.get("mail_dao")
local rmsg_mail     = quanta.get("rmsg_mail")
local data_mgr      = quanta.get("data_mgr")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local thread_mgr    = quanta.get("thread_mgr")

local MailCode      = enum("MailCode")
local MailType      = enum("MailType")
local MailState     = enum("MailState")
local AttachState   = enum("AttachState")
local RmsgPlat      = enum("RmsgPlat")
local DBLoading     = enum("DBLoading")
local PeriodTime    = enum("PeriodTime")

local MailMgr = singleton()
local prop = property(MailMgr)
prop:accessor("area_id", 0)
prop:accessor("global_mails", {})
prop:accessor("gmail_loading", DBLoading.INIT)

function MailMgr:__init()
    self.area_id = env_number("QUANTA_AREA_ID")
    router_mgr:watch_service_ready(self, "dbsvr")
end

-- 根据邮件id获取全局邮件
function MailMgr:get_global_mail(uuid)
    return self.global_mails[uuid]
end

--玩家收取邮件
function MailMgr:recv_mail(player, player_id, gmail)
    local mail_data = tcopy(gmail)
    mail_data.mail_id = guid_new()
    mail_data.attach_state = AttachState.UNTAKE
    mail_data.mail_state = MailState.UNREADED
    if mail_data.type == MailType.GLOBAL_MAIL then
        mail_data.src_player_id = 0
        mail_data.tar_player_id = player_id
    end
    player:add_mail(mail_data)
    return mail_data
end

--发送邮件
function MailMgr:send_mail(mail_data, mail_type)
    mail_data.send_time = quanta.now
    mail_data.type = mail_type or MailType.PLAYER_MAIL
    local rmsg = { msg = "do_mail_recvive", data = mail_data }
    local ok = rmsg_mail:send_message(quanta.name, mail_data.tar_player_id, RmsgPlat.PLAT_MAIL, rmsg )
    if not ok then
        log_err("[MailMgr][send_mail] rmsg send faild: player_id=%s", mail_data.src_player_id)
        return { MailCode.ERR_MAIL_RMSG_FAILED }
    end
    -- 通知对端处理离线邮件
    data_mgr:notify_player(mail_data.tar_player_id, "do_mail_message")
end

-- 添加全局邮件
function MailMgr:send_global_mail(global_mail, is_store)
    local uuid = guid_new()
    global_mail.uuid = uuid
    global_mail.send_time = quanta.now
    global_mail.type = MailType.GLOBAL_MAIL
    if is_store then
        local ok = mail_dao:insert_global_mail(self.area_id, global_mail)
        if not ok then
            log_err("[MailMgr][send_global_mail] insert global mail failed!")
            return false
        end
        if global_mail.area_id == 0 then
            --全区邮件广播
            router_mgr:call_platform_all("rpc_global_mail", global_mail)
        end
    end
    self.global_mails[uuid] = global_mail
    event_mgr:notify_listener("evt_global_mail", uuid, self.area_id)
    log_info("[MailMgr][send_global_mail] insert global mail success!")
    return true
end

--删除全局邮件
function MailMgr:delete_global_mail(mail_uuid)
    self.global_mails[mail_uuid] = nil
    mail_dao:delete_global_mail(self.area_id, mail_uuid)
    log_info("[MailMgr][delete_global_mail] delete global mail: %s success!", mail_uuid)
end

-- 获取玩家的新全局邮件
function MailMgr:get_player_global_mails(last_query_time)
    if self.gmail_loading == DBLoading.SUCCESS then
        local gmails = {}
        for _, global_mail in pairs(self.global_mails or {}) do
            if global_mail.send_time > last_query_time then
                tinsert(gmails, global_mail)
            end
        end
        return true, gmails
    end
    return false
end

--db连接成功
function MailMgr:on_service_ready(quanta_id, service_name)
    if self.gmail_loading == DBLoading.INIT then
        self.gmail_loading = DBLoading.LOADING
        thread_mgr:success_call(PeriodTime.SECOND_MS, function()
            local ok, gmails = mail_dao:load_global_mails(self.area_id)
            if not ok then
                return false
            end
            for _, gmail in pairs(gmails or {}) do
                local uuid = gmail.uuid
                if gmail.area_id == 0 or gmail.area_id == self.area_id then
                    gmail.uuid = nil
                    gmail.area_id = nil
                    self.global_mails[uuid] = gmail
                end
            end
            self.gmail_loading = DBLoading.SUCCESS
            log_info("[MailMgr][on_service_ready] load global mails success cnt=%s", #gmails)
            return true
        end)
    end
end

-- export
quanta.mail_mgr = MailMgr()

return MailMgr