--cooperate_dao.lua

local log_err       = logger.err
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local db_agent      = quanta.get("db_agent")

local DBGroup       = enum("DBGroup")

local GLOBAL_DB     = 1

local CooperateDao = singleton()
function CooperateDao:__init()
end

function CooperateDao:load_area_list()
    local query = {"area_list", {}, {_id = 0}}
    local ok, code, res = db_agent:find(quanta.id, query, DBGroup.GLOBAL, GLOBAL_DB)
    if not ok or check_failed(code) then
        log_err("[CooperateDao][load_area_list] failed: code: %s, res:%s", code, serialize(res))
        return false
    end
    return true, res or {area_id = self.area_id}
end

function CooperateDao:update_area_info(area_id, data)
    local query = {"area_list", data, {area_id = area_id}, true}
    local ok, code, res = db_agent:update(quanta.id, query, DBGroup.GLOBAL, GLOBAL_DB)
    if not ok or check_failed(code) then
        log_err("[CooperateDao][update_area_info] failed! code: %s, res: %s", code, serialize(res))
        return false
    end
    return true
end

function CooperateDao:delete_area(area_id)
    local query = {"area_list", {area_id = area_id}, true}
    local ok, code, res = db_agent:delete(quanta.id, query, DBGroup.GLOBAL, GLOBAL_DB)
    if not ok or check_failed(code) then
        log_err("[CooperateDao][update_area_info] failed! code: %s, res: %s", code, serialize(res))
        return false
    end

    return true
end

quanta.cooperate_dao = CooperateDao()

return CooperateDao
