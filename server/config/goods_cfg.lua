--goods_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local goods = config_mgr:get_table("goods")

--导出版本号
goods:set_version(10000)

--导出配置内容
goods:upsert({
    id = 1,
    name = '改名卡',
    desc = '改名卡',
    icon = '10001',
    quality = 1,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 100,
    items = {{item_id=10001,item_amount=1}},
    present_available = true,
    limit_type = 0,
    limits = 0,
})

goods:upsert({
    id = 2,
    name = '双倍金币卡：1小时',
    desc = '双倍金币卡：1小时',
    icon = '10101',
    quality = 2,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 101,
    items = {{item_id=10101,item_amount=1}},
    present_available = false,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 3,
    name = '双倍金币卡：2小时',
    desc = '双倍金币卡：2小时',
    icon = '10101',
    quality = 3,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 101,
    items = {{item_id=10102,item_amount=1}},
    present_available = true,
    limit_type = 0,
})

goods:upsert({
    id = 4,
    name = '双倍经验卡：1小时',
    desc = '双倍经验卡：1小时',
    icon = '10101',
    quality = 4,
    price = {{currency_id=1,currency_amount=588},{currency_id=3,currency_amount=188}},
    item_type = 102,
    items = {{item_id=10201,item_amount=1}},
    present_available = true,
    limit_type = 0,
})

goods:upsert({
    id = 5,
    name = '双倍经验卡：2小时',
    desc = '双倍经验卡：2小时',
    icon = '10101',
    quality = 5,
    price = {{currency_id=1,currency_amount=588},{currency_id=3,currency_amount=188}},
    item_type = 102,
    items = {{item_id=10202,item_amount=1}},
    present_available = true,
    limit_type = 0,
})

goods:upsert({
    id = 6,
    name = '双倍武器经验卡：1小时',
    desc = '双倍武器经验卡：1小时',
    icon = '10101',
    quality = 1,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 103,
    items = {{item_id=10301,item_amount=1}},
    present_available = true,
    limit_type = 1,
    limits = 2,
})

goods:upsert({
    id = 7,
    name = '双倍武器经验卡：2小时',
    desc = '双倍武器经验卡：2小时',
    icon = '10101',
    quality = 2,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 103,
    items = {{item_id=10302,item_amount=1}},
    present_available = true,
    limit_type = 2,
    limits = 3,
})

goods:upsert({
    id = 8,
    name = 'M4A1',
    desc = 'M4A1',
    icon = '10101001',
    quality = 3,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 3,
    items = {{item_id=10101002,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 9,
    name = 'AKM',
    desc = 'AKM',
    icon = '10102001',
    quality = 4,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 3,
    items = {{item_id=10102001,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 10,
    name = 'MP5A5',
    desc = 'MP5A5',
    icon = '10501001',
    quality = 5,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 3,
    items = {{item_id=10501001,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 11,
    name = 'USP45',
    desc = 'USP45',
    icon = '12301001',
    quality = 1,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 3,
    items = {{item_id=12301002,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 12,
    name = 'M249',
    desc = 'M249',
    icon = '10402001',
    quality = 2,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 3,
    items = {{item_id=10402001,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 13,
    name = '沙鹰',
    desc = '沙鹰',
    icon = '12303001',
    quality = 3,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 3,
    items = {{item_id=12303001,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 14,
    name = 'MG42',
    desc = 'MG42',
    icon = '10403001',
    quality = 4,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 3,
    items = {{item_id=10403001,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 15,
    name = 'SCAR-H',
    desc = 'SCAR-H',
    icon = '10108001',
    quality = 5,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 3,
    items = {{item_id=10108001,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 16,
    name = '彩溶手雷',
    desc = '彩溶手雷',
    icon = '18101001',
    quality = 1,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 3,
    items = {{item_id=18101001,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 17,
    name = '闪光弹',
    desc = '闪光弹',
    icon = '18201001',
    quality = 2,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 3,
    items = {{item_id=18201001,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 18,
    name = '新年礼包',
    desc = '新年礼包',
    icon = '10101',
    quality = 3,
    price = {{currency_id=1,currency_amount=588}},
    item_type = 200,
    items = {{item_id=20001,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 101,
    name = '米雪儿・ 李',
    desc = '米雪儿・ 李',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 2,
    items = {{item_id=101,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 105,
    name = '奥黛丽',
    desc = '奥黛丽',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 2,
    items = {{item_id=105,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 132,
    name = '明',
    desc = '明',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 2,
    items = {{item_id=132,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 107,
    name = '玛德蕾娜',
    desc = '玛德蕾娜',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588},{currency_id=1,currency_amount=888}},
    item_type = 2,
    items = {{item_id=107,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 128,
    name = '拉薇',
    desc = '拉薇',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588},{currency_id=1,currency_amount=888}},
    item_type = 2,
    items = {{item_id=128,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 108,
    name = '奥黛丽-AWM',
    desc = '奥黛丽-AWM',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588},{currency_id=1,currency_amount=888}},
    item_type = 2,
    items = {{item_id=108,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20101002,
    name = '米雪儿・李・绯红行动',
    desc = '米雪儿・李・绯红行动',
    icon = '1010102',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20101002,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20101003,
    name = '米雪儿・李・落樱缤纷',
    desc = '米雪儿・李・落樱缤纷',
    icon = '1010102',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20101003,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20101004,
    name = '米雪儿・李・露草微光',
    desc = '米雪儿・李・露草微光',
    icon = '1010102',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20101004,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20105002,
    name = '奥黛丽・白蔷薇',
    desc = '奥黛丽・白蔷薇',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20105002,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20105003,
    name = '奥黛丽・绿野仙踪',
    desc = '奥黛丽・绿野仙踪',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20105003,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20105004,
    name = '奥黛丽・天文时计',
    desc = '奥黛丽・天文时计',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20105004,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20105005,
    name = '奥黛丽・维多利亚时代',
    desc = '奥黛丽・维多利亚时代',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20105005,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20105006,
    name = '奥黛丽・阿瓦隆之誓',
    desc = '奥黛丽・阿瓦隆之誓',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20105006,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20105007,
    name = '奥黛丽・夏日沙滩',
    desc = '奥黛丽・夏日沙滩',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20105007,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20105008,
    name = '奥黛丽・冬日暖阳',
    desc = '奥黛丽・冬日暖阳',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20105008,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 20105009,
    name = '奥黛丽・青春校园',
    desc = '奥黛丽・青春校园',
    icon = '1010101',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 4,
    items = {{item_id=20105009,item_amount=1}},
    present_available = true,
    limit_type = 4,
    limits = 1,
})

goods:upsert({
    id = 19001,
    name = '基础搜查令',
    desc = '欧泊小队成员持有的基础搜查令',
    icon = '10001',
    quality = 5,
    price = {{currency_id=3,currency_amount=0}},
    item_type = 190,
    items = {{item_id=19001,item_amount=1}},
    present_available = false,
    limit_type = 0,
})

goods:upsert({
    id = 19101,
    name = '高级搜查令',
    desc = '比起基础搜查令具有更高的调查权限',
    icon = '10001',
    quality = 5,
    price = {{currency_id=3,currency_amount=588}},
    item_type = 191,
    items = {{item_id=19101,item_amount=1}},
    present_available = false,
    limit_type = 5,
    limits = 1,
})

goods:upsert({
    id = 19201,
    name = '高级搜查令·资深',
    desc = '行动现场拾获的高级搜查令，已采集部分线索',
    icon = '10001',
    quality = 5,
    price = {{currency_id=3,currency_amount=1788}},
    item_type = 192,
    items = {{item_id=19201,item_amount=1}},
    present_available = false,
    limit_type = 5,
    limits = 1,
})

goods:upsert({
    id = 19301,
    name = '探索点数',
    desc = '解锁相应级别调查权限的象征点数',
    icon = '10001',
    quality = 5,
    price = {{currency_id=3,currency_amount=1}},
    item_type = 193,
    items = {{item_id=19301,item_amount=1}},
    present_available = false,
    limit_type = 0,
})
