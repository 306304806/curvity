--open_service_dao.lua

local log_err       = logger.err
local serialize     = logger.serialize
local env_number    = environ.number
local check_failed  = utility.check_failed

local db_agent      = quanta.get("db_agent")

local OpenServiceDao = singleton()
function OpenServiceDao:__init()
    self.area_id = env_number("QUANTA_AREA_ID")
end

function OpenServiceDao:load_open_service()
    local ok, code, res = db_agent:find_one(quanta.id, { "plat_open_service", {area_id = self.area_id}, {_id = 0}})
    if not ok or check_failed(code) then
        log_err("[OpenServiceDao][load_open_service] failed: code: %s", code, serialize(res))
        return false
    end

    return true, res or {area_id = self.area_id}
end

function OpenServiceDao:update_open_service(data)
    local ok, code, res = db_agent:update(quanta.id, { "plat_open_service", data, {area_id = self.area_id}, true})
    if not ok or check_failed(code) then
        log_err("[OpenServiceDao][update_open_service] failed! code: %s, res: %s", code, serialize(res))
        return false
    end

    return true
end

quanta.open_service_dao = OpenServiceDao()

return OpenServiceDao
