---cooperate_mgr.lua
local log_err       = logger.err
local tunpack       = table.unpack
local guid_group    = guid.group

local event_mgr     = quanta.get("event_mgr")
local timer_mgr     = quanta.get("timer_mgr")
local router_mgr    = quanta.get("router_mgr")

local GMType        = enum("GMType")
local PeriodTime    = enum("PeriodTime")
local AdminMgr      = import("kernel/admin/admin_mgr.lua")

local CooperateMgr = singleton(AdminMgr)
local prop = property(CooperateMgr)
prop:accessor("platforms", {})

function CooperateMgr:__init()
    event_mgr:add_listener(self, "web_gm_test")
    event_mgr:add_listener(self, "rpc_gm_dispatch")

    --注册GM指令
    timer_mgr:once(PeriodTime.SECOND_MS, function()
        local cmd_list = {
            {gm_type = GMType.GLOBAL, name = "web_gm_test", desc = "发送邮件", args = "src_id|number tar_id|number title|string content|string"},
            {gm_type = GMType.GLOBAL, name = "gm_update_area_list", desc = "更新小区列表GM", args = "area_list|table"},
            {gm_type = GMType.GLOBAL, name = "gm_search_area_list", desc = "搜索小区列表GM", args = ""},
            {gm_type = GMType.GLOBAL, name = "gm_delete_area_list", desc = "删除小区列表GM", args = "area_list|table"},
        }
        self:report_cmd(cmd_list, quanta.id)
    end)
end

function CooperateMgr:rpc_gm_dispatch(cmd_args, gm_type)
    if gm_type then
        if gm_type == GMType.GLOBAL then
            return self:exec_global_cmd(tunpack(cmd_args))
        elseif gm_type == GMType.AREA then
            return self:exec_area_cmd(tunpack(cmd_args))
        end
    else
        return self:exec_player_cmd(tunpack(cmd_args))
    end
    return {code = 1, msg = "err command"}
end

function CooperateMgr:exec_global_cmd(cmd_name, ...)
    local ok, res = tunpack(event_mgr:notify_listener(cmd_name, ...))
    if not ok then
        return {code = 1, msg = res}
    end
    return res
end

function CooperateMgr:exec_area_cmd(cmd_name, area_id, ...)
    if not area_id then
        return {code = 1, msg = "area_id not exist!"}
    end
    return self:forward_platform_cmd(cmd_name, area_id, area_id, ...)
end

function CooperateMgr:exec_player_cmd(cmd_name, player_id, ...)
    if not player_id then
        return {code = 1, msg = "player_id not exist!"}
    end
    return self:forward_platform_cmd(cmd_name, guid_group(player_id), player_id, ...)
end

function CooperateMgr:forward_platform_cmd(cmd_name, area_id, ...)
    local platform_id = self.platforms[area_id]
    if not platform_id then
        local ok_1, quanta_id = router_mgr:call_gateway_hash(area_id, "rpc_platform_query", area_id)
        if not ok_1 or not quanta_id then
            log_err("[CooperateMgr][exec_player_cmd] rpc_platform_query failed! area_id=%s", area_id)
            return {code = 1, msg = "find platform failed!" }
        end
        self.platforms[area_id] = quanta_id
        platform_id = quanta_id
    end
    local ok_2, res = router_mgr:call_target(platform_id, "rpc_gm_execute", cmd_name, ...)
    if not ok_2 then
        log_err("[CooperateMgr][exec_player_cmd] rpc_gm_execute failed! area_id=%s", area_id)
        return {code = 1, msg = "exec gm failed!" }
    end
    return res
end

function CooperateMgr:web_gm_test(src_id, tar_id, title, content)
    return "success"
end

quanta.cooperate_mgr = CooperateMgr()

return CooperateMgr
