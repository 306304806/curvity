--rankseason_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local rankseason = config_mgr:get_table("rankseason")

--导出版本号
rankseason:set_version(10000)

--导出配置内容
rankseason:upsert({
    id = 1,
    start = 1613599200,
    finish = 1618696799,
})

rankseason:upsert({
    id = 2,
    start = 1618696800,
    finish = 1623967199,
})

rankseason:upsert({
    id = 3,
    start = 1623967200,
    finish = 1629237599,
})
