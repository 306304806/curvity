--mail_servlet.lua
-- 全局邮件管理器,用于即使推送
local log_info      = logger.info
local log_warn      = logger.warn
local tinsert       = table.insert
local tjoin         = table_ext.join

local CSCmdID       = ncmd_plat.CCmdId
local SSCmdID       = enum("PlatSSCmdID")

local MailCode      = enum("MailCode")
local KernCode      = enum("KernCode")
local DlogMailState = enum("DlogMailState")
local SUCCESS       = KernCode.SUCCESS

local mail_mgr      = quanta.get("mail_mgr")
local dlog_mgr      = quanta.get("dlog_mgr")
local event_mgr     = quanta.get("event_mgr")
local rmsg_mail     = quanta.get("rmsg_mail")
local player_mgr    = quanta.get("player_mgr")

local MailServlet = singleton()
function MailServlet:__init()
    --rpc
    event_mgr:add_trigger(self, "on_client_login")
    --event
    event_mgr:add_listener(self, "evt_global_mail")         -- 处理全局邮件推送
    event_mgr:add_listener(self, "rpc_global_mail")         -- 处理全局邮件
    --rmsg
    event_mgr:add_listener(self, "do_mail_recvive")         -- 处理收到邮件
    event_mgr:add_listener(self, "do_mail_message")         -- 处理离线邮件信息
    --cs
    event_mgr:add_cmd_listener(self, CSCmdID.NID_MAIL_SEND_REQ, "on_mail_send_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_MAIL_READ_REQ, "on_mail_read_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_MAIL_LOAD_REQ, "on_mail_load_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_MAIL_DELETE_REQ, "on_mail_delete_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_MAIL_ATTACH_TAKE_REQ, "on_mail_attach_take_req")
    --ss
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_MAIL_SEND_REQ, "on_ss_mail_send")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_MAIL_ATTACH_DEL_REQ, "on_ss_mail_attach_del")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_MAIL_ATTACH_GET_REQ, "on_ss_mail_attach_get")
end

function MailServlet:on_client_login(player, player_id)
    -- 领取离线邮件
    self:do_mail_message(player_id)
    -- 群邮件获取
    local ok, gmails = mail_mgr:get_player_global_mails(player:get_mail_query_time())
    if not ok then
        log_warn("[MailServlet][on_client_login] get player global mails failed player_id=%s", player_id)
        return
    end
    log_info("[MailServlet][on_client_login] global mails: player_id=%s, mails=%s", player_id, logger.serialize(gmails))
    player:update_mail_query_time()
    -- 全局邮件放到自己身上
    for _, gmail in pairs(gmails or {}) do
        mail_mgr:recv_mail(player, player_id, gmail)
    end
    player:send(CSCmdID.NID_MAIL_SYNC_NTF, { mails = player:get_mails() })
end

--客户端发送邮件
function MailServlet:on_mail_send_req(player, player_id, data)
    if 0 == data.tar_player_id then
        return { code = MailCode.ERR_MAIL_TAR_NOT_EXIST }
    end
    data.src_player_id = player_id
    data.src_player_name = player:get_nick()
    mail_mgr:send_mail(data)
    return { code = SUCCESS }
end

--客户端领取全局邮件
function MailServlet:on_mail_load_req(player, player_id, data)
    local mail_uuid = data.mail_uuid
    local gmail = mail_mgr:get_global_mail(mail_uuid)
    if gmail then
        local mail = mail_mgr:recv_mail(player, player_id, gmail)
        player:send(CSCmdID.NID_MAIL_RECV_NTF, { mail = mail })
        player:update_mail_query_time()
        log_info("[MailServlet][on_mail_load_req] recv global mail: player_id=%s, mail_uuid=%s", player_id, mail_uuid)
    end
    return { code = SUCCESS }
end

-- 客户端请求读邮件
function MailServlet:on_mail_read_req(player, player_id, data)
    local read_mails = {}
    for _, mail_id in pairs(data.mail_ids or {}) do
        if player:read_mail(mail_id) then
            tinsert(read_mails, mail_id)
            self:report_dlog_mail(player, mail_id, DlogMailState.READ)
            log_info("[MailServlet][on_mail_read_req] read finish: player_id=%s, mail_id=%s", player_id, mail_id)
        end
    end
    return { code = SUCCESS, mail_ids = read_mails }
end

-- 客户端请求删邮件
function MailServlet:on_mail_delete_req(player, player_id, data)
    local delete_ids = {}
    for _, mail_id in pairs(data.mail_ids or {}) do
        if player:delete_mail(mail_id) then
            tinsert(delete_ids, mail_id)
            log_info("[MailServlet][on_mail_delete_req] delete finish: player_id=%s, mail_id=%s", player_id, mail_id)
        end
    end
    return { code = SUCCESS, mail_ids = delete_ids }
end

-- 客户端领取附件
function MailServlet:on_mail_attach_take_req(player, player_id, data)
    local all_attach_items = {}
    for _, mail_id in pairs(data.mail_ids or {}) do
        local ok, attach_items = player:take_mail_attach(mail_id)
        if ok then
            tjoin(attach_items, all_attach_items)
            self:report_dlog_mail(player, mail_id, DlogMailState.RECIVED)
            log_info("[MailServlet][on_mail_attach_take_req] take finish: player_id=%s, mail_id=%s", player_id, mail_id)
        end
    end
    --通知服务器来领取附件
    event_mgr:notify_listener("rpc_subscribe_dispatch", "evt_mail_attach_take", { player_id = player_id })
    return { code = SUCCESS, attach_items = all_attach_items }
end

-- 服务器请求发送邮件
function MailServlet:on_ss_mail_send(rpc_req)
    local mail_info = rpc_req.mail_info
    if  mail_info.tar_player_id == 0 then
        -- 全局邮件处理
        log_info("[MailServlet][rpc_mail_send] global_mail_title=%s", mail_info.title)
        if mail_mgr:send_global_mail(mail_info, true) then
            return { SUCCESS }
        end
        return { MailCode.ERR_MAIL_RMSG_FAILED }
    end
    -- 单发邮件处理
    mail_mgr:send_mail(mail_info)
    log_info("[MailServlet][rpc_mail_send] mail_title=%s", mail_info.title)
    return { code = SUCCESS }
end

-- 服务器请求领取邮件附件
function MailServlet:on_ss_mail_attach_get(rpc_req)
    local player_id = rpc_req.player_id
    local player = player_mgr:get_player(player_id)
    if not player then
        return { code = KernCode.PLAYER_NOT_EXIST }
    end
    return { code = SUCCESS, attach_items = player:get_attach_items() }
end

-- 服务器请求删除邮件附件
function MailServlet:on_ss_mail_attach_del(rpc_req)
    local player_id = rpc_req.player_id
    local player = player_mgr:get_player(player_id)
    if not player then
        return { code = KernCode.PLAYER_NOT_EXIST }
    end
    for _, mail_id in pairs(rpc_req.mail_ids or {}) do
        player:del_mail_attach(mail_id)
    end
    return { code = SUCCESS }
end

-- rmsg 收到新邮件
function MailServlet:do_mail_recvive(player, data)
    local mail = mail_mgr:recv_mail(player, data)
    player:send(CSCmdID.NID_MAIL_RECV_NTF, { mail = mail })
    self:report_dlog_mail(player, data.mail_id, DlogMailState.NEW)
end

-- 进程内部事件： 处理新邮件
function MailServlet:do_mail_message(player_id)
    local player = player_mgr:get_player(player_id)
    if player then
        -- rmsg中的邮件处理
        local records = rmsg_mail:list_message(player_id)
        for _, record in pairs(records or {}) do
            local body = record.body
            event_mgr:notify_listener(body.msg, player, body.data)
            rmsg_mail:delete_message(player_id, record.uuid)
        end
    end
end

-- 处理的本区全局邮件
function MailServlet:rpc_global_mail(mail_info)
    mail_mgr:send_global_mail(mail_info)
end

-- 处理新的全局邮件
function MailServlet:evt_global_mail(mail_uuid, area_id)
    -- 通知全部的目标客户端有新的邮件需要接收
    player_mgr:boardcast_message(CSCmdID.NID_MAIL_GLOBAL_NTF, { mail_uuid = mail_uuid }, area_id)
end

-- 精分上报-邮件
function MailServlet:report_dlog_mail(player, mail_id, opt_type)
    local mail = player:get_mail(mail_id)
    if mail then
        local special_fields = {}
        special_fields.mail_id     = mail_id
        special_fields.action_type = opt_type
        special_fields.mail_type   = mail.type
        special_fields.mail_name   = mail.title
        dlog_mgr:send_dlog_game_mail({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
    end
end

-- export
quanta.mail_servlet = MailServlet()

return MailServlet