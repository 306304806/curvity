--functionunlock_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local functionunlock = config_mgr:get_table("functionunlock")

--导出版本号
functionunlock:set_version(10000)

--导出配置内容
functionunlock:upsert({
    id = 10,
    player_level = 1,
})

functionunlock:upsert({
    id = 20,
    player_level = 1,
})

functionunlock:upsert({
    id = 21,
    player_level = 1,
})

functionunlock:upsert({
    id = 22,
    player_level = 1,
})

functionunlock:upsert({
    id = 23,
    player_level = 1,
})

functionunlock:upsert({
    id = 24,
    player_level = 1,
})

functionunlock:upsert({
    id = 30,
    player_level = 1,
})

functionunlock:upsert({
    id = 40,
    player_level = 1,
})

functionunlock:upsert({
    id = 50,
    player_level = 1,
})

functionunlock:upsert({
    id = 51,
    player_level = 1,
})

functionunlock:upsert({
    id = 52,
    player_level = 1,
})

functionunlock:upsert({
    id = 53,
    player_level = 1,
})

functionunlock:upsert({
    id = 60,
    player_level = 1,
})

functionunlock:upsert({
    id = 61,
    player_level = 1,
})

functionunlock:upsert({
    id = 62,
    player_level = 1,
})

functionunlock:upsert({
    id = 63,
    player_level = 1,
})

functionunlock:upsert({
    id = 64,
    player_level = 1,
})

functionunlock:upsert({
    id = 101,
    player_level = 1,
    lock_tips = '玩家{PlayerLevel}级时解锁',
})

functionunlock:upsert({
    id = 102,
    player_level = 5,
    lock_tips = '玩家{PlayerLevel}级时解锁',
})

functionunlock:upsert({
    id = 103,
    player_level = 5,
    lock_tips = '玩家{PlayerLevel}级时解锁',
})
