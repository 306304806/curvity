-- account.lua

local log_err       = logger.err
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local cache_agent   = quanta.get("cache_agent")

local AccountDao = singleton()
function AccountDao:__init()
end

-- 加载账号信息
function AccountDao:load_account(open_id)
    local code, result = cache_agent:load(open_id, "account", open_id)
    if check_failed(code) then
        log_err("[AccountDao][load_account] failed: code: %s, result: %s", code, serialize(result))
        return false
    end
    local account = result.account
    if not account or not account.open_id then
        return true
    end
    return true, account
end

-- 更新账号信息
function AccountDao:upset_account(open_id, account)
    local code, result = cache_agent:update(open_id, "account", account, "account", true, open_id)
    if check_failed(code) then
        log_err("[AccountDao][upset_account] failed: code: %s, result: %s", code, result)
        return false
    end
    return true
end

-- 查询角色昵称
function AccountDao:load_player_name(name)
    local code, result = cache_agent:load(name, "player_name", name)
    if check_failed(code) then
        log_err("[AccountDao][load_player_name] failed: code: %s, result: %s", code, result)
        return false
    end
    local name_info = result.player_name
    if not name_info or not name_info.name then
        return true
    end
    return true, name_info
end

-- 使用角色昵称
function AccountDao:upset_player_name(name, player_name_info)
    local code, result = cache_agent:update(name, "player_name", player_name_info, "player_name", true, name)
    if check_failed(code) then
        log_err("[AccountDao][upset_name] failed: code: %s, result: %s", code, result)
        return false
    end
    return true
end

-- export
quanta.account_dao = AccountDao()

return AccountDao
