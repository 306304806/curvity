--season_mgr.lua
local otime         = os.time
local odate         = os.date
local log_err       = logger.err
local log_info      = logger.info

local router_mgr    = quanta.get("router_mgr")
local thread_mgr    = quanta.get("thread_mgr")
local config_mgr    = quanta.get("config_mgr")
local timer_mgr     = quanta.get("timer_mgr")

local PeriodTime    = enum("PeriodTime")

local rank_season_db        = config_mgr:init_table("rankseason", "id")
local battlepass_season_db  = config_mgr:init_table("battlepassseason", "id")

local SeasonMgr = singleton()
local prop = property(SeasonMgr)
prop:accessor("rank_season", 0)
prop:accessor("battlepass_season", 0)
prop:accessor("rank_start_time", 0)
prop:accessor("rank_finish_time", 0)
prop:accessor("battlepass_start_time", 0)
prop:accessor("battlepass_finish_time", 0)

function SeasonMgr:__init()
    self:setup()
end

function SeasonMgr:setup()
    -- 关注服务器注册事件
    router_mgr:watch_service_ready(self, "platform")
    -- 检查赛季配置
    self:check_season_config()
    self:on_check_season_change()
    -- 定时器轮询检查赛季变更
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:on_check_season_change()
    end)
end

--服务器注册处理
function SeasonMgr:on_service_ready(id, service_name)
    log_info("[SeasonService][on_service_ready]->id:%s, service_name:%s", id, service_name)
    local rank_season_time = self:get_rank_time()
    local bp_season_time = self:get_battlepass_time()
    thread_mgr:success_call(PeriodTime.SECOND_MS, function()
        local ok, err = router_mgr:call_target(id, "rpc_season_data_ntf", self.rank_season, self.battlepass_season, rank_season_time, bp_season_time)
        if ok then
            return true
        end
        log_err("[SeasonService][on_service_ready] not failed! id:%s, service_name:%s, err:%s", id, service_name, err)
        return false
    end)
end

-- 检查赛季配置
function SeasonMgr:check_season_config()
    for _, season_cfg in rank_season_db:iterator() do
        if season_cfg.start  >= season_cfg.finish then
            log_err("[SeasonMgr][check_season_config] rank season time error! check xlsm!!! start:%s, finish:%s",
            odate("%Y-%m-%d-%H-%M-%S", season_cfg.start), odate("%Y-%m-%d-%H-%M-%S", season_cfg.finish))
            return false
        end
    end
    for _, season_cfg in battlepass_season_db:iterator() do
        if season_cfg.start  >= season_cfg.finish then
            log_err("[SeasonMgr][check_season_config] battlepass season time error! check xlsm!!! start:%s, finish:%s",
            odate("%Y-%m-%d-%H-%M-%S", season_cfg.start), odate("%Y-%m-%d-%H-%M-%S", season_cfg.finish))
            return false
        end
    end
    return true
end

-- 检查赛季变更
function SeasonMgr:on_check_season_change()
    local cur_rank_season = self:calc_rank_season_index()
    if cur_rank_season ~= self.rank_season then
        local old_season = self.rank_season
        self.rank_season = cur_rank_season
        self:update_rank_season_time()
        router_mgr:call_platform_all("rpc_rank_season_update", old_season, cur_rank_season, self.rank_start_time, self.rank_finish_time)
        log_info("[SeasonMgr][on_check_season_change] rank season change! old_season:%s, new_season:%s", old_season, cur_rank_season)
    end
    local cur_bp_season = self:calc_battlepass_season_index()
    if cur_bp_season ~= self.battlepass_season then
        local old_season = self.battlepass_season
        self.battlepass_season = cur_bp_season
        self:update_battlepass_season_time()
        router_mgr:call_platform_all("rpc_battlepass_season_update", old_season, cur_bp_season, self.battlepass_start_time, self.battlepass_finish_time)
        log_info("[SeasonMgr][on_check_season_change] battlepass season change! old_season:%s, new_season:%s", old_season, cur_bp_season)
    end
end

-- 更新battlepass赛季时间
function SeasonMgr:update_battlepass_season_time()
    for _, season_cfg in battlepass_season_db:iterator() do
        if self.battlepass_season == season_cfg.id then
            self.battlepass_start_time  = season_cfg.start
            self.battlepass_finish_time = season_cfg.finish
            break
        end
    end
end

-- 更新排位赛季时间
function SeasonMgr:update_rank_season_time()
    for _, season_cfg in rank_season_db:iterator() do
        if self.rank_season == season_cfg.id then
            self.rank_start_time  = season_cfg.start
            self.rank_finish_time = season_cfg.finish
            break
        end
    end
end

-- 获取排位赛季时间信息
function SeasonMgr:get_rank_time()
    return {
        start_time = self.rank_start_time,
        finish_time = self.rank_finish_time,
    }
end

-- 获取battlepass赛季时间信息
function SeasonMgr:get_battlepass_time()
    return {
        start_time = self.battlepass_start_time,
        finish_time = self.battlepass_finish_time,
    }
end

-- 计算排位赛季索引
function SeasonMgr:calc_rank_season_index()
    local cur_time = otime()
    local cur_season
    for _, season_cfg in rank_season_db:iterator() do
        if cur_time >= season_cfg.start and cur_time <= season_cfg.finish then
            cur_season = season_cfg.id
            break
        end
    end
    if not cur_season then
        log_err("[SeasonMgr][calc_rank_season_index] failed!")
        return
    end
    return cur_season
end

-- 计算特别行动赛季索引
function SeasonMgr:calc_battlepass_season_index()
    local cur_time = otime()
    local cur_season
    for _, season_cfg in battlepass_season_db:iterator() do
        if cur_time >= season_cfg.start and cur_time <= season_cfg.finish then
            cur_season = season_cfg.id
            break
        end
    end
    if not cur_season then
        log_err("[SeasonMgr][calc_rank_season_index] failed!")
        return
    end
    return cur_season
end

-- export
quanta.season_mgr = SeasonMgr()

return SeasonMgr
