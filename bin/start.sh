LD_LIBRARY_PATH=./:$LD_LIBRARY_PATH

nohup ./quanta ./local/monitor.conf    --index=1 &
nohup ./quanta ./local/router.conf     --index=1 &
nohup ./quanta ./local/dbsvr.conf      --index=1 &
nohup ./quanta ./local/gateway.conf    --index=1 &
nohup ./quanta ./local/platform.conf   --index=1 &
nohup ./quanta ./local/platcenter.conf --index=1 &
nohup ./quanta ./local/proxy.conf      --index=1 &

exit
