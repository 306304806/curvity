--reddot_component.lua
local otime         = os.time
local new_guid      = guid.new
local tinsert       = table.insert

local player_dao    = quanta.get("player_dao")

local ReddotComponent = mixin(
    "add_reddot",
    "get_reddot",
    "read_reddot",
    "pack_reddots"
)

local prop = property(ReddotComponent)
prop:accessor("reddots", {})
prop:accessor("reddot_indexs", {})
prop:accessor("reddot_dirty", false)

local function insert_reddot(self, reddot)
    self.reddots[reddot.reddot_id] = reddot
    --建立索引
    if not self.reddot_indexs[reddot.reddot_type] then
        self.reddot_indexs[reddot.reddot_type] = {}
    end
    self.reddot_indexs[reddot.reddot_type][reddot.reddot_rid] = reddot
end

local function remove_reddot(self, reddot)
    self.reddots[reddot.reddot_id] = nil
    local reddot_rid = reddot.reddot_rid
    local reddot_type = reddot.reddot_type
    if self.reddot_indexs[reddot_type] and self.reddot_indexs[reddot_type][reddot_rid] then
        self.reddot_indexs[reddot_type][reddot_rid] = nil
    end
end

local function find_reddot(self, reddot_type, reddot_rid)
    if self.reddot_indexs[reddot_type] and self.reddot_indexs[reddot_type][reddot_rid] then
        return self.reddot_indexs[reddot_type][reddot_rid]
    end
end

local function check_reddot(reddots)
    local creddots = {}
    local now_time = quanta.now
    for _, reddot in pairs(reddots or {}) do
        if reddot.overtime < now_time then
            tinsert(creddots, reddot)
        end
    end
    return creddots
end

function ReddotComponent:__init(player_id)
end

function ReddotComponent:setup(db_res)
    local player_reddot = db_res.plat_player_reddot
    if not player_reddot then
        return false
    end
    local redot_data = player_reddot.reddot
    if redot_data then
        local creddots = check_reddot(redot_data.reddots)
        for _, reddot in pairs(creddots) do
            insert_reddot(self, reddot)
        end
    end
    return true
end

function ReddotComponent:update_data_2db()
    if not self.reddot_dirty then
        return true
    end
    local reddot_res = {
        reddots = {},
        player_id = self.player_id,
    }
    local creddots = check_reddot(self.reddots)
    for _, reddot in pairs(creddots) do
        tinsert(reddot_res.reddots, reddot)
    end
    if player_dao:update_player_reddot(self.player_id, reddot_res) then
        self.reddot_dirty = false
        return true
    end
    return false
end

function ReddotComponent:pack_reddots()
    local reddots = { }
    for _, reddot in pairs(self.reddots) do
        tinsert(reddots, reddot)
    end
    return { reddots = reddots }
end

function ReddotComponent:get_reddot(reddot_id)
    return self.reddots[reddot_id]
end

function ReddotComponent:add_reddot(reddot_type, reddot_rid, overtime, custom)
    self.reddot_dirty = true
    local oreddot = find_reddot(self, reddot_type, reddot_rid)
    if oreddot then
        oreddot.mark = true
        oreddot.custom = custom or ""
        if not overtime or 0 == overtime then
            oreddot.overtime = 0
        else
            if oreddot.overtime >= otime() then
                oreddot.overtime = oreddot.overtime + (overtime or 0)
            else
                oreddot.overtime = otime() + (overtime or 0)
            end
        end

        return oreddot
    else
        overtime = overtime or 0
        local reddot_id = new_guid()
        local reddot = {
            mark        = true,
            reddot_id   = reddot_id,
            reddot_rid  = reddot_rid,
            reddot_type = reddot_type,
            custom      = custom or "",
            overtime    = (0 ~= overtime) and (otime() + overtime) or 0,
        }

        insert_reddot(self, reddot)
        return reddot
    end
end

function ReddotComponent:read_reddot(reddot_id)
    local reddot = self.reddots[reddot_id]
    if reddot then
        self.reddot_dirty = true
        remove_reddot(self, reddot)
    end
end

return ReddotComponent
