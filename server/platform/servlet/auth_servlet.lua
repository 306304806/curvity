-- auth_servlet.lua
local otime             = os.time
local log_err           = logger.err
local log_debug         = logger.debug
local serialize         = logger.serialize

local event_mgr         = quanta.get("event_mgr")
local config_mgr        = quanta.get("config_mgr")
local thread_mgr        = quanta.get("thread_mgr")
local auth_api          = quanta.get("auth_api")
local auth_dao          = quanta.get("auth_dao")
local auth_mgr          = quanta.get("auth_mgr")

local CertType          = enum("CertType")
local KernCode          = enum("KernCode")
local SUCCESS           = KernCode.SUCCESS
local CSCmdID           = ncmd_plat.CCmdId

local utility_db        = config_mgr:init_table("utility", "id")

local AuthServlet = singleton()
function AuthServlet:__init()
    event_mgr:add_trigger(self, "evt_register_account")
    event_mgr:add_trigger(self, "on_client_login")
    event_mgr:add_trigger(self, "on_client_logout")

    --CS
    event_mgr:add_cmd_listener(self, CSCmdID.NID_AUTH_REAL_NAME_REQ, "on_auth_real_name_req")
end

-- 注册新账号
function AuthServlet:evt_register_account(info)
    if not self:is_open_auth() then
        return
    end

    -- 新注册账号默认为为进行实名认证操作
    local auth_info = {}
    auth_info.open_id            = info.other_open_id
    auth_info.auth_type          = 0                   -- 认证类型
    auth_info.auth_name          = 0                   -- 认证名字
    auth_info.auth_cert_id       = ""                   -- 证件id
    auth_info.unauth_game_time   = 0                   -- 未认证游戏时间
    auth_info.unauth_reset_time  = 0                   -- 未认证重置时间点
    auth_info.today_game_time    = 0                   -- 未成年今日累计游戏时间
    auth_info.today_update_time  = 0                   -- 未成年今日累计游戏时间刷新时间点
    auth_info.month_recharge_total     = 0             -- 未成年月充值额度
    auth_info.last_recharge_time       = 0             -- 未成年上次充值时间

    auth_mgr:update_auth_info(auth_info)
    auth_dao:upset_auth_info(auth_info)
end

-- 玩家完成登录
function AuthServlet:on_client_login(player, player_id)
    if not self:is_open_auth() then
        return
    end
    local open_id = player:get_open_id()
    local auth_info = auth_mgr:get_auth_info(open_id)
    if not auth_info then
        log_err("[AuthServlet][on_client_login] ger auth info failed! open_id:%s", open_id)
        return
    end
    log_debug("[AuthServlet][on_client_login]->player_id:%s, info:%s", player_id, serialize(auth_info))
    local cur_time = otime()
    local reset_time_var = utility_db:find_one("unauth_reset_time").value
    local reset_flag = false
    if auth_info.auth_type == 0 then
        if auth_info.unauth_game_time > 0 and auth_info.unauth_reset_time and cur_time > auth_info.unauth_reset_time + reset_time_var then
            reset_flag = true
        end
    end
    local ntf = {
        unauth_reset_time = auth_info.unauth_reset_time,
        auth_type         = auth_info.auth_type,
        auth_game_time    = auth_info.unauth_game_time,
        config_game_time  = utility_db:find_one("unauth_game_time").value,
        auth_age          = auth_mgr:get_auth_age(open_id)
    }
    if reset_flag then
        ntf.auth_game_time    = 0
    end
    player:send(CSCmdID.NID_AUTH_REAL_NAME_NTF, ntf)
    if not auth_info.auth_type or auth_info.auth_type == 0 then
        auth_mgr:add_unauth_record(open_id, player_id)
    else
        auth_mgr:rm_unauth_record(open_id)
    end
    if reset_flag or auth_info.unauth_reset_time == 0 then
        auth_info.unauth_game_time  = 0
        auth_info.unauth_reset_time = cur_time
        local function save_func()
            auth_dao:upset_auth_info(auth_info)
        end
        thread_mgr:fork(save_func)
    end
    log_debug("[AuthServlet][on_client_login]->open_id:%s, auth_info.auth_cert_id:%s!", open_id, auth_info.auth_cert_id)
    auth_mgr:add_teen_info(open_id, auth_info.auth_cert_id, player_id, 0, cur_time)
end

-- 玩家登出
function AuthServlet:on_client_logout(player, player_id)
    if not self:is_open_auth() then
        return
    end
    local open_id = player:get_open_id()
    local auth_info = auth_mgr:get_auth_info(open_id)
    if auth_info and next(auth_info) then
        auth_dao:upset_auth_info(auth_info)
    end
    auth_mgr:rm_unauth_record(open_id)
    auth_mgr:rm_auth_info(open_id)
    auth_mgr:rm_teen_info(open_id)
end

-- 实名认证请求
function AuthServlet:on_auth_real_name_req(player, player_id, data)
    log_debug("[AuthServlet][on_auth_real_name_req]->player_id:%s, data:%s", player_id, serialize(data))
    local cert_type, real_name, cert_id = data.type, data.real_name, data.cert_id
    if not real_name or not cert_id or not cert_type or cert_type ~= CertType.ID_CARD then
        -- 当前只支持身份证验证
        return { code = KernCode.PARAM_ERROR }
    end
    local open_id = player:get_open_id()
    local auth_info = auth_mgr:get_auth_info(open_id)
    if auth_info and auth_info.auth_type > 0 then
        return { code = KernCode.PARAM_ERROR }
    end
    local code, is_match, msg = auth_api:auth_user_id_card(real_name, cert_id)
    log_debug("[AuthServlet][on_auth_real_name_req]->open_id:%s, resp.is_match:%s, auth_info:%s!", open_id, is_match, serialize(auth_info))
    if code == SUCCESS and is_match then
        if auth_info then
            local cur_time = quanta.now
            auth_info.unauth_game_time   = 0  -- 清零
            auth_info.auth_type          = cert_type
            auth_info.auth_name          = real_name
            auth_info.auth_cert_id       = cert_id
            auth_info.unauth_reset_time  = cur_time
            auth_info.today_game_time    = 0
            auth_info.today_update_time  = cur_time
            auth_dao:upset_auth_info(auth_info)

            auth_mgr:update_auth_info(auth_info)
            auth_mgr:rm_unauth_record(open_id)
            log_debug("[AuthServlet][on_auth_real_name_req]->add_teen_info!->open_id:%s, data.cert_id:%s", open_id, data.cert_id)
            auth_mgr:add_teen_info(open_id, data.cert_id, player_id, 0, cur_time)
        end
        return { code = code, is_match = is_match, tip_msg = msg, auth_age = auth_mgr:get_auth_age(open_id) }
    end
    return { code = code, is_match = is_match, tip_msg = msg, auth_age = 0 }
end

-- 是否开放实名认证
function AuthServlet:is_open_auth()
    local open_check_id = utility_db:find_one("auth_open").value
    if not open_check_id or open_check_id <= 0 then
        return false
    end
    return true
end

quanta.auth_servlet = AuthServlet()

return AuthServlet
