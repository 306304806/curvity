-- auth_dao.lua

local log_err       = logger.err
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local db_agent      = quanta.get("db_agent")

local AuthDao = singleton()
function AuthDao:__init()
end

-- 加载认证信息
function AuthDao:load_auth_info(open_id)
    local ok, code, res = db_agent:find_one(open_id,
        {"auth_info", {open_id = open_id,}, {_id = 0}})
    if not ok or check_failed(code) then
        log_err("[AuthDao][load_auth_info] failed: ok:%s, code: %s, res: %s", ok, code, serialize(res))
        return false
    end

    return true, res or {}
end

-- 更新实名认证信息
function AuthDao:upset_auth_info(auth_info)
    local open_id = auth_info.open_id
    local ok, code, res = db_agent:update(open_id,
        {
            "auth_info",
            auth_info,
            {open_id = open_id,},
            true
        }
    )

    if not ok or check_failed(code) then
        log_err("[AuthDao][upset_auth_info] failed: ok:%s code: %s, res: %s", ok, code, res)
        return false
    end

    return true
end

-- export
quanta.auth_dao = AuthDao()

return AuthDao
