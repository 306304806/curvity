--mail_component.lua
local tinsert       = table.insert
local log_info      = logger.info

local player_dao    = quanta.get("player_dao")

local MailState     = enum("MailState")
local AttachState   = enum("AttachState")

local MailComponent = mixin(
    "add_mail",
    "get_mail",
    "read_mail",
    "delete_mail",
    "take_mail_attach",
    "del_mail_attach",
    "update_mail_query_time"
)

local prop = property(MailComponent)
prop:reader("mails", {})
prop:reader("attach_items", {})
prop:reader("mail_dirty", false)
prop:reader("mail_query_time", 0)    -- 全局邮件查询时间

function MailComponent:__init(player_id)
end

function MailComponent:setup(db_data)
    local player_mail = db_data.plat_player_mail
    if not player_mail then
        return false
    end
    local mail_data = player_mail.mail
    if mail_data then
        for _, mail in pairs(mail_data.mails or {}) do
            self.mails[mail.mail_id] = mail
        end
        for _, attach_item in pairs(mail_data.attach_items or {}) do
            self.attach_items[attach_item.mail_id] = attach_item.items
        end
        self.mail_query_time = mail_data.mail_query_time or 0
    end
    return true
end

function MailComponent:update_data_2db()
    if not self.mail_dirty then
        return true
    end
    local mails = {}
    for _, mail in pairs(self.mails) do
        tinsert(mails, mail)
    end
    local attach_items = {}
    for mail_id, items in pairs(self.attach_items) do
        tinsert(attach_items, { mail_id = mail_id, items = items })
    end
    local mail_data = {
        mails = mails,
        attach_items = attach_items,
        mail_query_time = self.mail_query_time
    }
    if player_dao:update_player_mail(self.player_id, mail_data) then
        self.mail_dirty = false
        return true
    end
    return false
end

-- 添加新邮件
function MailComponent:add_mail(mail_info)
    log_info("[MailComponent][add_mail] finish: player_id=%s, mail_id=%s", self.player_id, mail_info.mail_id)
    self.mails[mail_info.mail_id] = mail_info
    self.mail_dirty = true
end

-- 获取指定邮件
function MailComponent:get_mail(mail_id)
    return self.mails[mail_id]
end

-- 设置邮件已读
function MailComponent:read_mail(mail_id)
    local mail = self.mails[mail_id]
    if mail and mail.mail_state == MailState.UNREADED then
        mail.mail_state = MailState.READED
        self.mail_dirty = true
        return true
    end
    return false
end

-- 设置邮件删除
function MailComponent:delete_mail(mail_id)
    if self.mails[mail_id] then
        self.mails[mail_id] = nil
        self.mail_dirty = true
        return true
    end
    return false
end

--领取取附件
function MailComponent:take_mail_attach(mail_id)
    local mail = self.mails[mail_id]
    if mail then
        if mail.attach_state == AttachState.TAKEED then
            return false
        end
        local attach_items = mail.attach_items
        if #attach_items > 0 then
            self.attach_items[mail_id] = attach_items
        end
        mail.mail_state = MailState.READED
        mail.attach_state = AttachState.TAKING
        self.mail_dirty = true
        return true, attach_items
    end
    return false
end

--删除邮件附件
function MailComponent:del_mail_attach(mail_id)
    if self.attach_items[mail_id] then
        self.attach_items[mail_id] = nil
        self.mail_dirty = true
    end
end

-- 获取最后一次查询群邮件的时间
function MailComponent:update_mail_query_time()
    self.mail_query_time = quanta.now
    self.mail_dirty = true
end

return MailComponent