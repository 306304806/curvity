--rank_dao.lua

local log_err       = logger.err
local check_failed  = utility.check_failed

local db_agent      = quanta.get("db_agent")

local RankDao = singleton()
function RankDao:__init()
end

function RankDao:load_rank_dir_data(rank_type, rank_name, db_selector)
    local coll_name = "plat_dir_" .. rank_name
    local ok, code, res = db_agent:find(rank_type, {coll_name, db_selector, {_id = 0}})
    if not ok or check_failed(code) then
        --log_err("[RankDao][load_rank_dir_data] failed! rank_name:%s code: %s, res: %s", rank_name, code, res)
        return false
    end

    return true, res or {}
end

function RankDao:update_rank_dir_data(rank_type, rank_name, rank_dir, db_selector)
    local coll_name = "plat_dir_" .. rank_name
    local ok, code, res = db_agent:update(rank_type, {coll_name, rank_dir, db_selector, true})
    if not ok or check_failed(code) then
        log_err("[RankDao][update_rank_dir_data] failed! rank_name:%s code: %s, res: %s", rank_name, code, res)
        return false
    end

    return true
end

function RankDao:load_rank_data(rank_id, rank_name)
    if type(rank_name) ~= "string" then
        log_err("[RankDao][load_rank_data] param error! rank_name:%s", rank_name)
        return
    end

    local hash_key = rank_id
    local coll_name = "plat_" .. rank_name
    local ok, code, res = db_agent:find_one(hash_key, {coll_name, {rank_id = rank_id,}, {_id = 0}})
    if not ok or check_failed(code) then
        log_err("[RankDao][load_rank_data] failed! rank_name:%s code: %s, res: %s", rank_name, code, res)
        return
    end

    return res or {}
end

function RankDao:update_rank_data(rank_id, rank_name, rank_data)
    local hash_key = rank_id
    local coll_name = "plat_"..rank_name
    local ok, code, res = db_agent:update(hash_key, {coll_name, rank_data, {rank_id = rank_id,}, true})
    if not ok or check_failed(code) then
        log_err("[RankDao][update_rank_data] failed! rank_name:%s code: %s, res: %s", rank_name, code, res)
        return false
    end
    return true
end

quanta.rank_dao = RankDao()

return RankDao
