-- auth_api.lua
local ljson         = require("luacjson")
local encrypt       = require("encrypt")
local otime         = os.time
local mrandom       = math.random
local sformat       = string.format
local json_decode   = ljson.decode
local log_err       = logger.err
local serialize     = logger.serialize

local KernCode      = enum("KernCode")
local router_mgr    = quanta.get("router_mgr")
local config_mgr    = quanta.get("config_mgr")

local utility_db    = config_mgr:init_table("utility", "id")

--local AUTH_URL    = utility_db:find_one("auth_url").value
local AUTH_URL      = utility_db:find_one("auth_test_url").value
local AUTH_CMD      = utility_db:find_one("auth_cmd").value
local AUTH_APPID    = utility_db:find_one("auth_app_id").value
local AUTH_SECORT   = utility_db:find_one("auth_secret").value

local AuthApi = singleton()
function AuthApi:__init()
    ljson.encode_sparse_array(true)
end

-- 验证玩家身份证信息
function AuthApi:auth_user_id_card(name, cert_id, open_id)
    local timestamp = tostring(otime())
    local rand_var  = mrandom(1000, 999999999)
    local nonce     = tostring(rand_var)
    local command_str = sformat("appid=%s&cmd=%s&nonce=%s&timestamp=%s%s", AUTH_APPID, AUTH_CMD, nonce, timestamp, AUTH_SECORT)
    local flag, sign_str = encrypt.md5str(command_str)
    if not flag then
        log_err("[AuthApi][auth_user_id_card] md5str failed! name:%s, cert_id:%s, open_id:%s", name, cert_id, open_id)
        return KernCode.NETWORK_ERROR, false, ""
    end
    local query_parameter = {
        nonce       = nonce,
        timestamp   = timestamp,
        sign        = sign_str,
        cmd         = AUTH_CMD,
        appid       = AUTH_APPID,
    }
    local post_req      = {
        name        = name,
        certId      = cert_id,
        context     = open_id,
        authInfo    = {
            authUserType = 0,
            authAppid    = AUTH_APPID,
            authUserId   = "24347382",
            authKey      = "244fe73ab2343803ce",
        }
    }
    local headers       = {
        ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8",
    }
    post_req = ljson.encode(post_req)
    local ok, code, res = router_mgr:call_proxy_hash(rand_var, "rpc_http_post", AUTH_URL, query_parameter, post_req, headers)
    if not ok or 200 ~= code then
        log_err("[AuthApi][auth_user_id_card] call faild: ok=%s, code=%s,res=%s", ok, code, serialize(res))
        return KernCode.NETWORK_ERROR, false, ""
    end
    ok, res = pcall(json_decode, res)
    if not ok or not res or res.msg ~= "success" then
        log_err("[AuthApi][auth_user_id_card] json_decode faild: ok=%s, code=%s, msg=%s", ok, code, res.msg)
        return KernCode.NETWORK_ERROR, res.isMatch, res.msg
    end
    return KernCode.SUCCESS, res.isMatch, res.msg
end

-- export
quanta.auth_api = AuthApi()