--player.lua

local log_warn      = logger.warn
local env_number    = environ.number

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local player_dao    = quanta.get("player_dao")

local FriendComponent   = import("platform/component/friend_component.lua")
local MailComponent     = import("platform/component/mail_component.lua")
local RedotComponent    = import("platform/component/reddot_component.lua")
local ShopComponent     = import("platform/component/shop_component.lua")
local Player = class(nil, FriendComponent, RedotComponent, MailComponent, ShopComponent)

local prop = property(Player)
prop:accessor("player_id", 0)
prop:accessor("gateway", nil)
prop:accessor("dirty", false)
prop:accessor("token", "")
prop:accessor("data", {})
prop:accessor("machine_id", "")
prop:accessor("open_id", "")
prop:accessor("ip", "")

function Player:__init(player_id)
    self.player_id = player_id
end

function Player:setup(player_res, image_res)
    if image_res then
        local player_data = image_res.plat_player
        if not player_data then
            return false
        end
        local slf_data = self.data
        for attr, value in pairs(player_data.player or {}) do
            slf_data[attr] = value or 0
        end
        slf_data.time = quanta.now
        self.dirty = true
    end
    local setup_res = self:collect("setup", player_res)
    if not setup_res then
        log_warn("[Player:setup] player %s setup faild!", self.player_id)
        return false
    end
    return setup_res
end

--更新数据
function Player:update_data(player_data)
    local slf_data = self.data
    for attr, value in pairs(player_data) do
        slf_data[attr] = value or 0
    end
    event_mgr:notify_trigger("update_friend_status", self)
    self.dirty = true
end

function Player:get_nick()
    return self.data.nick
end

--更新数据
function Player:update()
    if self.dirty then
        if player_dao:update_player_image(self.player_id, self.data) then
            self.dirty = false
        end
    end
    self:invoke("update_data_2db")
end

--踢出玩家
function Player:kick_out()
    if self.gateway then
        router_mgr:send_target(self.gateway, "rpc_client_kickout", self.player_id)
    end
end

--发送消息
function Player:send(cmd_id, data)
    if self.gateway then
        router_mgr:send_target(self.gateway, "rpc_trans_message", self.player_id, cmd_id, data)
    end
end

--群发消息
function Player:send_group(groups, cmd_id, data)
    router_mgr:call_gateway_all("rpc_group_message", groups, cmd_id, data)
end

function Player:build_dlog_public_fields()
    return {area_id = env_number("QUANTA_AREA_ID"), open_id = self.open_id, character_id = self.player_id, character_level = self.data.level}
end

return Player
