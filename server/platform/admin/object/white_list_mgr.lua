--white_list_mgr.lua
local otime                 = os.time
local tinsert               = table.insert
local tmapv2array           = table_ext.mapv2array
local log_err               = logger.err
local log_debug             = logger.debug
local serialize             = logger.serialize

local white_list_dao        = quanta.get("white_list_dao")
local timer_mgr             = quanta.get("timer_mgr")
local router_mgr            = quanta.get("router_mgr")

local DBLoading             = enum("DBLoading")
local PeriodTime            = enum("PeriodTime")

local WhiteListMgr = singleton()
local prop = property(WhiteListMgr)
prop:accessor("data_loading", DBLoading.INIT)
prop:accessor("indexs", {})
prop:accessor("dirty_key_map", {})
prop:reader("dbsvr_online", false)

function WhiteListMgr:__init()
    self:setup()

    router_mgr:watch_service_ready(self, "dbsvr")
    router_mgr:watch_service_close(self, "dbsvr")
end

-- 初始化函数
function WhiteListMgr:setup()
    -- 延迟从数据库加载排行数据
    self.load_timer = timer_mgr:loop(PeriodTime.SECOND_5_MS, function()
        self:load_white_list_data()
    end)

    -- 定时检查存储排行索引数据
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:update_data_2db()
    end)
end

function WhiteListMgr:on_service_ready(quanta_id, service_name)
    self.dbsvr_online = true
end

function WhiteListMgr:on_service_close(quanta_id, service_name)
    self.dbsvr_online = false
end

-- 从数据库加载数据
function WhiteListMgr:load_white_list_data()
    log_debug("[WhiteListMgr][load_white_list_data]")
    if not self.dbsvr_online or self.data_loading ~= DBLoading.INIT then
        return
    end
    self.data_loading = DBLoading.LOADING
    local ok, result = white_list_dao:load_white_list()
    if not ok then
        self.data_loading = DBLoading.INIT
        log_err("[WhiteListMgr][load_white_list_data]->load failed!")
        return
    end

    self.data_loading = DBLoading.SUCCESS
    for _, item in pairs(result) do
        self.indexs[item.open_id] = item
    end
    timer_mgr:unregister(self.load_timer)

    return true
end

-- 数据落地
function WhiteListMgr:update_data_2db()
    if self.data_loading ~= DBLoading.SUCCESS then
        return
    end
    for open_id, _ in pairs(self.dirty_key_map) do
        local data = self.indexs[open_id]
        if not data then
            local ok = white_list_dao:delete_white_list(open_id)
            if ok then
                self.dirty_key_map[open_id] = nil
            end
        else
            local ok = white_list_dao:update_white_list(open_id, data)
            if ok then
                self.dirty_key_map[open_id] = nil
            end
        end
    end
end

-- 导入白名单
function WhiteListMgr:import_white_list(white_lists)
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[WhiteListMgr][import_white_list] white_lists = %s", serialize(white_lists))
        return false
    end

    local fail_lists = {}
    for _, open_id in pairs(white_lists) do
        if not self.indexs[open_id] then
            self.indexs[open_id] = {open_id = open_id, time = otime()}
            self.dirty_key_map[open_id] = true
        else
            tinsert(fail_lists, open_id)
        end
    end
    return true, fail_lists
end

-- 查询白名单
function WhiteListMgr:search_white_list()
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[WhiteListMgr][search_white_list]")
        return false
    end

    local ret = tmapv2array(self.indexs)
    return true, ret
end

-- 删除白名单
function WhiteListMgr:delete_white_list(white_lists)
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[WhiteListMgr][delete_white_list] white_lists = %s", serialize(white_lists))
        return false
    end
    log_err("[WhiteListMgr][delete_white_list] white_lists = %s", serialize(self.indexs))
    local fail_lists = {}
    for _, open_id in pairs(white_lists) do
        if self.indexs[open_id] then
            self.indexs[open_id] = nil
            self.dirty_key_map[open_id] = true
        else
            tinsert(fail_lists, open_id)
        end
    end
    return true, fail_lists
end

-- 查询白名单
function WhiteListMgr:is_in_white_list(open_id)
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[WhiteListMgr][is_in_white_list]")
        return false
    end

    return (self.indexs[open_id] ~= nil)
end

-- export
quanta.white_list_mgr = WhiteListMgr()

return WhiteListMgr