--friend_servlet.lua
local tinsert       = table.insert
local log_debug     = logger.debug
local serialize     = logger.serialize
local max_number    = math.huge

local CSCmdID       = ncmd_plat.CCmdId
local SSCmdID       = enum("PlatSSCmdID")

local RmsgPlat      = enum("RmsgPlat")
local FriendType    = enum("FriendType")
local FriendLimit   = enum("FriendLimit")
local FriendSetup   = enum("FriendSetup")
local ReddotType    = enum("ReddotType")
local PlatCommon    = enum("PlatCommon")
local SocialSecret  = enum("SocialSecret")

local KernCode      = enum("KernCode")
local FriendCode    = enum("FriendCode")
local SafeCode      = enum("SafeCode")
local SUCCESS       = KernCode.SUCCESS

local APPLY_LIMIT   = quanta_const.APPLY_LIMIT

local dlog_mgr      = quanta.get("dlog_mgr")
local data_mgr      = quanta.get("data_mgr")
local fword_mgr     = quanta.get("fword_mgr")
local event_mgr     = quanta.get("event_mgr")
local player_mgr    = quanta.get("player_mgr")
local rmsg_friend   = quanta.get("rmsg_friend")

local FriendServlet = singleton()
function FriendServlet:__init()
    --监听事件
    event_mgr:add_trigger(self, "on_client_login")
    event_mgr:add_trigger(self, "update_friend_status")
    event_mgr:add_listener(self, "do_friend_add")
    event_mgr:add_listener(self, "do_friend_del")
    event_mgr:add_listener(self, "do_request_del")
    event_mgr:add_listener(self, "do_friend_reply")
    event_mgr:add_listener(self, "do_friend_message")

    --监听服务器协议
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_FRIEND_SECRET_REQ, "on_friend_secret_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_FRIEND_BUILD_RECENT_REQ, "on_friend_build_recent_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_FRIEND_ADD_INTIMACY_REQ, "on_friend_add_intimacy_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_FRIENDSHIP_QUERY_REQ, "on_friendship_query_req")
    --监听客户端协议
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_ADD_REQ, "on_friend_add_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_DEL_REQ, "on_friend_del_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_REPLY_REQ, "on_friend_reply_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_SETUP_REQ, "on_friend_setup_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_SEARCH_REQ, "on_friend_search_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_REMARKS_REQ, "on_friend_remarks_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_GROUP_ADD_REQ, "on_friend_group_add_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_GROUP_DEL_REQ, "on_friend_group_del_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_GROUP_SWAP_REQ, "on_friend_group_swap_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_GROUP_MOVE_REQ, "on_friend_group_move_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_FRIEND_GROUP_MODIFY_REQ, "on_friend_group_modify_req")
end

function FriendServlet:on_client_login(player, player_id)
    log_debug("[FriendServlet][on_client_login] player_id:%s login success", player_id)
    player:send(CSCmdID.NID_FRIEND_GROUP_NTF, player:pack_groups())
    player:send(CSCmdID.NID_FRIEND_LIST_NTF, player:pack_friends())
    self:update_friend_status(player)
    self:do_friend_message(player_id)
end

function FriendServlet:friend_exist_code(friend_type, exist)
    if friend_type == FriendType.FRIEND then
        return exist and FriendCode.ERR_FRIEND_AREADY_FRIEND or FriendCode.ERR_FRIEND_NOT_IS_FRIEND
    end
    if friend_type == FriendType.BLACK then
        return exist and FriendCode.ERR_FRIEND_AREADY_BLACK or FriendCode.ERR_FRIEND_NOT_IS_BLACK
    end
    return exist and FriendCode.ERR_FRIEND_AREADY_APPLY or FriendCode.ERR_FRIEND_NOT_IS_APPLY
end

function FriendServlet:sync_friend_info(player, friend, target_id)
    local friend_data = {
        friend = {
            remarks = friend.remarks,
            intimacy = friend.intimacy,
            group_id = friend.group_id,
            friend_type = friend.friend_type,
            social_secret = friend.social_secret,
            player = data_mgr:get_player_data(target_id),
        }
    }
    player:send(CSCmdID.NID_FRIEND_MEMBER_NTF, friend_data)
end

function FriendServlet:remove_limit_friend(player, friend_type)
    local friend_id = player:get_far_member(friend_type)
    if friend_id then
        player:del_friend(friend_id)
        player:send(CSCmdID.NID_FRIEND_DEL_NTF, { target_id = friend_id })
    end
end

--检查每日申请是否超限制
function FriendServlet:check_day_request_limit(player, friend_count)
    local day_limit = max_number
    local day_request = player:get_day_request()
    for i = #APPLY_LIMIT, 1, -1 do
        local limit_info = APPLY_LIMIT[i]
        if friend_count > limit_info[1] then
            day_limit = limit_info[2]
            break
        end
    end
    return day_request >= day_limit
end

--申请列表满后顶掉最早的
function FriendServlet:remove_limit_request(player, target_id, player_id, friend_count)
    local request_count = player:get_request_count()
    if friend_count + request_count >= FriendLimit.FRIEND then
        local delete_id = player:get_far_member()
        if delete_id then
            player:del_request(delete_id)
            local rmsg = { msg = "do_request_del", data = player_id }
            local ok = rmsg_friend:send_message(quanta.name, delete_id, RmsgPlat.PLAT_FRIEND, rmsg)
            if not ok then
                return false
            end
            data_mgr:notify_player(delete_id, "do_friend_message")
        end
    end
    player:add_request(target_id)
    return true
end

function FriendServlet:update_friend_status(player)
    local players = {}
    local friends = player:get_friends()
    for friend_id, friend in pairs(friends) do
        if friend.friend_type == FriendType.FRIEND then
            tinsert(players, friend_id)
        end
    end
    if #players > 0 then
        player:send_group(players, CSCmdID.NID_FRIEND_STATUS_NTF, { friend = player:get_data() })
    end
end

function FriendServlet:update_friend_setup(player, setup_data)
    local players = {}
    local friends = player:get_friends()
    for friend_id, friend in pairs(friends) do
        tinsert(players, friend_id)
    end
    if #players > 0 then
        player:send_group(players, CSCmdID.NID_FRIEND_SETUP_NTF, setup_data)
    end
end

function FriendServlet:apply_add_friend(player, player_id, target_id)
    local friend_count = player:get_friend_count()
    if friend_count >= FriendLimit.FRIEND then
        return FriendCode.ERR_FRIEND_NUMBER_IS_LIMIT
    end
    if self:check_day_request_limit(player, friend_count) then
        return FriendCode.ERR_FRIEND_APPLY_IS_LIMIT
    end
    if not self:remove_limit_request(player, target_id, player_id, friend_count) then
        return FriendCode.ERR_FRIEND_RMSG_FAILED
    end
    local rmsg = { msg = "do_friend_add", data = player:get_data()}
    local ok = rmsg_friend:send_message(quanta.name, target_id, RmsgPlat.PLAT_FRIEND, rmsg)
    if not ok then
        return FriendCode.ERR_FRIEND_RMSG_FAILED
    end
    data_mgr:notify_player(target_id, "do_friend_message")

    self:report_dlog_friend(player_id, {opt_type = 2, friend_id = target_id,})
    return SUCCESS
end

-------------------------------------------------------------------------------
--检查好友隐私
function FriendServlet:on_friend_secret_req(data)
    local player_id, target_id = data.player_id, data.target_id
    local player = player_mgr:get_player(player_id)
    if player then
        local social_secret = player:get_social_secret()
        if social_secret == SocialSecret.PUBLIC then
            return { code = SUCCESS }
        end
        if social_secret == SocialSecret.FRIEND then
            if player:get_friend(target_id) then
                return { code = SUCCESS }
            end
        end
        return { code = KernCode.SERVER_FAILED }
    end
    return { code = KernCode.PLAYER_NOT_EXIST }
end

--添加附近好友
function FriendServlet:on_friend_build_recent_req(data)
    local player_id, targets = data.player_id, data.targets
    local player = player_mgr:get_player(player_id)
    if player then
        local friend_type = FriendType.NEAR
        for _, target_id in pairs(targets) do
            log_debug("[FriendServlet][on_friend_build_recent_req] player_id:%s, target_id:%s", player_id, target_id)
            if target_id ~= player_id then
                local friend = player:get_friend(target_id)
                if friend then
                    player:add_intimacy(target_id, PlatCommon.FIGHT_INTIMACY)
                    self:sync_friend_info(player, friend, target_id)
                    event_mgr:notify_trigger("on_add_reddot_req", player, ReddotType.INTIMACY, target_id)
                else
                    if player:get_near_count() >= FriendLimit.NEAR then
                        self:remove_limit_friend(player, friend_type)
                    end
                    local nfriend = player:add_friend(target_id, friend_type)
                    self:sync_friend_info(player, nfriend, target_id)
                end
            end
        end
    end
    return { code = SUCCESS }
end

--修改友好度
function FriendServlet:on_friend_add_intimacy_req(data)
    local player_id, target_id, intimacy = data.player_id, data.target_id, data.intimacy
    log_debug("[FriendServlet][on_friend_add_intimacy_req] player_id:%s, target_id:%s intimacy:%s", player_id, target_id, intimacy)
    local player = player_mgr:get_player(player_id)
    if player then
        local friend = player:get_friend(target_id)
        if friend then
            player:add_intimacy(target_id, intimacy)
            self:sync_friend_info(player, friend, target_id)
            event_mgr:notify_trigger("on_add_reddot_req", player, ReddotType.INTIMACY, target_id)
        end
    end
    return { code = SUCCESS }
end

--修改设置
function FriendServlet:on_friend_setup_req(player, player_id, data)
    local setup_type, setup_status = data.setup_type, data.setup_status
    log_debug("[FriendServlet][on_friend_setup_req] player_id:%s, type:%s, status:%s", player_id, setup_type, setup_status)
    if setup_type == FriendSetup.SETUP_ONLINE then
        player:update_data({ online_status = setup_status }, true)
    elseif setup_type == FriendSetup.SETUP_SECRET then
        player:change_secret(setup_status)
    end
    data.friend_id = player_id
    self:update_friend_setup(player, data)
    return { code = SUCCESS, setup_type = setup_type, setup_status = setup_status }
end

--修改备注
function FriendServlet:on_friend_remarks_req(player, player_id, data)
    local target_id, remarks = data.target_id, data.remarks
    log_debug("[FriendServlet][on_friend_remarks_req] player_id:%s, target_id:%s, remarks:%s", player_id, target_id, remarks)
    local friend = player:get_friend(target_id)
    if not friend then
        return { code = FriendCode.ERR_FRIEND_NOT_IS_FRIEND }
    end
    if fword_mgr:check(remarks) then
        return { code = SafeCode.HAS_FILTERWORD }
    end
    player:set_remarks(target_id, remarks)
    self:sync_friend_info(player, friend, target_id)
    return { code = SUCCESS }
end

--添加分组
function FriendServlet:on_friend_group_add_req(player, player_id, data)
    local group_name = data.group_name
    log_debug("[FriendServlet][on_friend_group_add_req] player_id:%s, group_name:%s", player_id, group_name)
    player:add_group(group_name)
    player:send(CSCmdID.NID_FRIEND_GROUP_NTF, player:pack_groups())
    return { code = SUCCESS }
end

--删除分组
function FriendServlet:on_friend_group_del_req(player, player_id, data)
    local group_id = data.group_id
    log_debug("[FriendServlet][on_friend_group_del_req] player_id:%s, group_id:%s", player_id, group_id)
    local group = player:get_group(group_id)
    if not group then
        return { code = FriendCode.ERR_FRIEND_GROUP_NOT_EXIST }
    end
    local move_friends = player:del_group(group, group_id)
    for friend_id, friend in pairs(move_friends) do
        self:sync_friend_info(player, friend, friend_id)
    end
    player:send(CSCmdID.NID_FRIEND_GROUP_NTF, player:pack_groups())
    return { code = SUCCESS }
end

--交换分组
function FriendServlet:on_friend_group_swap_req(player, player_id, data)
    local group_id, index = data.group_id, data.index
    log_debug("[FriendServlet][on_friend_group_swap_req] player_id:%s, group_id:%s, index: %s", player_id, group_id, index)
    local group = player:get_group(group_id)
    if not group then
        return { code = FriendCode.ERR_FRIEND_GROUP_NOT_EXIST }
    end
    local sindex = group.index
    if sindex == index or sindex + 1 == index then
        --移到自己前面和后面不处理
        return { code = SUCCESS }
    end
    player:swap_group(group, index)
    player:send(CSCmdID.NID_FRIEND_GROUP_NTF, player:pack_groups())
    return { code = SUCCESS }
end

--修改分组
function FriendServlet:on_friend_group_modify_req(player, player_id, data)
    local group_id, group_name = data.group_id, data.group_name
    log_debug("[FriendServlet][on_friend_group_modify_req] player_id:%s, group_id:%s, group_name: %s", player_id, group_id, group_name)
    local group = player:get_group(group_id)
    if not group then
        return { code = FriendCode.ERR_FRIEND_GROUP_NOT_EXIST }
    end
    player:modify_group(group, group_name)
    player:send(CSCmdID.NID_FRIEND_GROUP_NTF, player:pack_groups())
    return { code = SUCCESS }
end

--移到分组
function FriendServlet:on_friend_group_move_req(player, player_id, data)
    local target_id = data.target_id
    local group_id = data.group_id or 0
    log_debug("[FriendServlet][on_friend_group_move_req] player_id:%s, group_id:%s, target_id: %s", player_id, group_id, target_id)
    if group_id > 0 then
        local group = player:get_group(group_id)
        if not group then
            return { code = FriendCode.ERR_FRIEND_GROUP_NOT_EXIST }
        end
    end
    local friend = player:get_friend(target_id)
    if not friend then
        return { code = FriendCode.ERR_FRIEND_NOT_IS_FRIEND }
    end
    player:move2group(target_id, group_id)
    self:sync_friend_info(player, friend, target_id)
    return { code = SUCCESS }
end

--搜索玩家
function FriendServlet:on_friend_search_req(player, player_id, data)
    local nick, target_id = data.nick, data.player_id
    log_debug("[FriendServlet][on_friend_search_req] player_id=%s, target_id:%s, nick:%s", player_id, target_id, nick)
    local player_datas = data_mgr:search_friends(player_id, nick, target_id)
    return { code = SUCCESS, friends = player_datas }
end

--添加好友/黑名单
function FriendServlet:on_friend_add_req(player, player_id, data)
    local friend_type, target_id = data.friend_type, data.target_id
    log_debug("[FriendServlet][on_friend_add_req] player_id:%s, friend_type:%s, target_id:%s", player_id, friend_type, target_id)
    if player_id == target_id then
        return { code = KernCode.OPERATOR_SELF }
    end
    local friend = player:get_friend(target_id)
    if friend then
        if friend.friend_type == FriendType.FRIEND and friend_type == FriendType.APPLY then
            return { code = self:friend_exist_code(FriendType.FRIEND, true) }
        end
        if friend.friend_type == FriendType.BLACK and friend_type == FriendType.BLACK then
            return { code = self:friend_exist_code(FriendType.BLACK, true) }
        end
    end
    if friend_type == FriendType.BLACK then
        local rmsg = { msg = "do_friend_del", data = player_id }
        local ok = rmsg_friend:send_message(quanta.name, target_id, RmsgPlat.PLAT_FRIEND, rmsg )
        if not ok then
            return { code = FriendCode.ERR_FRIEND_RMSG_FAILED }
        end
        data_mgr:notify_player(target_id, "do_friend_message")
        local nfriend = player:add_friend(target_id, friend_type)
        self:sync_friend_info(player, nfriend, target_id)
        return { code = SUCCESS }
    end
    return { code = self:apply_add_friend(player, player_id, target_id) }
end

--删除好友/黑名单
function FriendServlet:on_friend_del_req(player, player_id, data)
    local target_id, friend_type = data.target_id, data.friend_type
    log_debug("[FriendServlet][on_friend_del_req] player_id:%s, target_id:%s", player_id, target_id)
    local friend = player:get_friend(target_id)
    if not friend then
        return { code = self:friend_exist_code(friend_type, false) }
    end

    if friend_type == FriendType.FRIEND then
        local rmsg = { msg = "do_friend_del", data = player_id }
        local ok = rmsg_friend:send_message(quanta.name, target_id, RmsgPlat.PLAT_FRIEND, rmsg )
        if not ok then
            return { code = FriendCode.ERR_FRIEND_RMSG_FAILED }
        end
        data_mgr:notify_player(target_id, "do_friend_message")
    end
    player:del_friend(target_id)
    player:send(CSCmdID.NID_FRIEND_DEL_NTF, { target_id = target_id })

    self:report_dlog_friend(player_id, {opt_type = 3, friend_id = target_id,})
    return { code = SUCCESS }
end

--回复申请
function FriendServlet:on_friend_reply_req(player, player_id, data)
    local reply, target_id = data.reply, data.target_id
    log_debug("[FriendServlet][on_friend_reply_req] player_id:%s, reply:%s, target_id:%s", player_id, reply, target_id)
    local friend = player:get_friend(target_id)
    if not friend or friend.friend_type ~= FriendType.APPLY then
        return { code = FriendCode.ERR_FRIEND_NOT_IS_APPLY }
    end
    if reply > 0 then
        if player:get_friend_count() >= FriendLimit.FRIEND then
            return { code = FriendCode.ERR_FRIEND_NUMBER_IS_LIMIT }
        end
        local rmsg = { msg = "do_friend_reply", data = player:get_data() }
        local ok = rmsg_friend:send_message(quanta.name, target_id, RmsgPlat.PLAT_FRIEND, rmsg)
        if not ok then
            return { code = FriendCode.ERR_FRIEND_RMSG_FAILED }
        end
        local nfriend, count = player:add_friend(target_id, FriendType.FRIEND)
        self:sync_friend_info(player, nfriend, target_id)
        event_mgr:notify_listener("rpc_subscribe_dispatch", "evt_friend_add", {player_id = player_id, count = count})
        --目标处理
        data_mgr:notify_player(target_id, "do_friend_message")

        self:report_dlog_friend(player_id, {opt_type = 1, friend_id = target_id,})
    else
        --删除申请列表
        player:del_friend(target_id)
        player:send(CSCmdID.NID_FRIEND_DEL_NTF, { target_id = target_id })
        --通知对方删除请求列表
        local rmsg = { msg = "do_request_del", data = player_id }
        rmsg_friend:send_message(quanta.name, target_id, RmsgPlat.PLAT_FRIEND, rmsg )
        data_mgr:notify_player(target_id, "do_friend_message")
    end
    return { code = SUCCESS }
end

--执行回复
function FriendServlet:do_friend_reply(player, player_data)
    local target_id = player_data.player_id
    local player_id = player:get_player_id()
    log_debug("[FriendServlet][do_friend_reply] player_id:%s, target_id:%s", player_id, target_id)
    local nfriend, count = player:add_friend(target_id, FriendType.FRIEND)
    player:del_request(target_id)
    self:sync_friend_info(player, nfriend, target_id)
    event_mgr:notify_listener("rpc_subscribe_dispatch", "evt_friend_add", {player_id = player_id, count = count})
end

--执行申请
function FriendServlet:do_friend_add(player, player_data)
    local friend_type = FriendType.APPLY
    local target_id = player_data.player_id
    log_debug("[FriendServlet][do_friend_add] player_id:%s, target_id:%s", player:get_player_id(), target_id)
    local friend = player:get_friend(target_id)
    if friend and friend.friend_type ~= FriendType.NEAR then
        --已经在列表中，除了附近
        return
    end
    if player:get_apply_count() >= FriendLimit.APPLY then
        self:remove_limit_friend(player, friend_type)
    end
    local friend_data = {
        friend = player_data,
    }
    player:add_friend(target_id, friend_type)
    player:send(CSCmdID.NID_FRIEND_ADD_NTF, friend_data)
end

--执行删除
function FriendServlet:do_friend_del(player, target_id)
    log_debug("[FriendServlet][do_friend_del] player_id:%s, target_id:%s", player:get_player_id(), target_id)
    local friend = player:get_friend(target_id)
    if friend and friend.friend_type ~= FriendType.BLACK then
        player:del_friend(target_id)
        player:send(CSCmdID.NID_FRIEND_DEL_NTF, { target_id = target_id })
    end
end

--执行删除请求
function FriendServlet:do_request_del(player, target_id)
    log_debug("[FriendServlet][do_request_del] player_id:%s, target_id:%s", player:get_player_id(), target_id)
    player:del_request(target_id)
    local friend = player:get_friend(target_id)
    if friend and friend.friend_type == FriendType.APPLY then
        player:del_friend(target_id)
        player:send(CSCmdID.NID_FRIEND_DEL_NTF, { target_id = target_id })
    end
end

--执行回调
function FriendServlet:do_friend_message(player_id)
    local player = player_mgr:get_player(player_id)
    if player then
        local records = rmsg_friend:list_message(player_id)
        for _, record in pairs(records or {}) do
            local body = record.body
            event_mgr:notify_listener(body.msg, player, body.data)
            rmsg_friend:delete_message(player_id, record.uuid)
        end
    end
end

-- 精分上报-好友流水
function FriendServlet:report_dlog_friend(player_id, info)
    local player = player_mgr:get_player(player_id)
    if not player then return end

    local target_data = data_mgr:get_player_data(info.friend_id)
    local special_fields = {}
    special_fields.opt_type = info.opt_type
    special_fields.friends_character_id    = info.friend_id
    special_fields.friends_character_name  = target_data.nick
    special_fields.friends_character_level = target_data.level
    special_fields.friends_num             = player:get_friend_count()
    -- 精分上报-好友流水
    dlog_mgr:send_dlog_game_friend_acts_klbq({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
end

-- 查询好友关系
function FriendServlet:on_friendship_query_req(rpc_req)
    log_debug("[FriendServlet][on_friendship_query_req] rpc_req:%s", serialize(rpc_req))
    local player_group = rpc_req.player_group
    local friendship_map = {}
    local friendship = {}
    for _, group in pairs(player_group) do
        local group_cnt = #group
        for i = 1, group_cnt, 1 do
            local player_id = group[i]
            if friendship_map[player_id] then
                goto continue
            end

            local player = player_mgr:get_player(player_id)
            if not player then
                goto continue
            end

            for j = i + 1, group_cnt, 1 do
                local target_id = group[j]
                local friend = player:get_friend(target_id)
                if not friend or friend.friend_type ~= FriendType.FRIEND then
                    goto continue
                end
                friendship_map[player_id] = true
                friendship_map[target_id] = true
                tinsert(friendship, player_id)
                tinsert(friendship, target_id)
            end

            ::continue::
        end
    end

    return {code = SUCCESS, friendship = friendship}
end

-- export
quanta.friend_servlet     = FriendServlet()

return FriendServlet
