--chat_mgr.lua
local new_guid      = guid.new
local env_number    = environ.number
local tinsert       = table.insert
local tdelete       = table_ext.delete

local ChatType      = enum("ChatType")
local PeriodTime    = enum("PeriodTime")

local WORLD_CHAT_RESET_TIME     = 60           -- 群聊频率限制重置周期
local WORLD_CHAT_PERIIOD_COUNT  = 5            -- 群聊中每个玩家一个周期最多能发送聊天的次数
--local AREA_CHAT_RESET_TIME      = 15           -- 群聊频率限制重置周期
--local AREA_CHAT_PERIIOD_COUNT   = 5            -- 群聊中每个玩家一个周期最多能发送聊天的次数

local timer_mgr     = quanta.get("timer_mgr")

local ChatMgr = singleton()
local prop = property(ChatMgr)
prop:accessor("common_groups", {})  -- 公共群
prop:accessor("chat_groups", {})    -- 群列表
prop:accessor("player_groups", {})  -- 玩家私有群

function ChatMgr:__init()
    self.area_id = env_number("QUANTA_AREA_ID")
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:on_timer()
    end)
    timer_mgr:once(PeriodTime.SECOND_MS, function()
        local world_group = self:create_chat_group(ChatType.WORLD, "世界")
        world_group:set_limit(WORLD_CHAT_RESET_TIME, WORLD_CHAT_PERIIOD_COUNT)
        --[[
        local area_group = self:create_chat_group(ChatType.AREA, "分区")
        area_group:set_limit(AREA_CHAT_RESET_TIME, AREA_CHAT_PERIIOD_COUNT)
        area_group:set_area_id(self.area_id)
        ]]
    end)
end

-- 创建一个新的群
function ChatMgr:create_chat_group(type, name)
    local ChatGroup = import("platform/object/chat_group.lua")
    local group_id  = new_guid(quanta.index, type)
    local group = ChatGroup(group_id, type, name)
    self.chat_groups[group_id] = group
    if group:is_public() then
        self.common_groups[group_id] = group
    end
    return group
end

-- 销毁一个群
function ChatMgr:destory_chat_group(group_id)
    local group = self.chat_groups[group_id]
    if group then
        group:destory()
        self.chat_groups[group_id] = nil
    end
end

-- 查询指定群是否存在
function ChatMgr:get_chat_group(group_id)
    return self.chat_groups[group_id]
end

-- 注册player_id和group_id
function ChatMgr:register_player_group(player_id, group_id)
    local player_groups = self.player_groups[player_id]
    if not player_groups then
        player_groups = { }
        self.player_groups[player_id] = player_groups
    end
    tinsert(player_groups, group_id)
end

-- 反注册player和group_id
function ChatMgr:unregister_player_group(player_id, group_id)
    local player_groups = self.player_groups[player_id]
    if player_groups then
        tdelete(player_groups, group_id)
    end
end

-- 获取当前玩家所在的全部群
function ChatMgr:get_player_groups(player_id)
    local groups = {}
    for group_id, group in pairs(self.common_groups) do
        tinsert(groups, group)
    end
    for _, group_id in pairs(self.player_groups[player_id] or {}) do
        local group = self:get_chat_group(group_id)
        if group then
            tinsert(groups, group)
        end
    end
    return groups
end

-- tick回调
function ChatMgr:on_timer()
    local cur_time = quanta.now
    for _, group in pairs(self.chat_groups) do
        group:on_tick(cur_time)
    end
end

-- export
quanta.chat_mgr = ChatMgr()

return ChatMgr