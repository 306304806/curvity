--!!!!以下代码由工具自动生成，如需修改请前往生成文件dlog2lua.lua!!!!
--dlog_mgr.lua
local log_debug      = logger.debug
local otime          = os.time
local odate          = os.date
local config_mgr     = quanta.get("config_mgr")
local utility_db     = config_mgr:init_table("utility", "id")
local router_mgr     = quanta.get("router_mgr")
local thread_mgr     = quanta.get("thread_mgr")

local DlogGameMail = import("dlog_plat/dlog_game_mail.lua")
local DlogGameChat = import("dlog_plat/dlog_game_chat.lua")
local DlogGameFriendActsKlbq = import("dlog_plat/dlog_game_friend_acts_klbq.lua")
local DlogGameRankKlbq = import("dlog_plat/dlog_game_rank_klbq.lua")
local DlogGameDeposite = import("dlog_plat/dlog_game_deposite.lua")

local DlogMgr = singleton()

function DlogMgr:__init()
end

function DlogMgr:fill_public_fields(dlog_obj, ctx)
    dlog_obj["event_time"] = odate("%Y-%m-%d %H:%M:%S", otime())
    dlog_obj["game_appkey"] = utility_db:find_one("dlog_app_key").value
    dlog_obj["zone_id"] = ctx.public_fields.area_id
    dlog_obj["open_id"] = ctx.public_fields.open_id or ctx.public_fields.character_id
    dlog_obj["sdk_device_id"] = ""
    dlog_obj["platform"] = utility_db:find_one("dlog_plat_id").value
    dlog_obj["channel_group_id"] = ""
    dlog_obj["channel_id"] = ""
    dlog_obj["game_version"] = "1.0.0"
    dlog_obj["character_id"] = ctx.public_fields.character_id
    dlog_obj["character_level"] = ctx.public_fields.character_level
    dlog_obj["vip_level"] = ""
end


-- 邮件
function DlogMgr:send_dlog_game_mail(ctx)
    local function send_func()
        local dlog_obj = DlogGameMail()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 聊天记录
function DlogMgr:send_dlog_game_chat(ctx)
    local function send_func()
        local dlog_obj = DlogGameChat()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 好友事件流水
function DlogMgr:send_dlog_game_friend_acts_klbq(ctx)
    local function send_func()
        local dlog_obj = DlogGameFriendActsKlbq()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 排行榜
function DlogMgr:send_dlog_game_rank_klbq(ctx)
    local function send_func()
        local dlog_obj = DlogGameRankKlbq()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

-- 付费充值
function DlogMgr:send_dlog_game_deposite(ctx)
    local function send_func()
        local dlog_obj = DlogGameDeposite()
        if dlog_obj.has_public then
            self:fill_public_fields(dlog_obj, ctx)
        end
        local report_str = dlog_obj:contruct_report_str(ctx)
        self:send(report_str)
    end
    thread_mgr:fork(send_func)
end

function DlogMgr:send(report_str)
    log_debug("dlog report_str:%s", report_str)
    router_mgr:call_proxy_hash(quanta.id, "rpc_http_post", utility_db:find_one("dlog_report_url").value, {}, report_str, {})
end

-- export
quanta.dlog_mgr    = DlogMgr()

return DlogMgr