--init.lua
import("constant/plat_code.lua")
import("constant/plat_const.lua")
import("dlog_plat/dlog_mgr.lua")

import("kernel/store/db_agent.lua")

import("platcenter/dao/rank_dao.lua")
import("platcenter/dao/player_dao.lua")
import("platcenter/dao/cooperate_dao.lua")

import("platcenter/object/season_mgr.lua")
import("platcenter/object/shop_mgr.lua")
import("platcenter/object/rank_mgr.lua")
import("platcenter/object/cooperate_mgr.lua")
import("platcenter/object/address_mgr.lua")

import("platcenter/servlet/rank_servlet.lua")
import("platcenter/servlet/safe_servlet.lua")
