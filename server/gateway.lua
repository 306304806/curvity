#!./quanta
import("kernel.lua")
local ljson         = require("luacjson")

local log_info      = logger.info
local env_addr      = environ.addr
local json_encode   = ljson.encode
local json_decode   = ljson.decode
local qxpcall       = quanta.xpcall
local quanta_update = quanta.update
local qxpcall_quit  = quanta.xpcall_quit

quanta.run = function()
    qxpcall(quanta_update, "quanta_update error: %s")
end

if not quanta.init_flag then
    local function startup()
        --初始化quanta
        quanta.init()
        --初始化gateway
        local NetServer = import("kernel/network/net_server.lua")
        --创建客户端网络管理
        local client_mgr = NetServer("gate_client")
        local cip, cport = env_addr("QUANTA_PLATC_ADDR")
        client_mgr:setup(cip, cport, true)
        quanta.client_mgr = client_mgr
        --创建服务端网络管理
        local server_mgr = NetServer("gate_server")
        local sip, sport = env_addr("QUANTA_PLATS_ADDR")
        server_mgr:setup(sip, sport, true)
        quanta.server_mgr = server_mgr
        --设置编解码器
        server_mgr:set_encoder(function(cmd_id, data)
            return json_encode(data)
        end)
        server_mgr:set_decoder(function(cmd_id, data)
            local cmd_name = "json." .. cmd_id
            return json_decode(data), cmd_name
        end)

        import("gateway/gateway.lua")
        log_info("gateway %d now startup!", quanta.id)
    end
    ljson.encode_sparse_array(true)
    qxpcall_quit(startup, "quanta startup error: %s")
    quanta.init_flag = true
end
