--reddot_servlet.lua

local log_debug     = logger.debug

local KernCode      = enum("KernCode")
local SSCmdID       = enum("PlatSSCmdID")

local SUCCESS       = KernCode.SUCCESS
local CSCmdID       = ncmd_plat.CCmdId

local player_mgr    = quanta.get("player_mgr")
local event_mgr     = quanta.get("event_mgr")

local ReddotServlet = singleton()
function ReddotServlet:__init()
    event_mgr:add_trigger(self, "on_client_login")
    event_mgr:add_trigger(self, "on_add_reddot_req")

    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_REDDOT_ADD_REQ,  "on_ss_reddot_add_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_REDDOT_ADD_REQ,  "on_player_reddot_add_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_REDDOT_READ_REQ, "on_player_reddot_read_req")
end

function ReddotServlet:on_client_login(player, player_id)
    player:send(CSCmdID.NID_REDDOT_SYNC_NTF, player:pack_reddots())
end

function ReddotServlet:on_add_reddot_req(player, reddot_type, reddot_rid, overtime, custom)
    log_debug("[ReddotServlet][on_add_reddot_req] player_id:%s, type:%s, reddot_rid:%s", player:get_player_id(), reddot_type, reddot_rid)
    local reddot = player:add_reddot(reddot_type, reddot_rid, overtime, custom)
    player:send(CSCmdID.NID_REDDOT_ADD_NTF, { reddot = reddot })
end

-- 添加红点
function ReddotServlet:on_ss_reddot_add_req(data)
    local player_id = data.player_id
    local player = player_mgr:get_player(player_id)
    if player then
        self:on_add_reddot_req(player, data.reddot_type, data.reddot_rid, data.overtime, data.custom)
    end
    return { code = SUCCESS }
end

function ReddotServlet:on_player_reddot_add_req(player, player_id, data)
    self:on_add_reddot_req(player, data.reddot_type, data.reddot_rid, data.overtime, data.custom)
    return { code = SUCCESS }
end

function ReddotServlet:on_player_reddot_read_req(player, player_id, data)
    local reddot_id = data.reddot_id
    log_debug("[ReddotServlet][on_player_reddot_read_req] player_id:%s, reddot_id:%s", player_id, reddot_id)
    player:read_reddot(reddot_id)
    return { code = SUCCESS }
end

-- export
quanta.reddot_servlet   = ReddotServlet()

return ReddotServlet
