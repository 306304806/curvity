--player_mgr.lua
local WheelMap      = import("kernel/basic/wheel_map.lua")
local log_err       = logger.err
local log_info      = logger.info
local log_debug     = logger.debug
local tinsert       = table.insert
local hash_code     = utility.hash_code
local check_failed  = utility.check_failed

local KernCode      = enum("KernCode")
local PlatCode      = enum("PlatCode")
local PeriodTime    = enum("PeriodTime")
local SUCCESS       = KernCode.SUCCESS

local event_mgr     = quanta.get("event_mgr")
local timer_mgr     = quanta.get("timer_mgr")
local router_mgr    = quanta.get("router_mgr")
local player_dao    = quanta.get("player_dao")
local cache_agent   = quanta.get("cache_agent")

local PlayerMgr = singleton()
local prop = property(PlayerMgr)
prop:accessor("id2player", {})

function PlayerMgr:__init()
    --注册转发消息
    event_mgr:add_listener(self, "rpc_forward_message")
    event_mgr:add_listener(self, "rpc_client_login")
    event_mgr:add_listener(self, "rpc_client_close")
    event_mgr:add_listener(self, "rpc_client_sync")
    event_mgr:add_listener(self, "rebuild_cache")

    self.id2player = WheelMap(10)

    --定时器
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:update()
    end)
end

function PlayerMgr:update()
    for _, player in self.id2player:iterator() do
        player:update()
    end
end

--请求消息
function PlayerMgr:rpc_forward_message(player_id, cmd_id, data)
    if player_id then
        local player = self:get_player(player_id)
        if player then
            return event_mgr:notify_command(cmd_id, player, player_id, data)
        end
    else
        return event_mgr:notify_command(cmd_id, data)
    end
end

--客户端上线
function PlayerMgr:rpc_client_login(gateway, player_id, data)
    local player = self:get_player(player_id)
    if player and player:get_token() == data.token then
        player:kick_out()
        player:set_gateway(gateway)
        --延迟调用上线通知
        timer_mgr:once(PeriodTime.SECOND_MS, function()
            event_mgr:notify_trigger("on_client_login", player, player_id)
        end)
        log_info("[PlayerMgr][rpc_client_login] success player_id:%s, gateway:%s, token:%s", player_id, gateway, data.token)
        return SUCCESS, player:get_data()
    end
    log_err("[PlayerMgr][rpc_client_login] failed player_id:%s, gateway:%s, token:%s", player_id, gateway, data.token)
    return PlatCode.ERR_LOGIN_TOKEN
end

--客户端需要同步
function PlayerMgr:rpc_client_sync(player_id)
    local player = self:get_player(player_id)
    if player then
        log_debug("[PlayerMgr][rpc_client_sync] player_id:%s", player_id)
        event_mgr:notify_trigger("on_player_sync", player_id, player)
    end
end

--客户端退出
function PlayerMgr:rpc_client_close(player_id)
    local player = self:get_player(player_id)
    if player then
        local gateway = player:get_gateway()
        log_info("[PlayerMgr][rpc_client_close] player_id:%s, gateway:%s", player_id, gateway)
        player:set_gateway(nil)
    end
end

--创建玩家
function PlayerMgr:create_player(player_id)
    local Player = import("platform/object/player.lua")
    local player = Player(player_id)
    local pok, player_res, image_res = player_dao:load_player(player_id)
    if not pok or not player:setup(player_res, image_res) then
        return
    end
    self.id2player:set(player_id, player)
    return player
end

--玩家登录
function PlayerMgr:player_login(player_id, player_data, token, account_data)
    local player = self.id2player:get(player_id)
    if not player then
        player = self:create_player(player_id)
    end
    if player then
        player:set_token(token)
        player:update_data(player_data)
        player:set_machine_id(account_data.machine_id)
        player:set_open_id(account_data.open_id)
        player:set_ip(account_data.ip)
    end
    return player
end

--获取玩家
function PlayerMgr:get_player(player_id)
    return self.id2player:get(player_id)
end

--删除玩家
function PlayerMgr:del_player(player, player_id)
    player:update()
    self.id2player:set(player_id, nil)
    player_dao:flush_player(player_id)
end

--重建缓存
function PlayerMgr:rebuild_cache(hash, count)
    local players = {}
    for player_id in self.id2player:iterator() do
        local hash_key = hash_code(player_id, count)
        if hash_key == hash then
            tinsert(players, player_id)
        end
    end
    if #players > 0 then
        local code1 = cache_agent:rebuild(players, "player")
        if check_failed(code1) then
            log_err("[PlayerMgr][rebuild_cache] failed: code1=%s", code1)
        end
        local code2 = cache_agent:rebuild(players, "image")
        if check_failed(code2) then
            log_err("[PlayerMgr][rebuild_cache] failed: code2=%s", code2)
        end
    end
end

--群发消息
function PlayerMgr:send_group_message(players, cmd_id, data)
    router_mgr:call_gateway_all("rpc_group_message", players, cmd_id, data)
end

--广播消息
function PlayerMgr:boardcast_message(cmd_id, data, area_id)
    router_mgr:call_gateway_all("rpc_board_message", area_id, cmd_id, data)
end

-- export
quanta.player_mgr = PlayerMgr()

return PlayerMgr
