--account_code_dao.lua

local log_err       = logger.err
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local db_agent      = quanta.get("db_agent")

local ActiveCodeDao = singleton()
function ActiveCodeDao:__init()
end

function ActiveCodeDao:load_active_code()
    local ok, code, res = db_agent:find(quanta.id, { "plat_active_code", {}, {_id = 0}})
    if not ok or check_failed(code) then
        log_err("[ActiveCodeDao][load_active_code] failed: code: %s", code, serialize(res))
        return false
    end

    return true, res or {}
end

function ActiveCodeDao:update_active_code(active_code, data)
    local ok, code, res = db_agent:update(quanta.id, { "plat_active_code", data, {active_code = active_code}, true})
    if not ok or check_failed(code) then
        log_err("[ActiveCodeDao][update_active_code] failed! code: %s, res: %s", code, serialize(res))
        return false
    end

    return true
end

function ActiveCodeDao:delete_active_code(active_code)
    local ok, code, res = db_agent:delete(quanta.id, { "plat_active_code", {active_code = active_code}, true})
    if not ok or check_failed(code) then
        log_err("[ActiveCodeDao][delete_active_code] failed! code: %s, res: %s", code, serialize(res))
        return false
    end

    return true
end

quanta.active_code_dao = ActiveCodeDao()

return ActiveCodeDao
