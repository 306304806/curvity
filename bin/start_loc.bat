taskkill /f /fi "windowtitle eq plat_*"

start "plat_monitor"     ./quanta.exe ./local/monitor.conf    --index=1
start "plat_router1"     ./quanta.exe ./local/router.conf     --index=1
start "plat_router2"     ./quanta.exe ./local/router.conf     --index=2
start "plat_dbsvr1"      ./quanta.exe ./local/dbsvr.conf      --index=1
start "plat_gateway1"    ./quanta.exe ./local/gateway.conf    --index=1
start "plat_platcenter1" ./quanta.exe ./local/platcenter.conf --index=1
start "plat_platform1"   ./quanta.exe ./local/platform.conf   --index=1
start "plat_proxy"       ./quanta.exe ./local/proxy.conf      --index=1
start "plat_cachesvr"    ./quanta.exe ./local/cachesvr.conf   --index=1

exit
