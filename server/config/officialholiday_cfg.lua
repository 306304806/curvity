--officialholiday_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local officialholiday = config_mgr:get_table("officialholiday")

--导出版本号
officialholiday:set_version(10000)

--导出配置内容
officialholiday:upsert({
    id = 1,
    holiday = 1577808000,
})

officialholiday:upsert({
    id = 2,
    holiday = 1579795200,
})

officialholiday:upsert({
    id = 3,
    holiday = 1579881600,
})

officialholiday:upsert({
    id = 4,
    holiday = 1579968000,
})

officialholiday:upsert({
    id = 5,
    holiday = 1580054400,
})

officialholiday:upsert({
    id = 6,
    holiday = 1580140800,
})

officialholiday:upsert({
    id = 7,
    holiday = 1580227200,
})

officialholiday:upsert({
    id = 8,
    holiday = 1580313600,
})

officialholiday:upsert({
    id = 9,
    holiday = 1580400000,
})

officialholiday:upsert({
    id = 10,
    holiday = 1580486400,
})

officialholiday:upsert({
    id = 11,
    holiday = 1580572800,
})

officialholiday:upsert({
    id = 12,
    holiday = 1585929600,
})

officialholiday:upsert({
    id = 13,
    holiday = 1586016000,
})

officialholiday:upsert({
    id = 14,
    holiday = 1586102400,
})

officialholiday:upsert({
    id = 15,
    holiday = 1588262400,
})

officialholiday:upsert({
    id = 16,
    holiday = 1588348800,
})

officialholiday:upsert({
    id = 17,
    holiday = 1588435200,
})

officialholiday:upsert({
    id = 18,
    holiday = 1588521600,
})

officialholiday:upsert({
    id = 19,
    holiday = 1588608000,
})

officialholiday:upsert({
    id = 20,
    holiday = 1593014400,
})

officialholiday:upsert({
    id = 21,
    holiday = 1593100800,
})

officialholiday:upsert({
    id = 22,
    holiday = 1593187200,
})

officialholiday:upsert({
    id = 23,
    holiday = 1601481600,
})

officialholiday:upsert({
    id = 24,
    holiday = 1601568000,
})

officialholiday:upsert({
    id = 25,
    holiday = 1601654400,
})

officialholiday:upsert({
    id = 26,
    holiday = 1601740800,
})

officialholiday:upsert({
    id = 27,
    holiday = 1601827200,
})

officialholiday:upsert({
    id = 28,
    holiday = 1601913600,
})

officialholiday:upsert({
    id = 29,
    holiday = 1602000000,
})

officialholiday:upsert({
    id = 30,
    holiday = 1602086400,
})
