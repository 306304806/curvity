--rank_cache_mgr.lua

local RankCacheObj  = import("platform/object/rank_cache_obj.lua")

local log_err       = logger.err

local config_mgr    = quanta.get("config_mgr")

local rank_db       = config_mgr:init_table("rank", "id")

local RankCacheMgr = singleton()
local prop = property(RankCacheMgr)
prop:accessor("rank_cache_objs", {})

function RankCacheMgr:__init()
    self:setup()
end

function RankCacheMgr:setup()
    for _, rank_cfg in rank_db:iterator() do
        local rank_cache_obj = RankCacheObj()
        local rank_type = rank_cfg.rank_type
        if rank_cache_obj:setup(rank_cfg) then
            self.rank_cache_objs[rank_type] = rank_cache_obj
        else
            log_err("[RankCacheMgr]create rank_cache_obj failed! rank_type:%s", rank_type)
        end
    end
end

-- 获取排行榜对象
function RankCacheMgr:get_rank_obj(rank_type)
    local rank_cache_obj = self.rank_cache_objs[rank_type or 0]
    return rank_cache_obj
end

-- export
quanta.rank_cache_mgr = RankCacheMgr()

return RankCacheMgr
