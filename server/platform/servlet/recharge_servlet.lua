--recharge_servlet.lua
-- 全局邮件管理器,用于即使推送

local otime         = os.time
local new_guid      = guid.new
local log_info      = logger.info
local log_warn      = logger.warn
local log_err       = logger.err
local tinsert       = table.insert
local tsize         = table_ext.size
local tointeger     = math.tointeger
local check_failed  = utility.check_failed

local CSCmdID               = ncmd_plat.CCmdId
local SSCmdID               = enum("PlatSSCmdID")
local KernCode              = enum("KernCode")
local RechargeCode          = enum("RechargeCode")
local RechargeOrderState    = enum("RechargeOrderState")
local RechargePlatformId    = enum("RechargePlatformId")
local PeriodTime            = enum("PeriodTime")
local DlogRechageState      = enum("DlogRechageState")

local auth_mgr              = quanta.get("auth_mgr")
local dlog_mgr              = quanta.get("dlog_mgr")
local steam_api             = quanta.get("steam_api")
local timer_mgr             = quanta.get("timer_mgr")
local event_mgr             = quanta.get("event_mgr")
local player_mgr            = quanta.get("player_mgr")
local config_mgr            = quanta.get("config_mgr")
local recharge_mgr          = quanta.get("recharge_mgr")

local recharge_db           = config_mgr:init_table("recharge", "commodity_id")

local new_order_id = function()
    return tostring(new_guid())
end

local RechargeServlet = singleton()
--local prop = property(RechargeServlet)

function RechargeServlet:__init()
    event_mgr:add_trigger(self, "on_client_login")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_RECHARGE_COMMODITY_SYNC_REQ, "on_recharge_commodity_sync_req")
    event_mgr:add_cmd_listener(self, CSCmdID.NID_RECHARGE_ORDER_CREATE_REQ,   "on_recharge_order_create_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_RECHARGE_ORDER_QUERY_REQ, "on_ss_recharge_order_query_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_RECHARGE_ORDER_SEND_REQ,  "on_ss_recharge_order_send_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_RECHARGE_WS_ORDER_IDS_QUERY_REQ, "on_ss_recharge_ws_order_ids_query_req")

    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:on_timer()
    end)

    event_mgr:add_trigger(self, "evt_dlog_rechage_update")
end

function RechargeServlet:on_timer()
    self:_update_unfinish_orders()
    self:_update_unconfirmed_orders()
end

function RechargeServlet:on_client_login(player, player_id)
    recharge_mgr:load_player_wait_send_orders(player_id)
end

-- rpc 查询订单
function RechargeServlet:on_ss_recharge_order_query_req(rpc_req)
    local rpc_res = {}

    return rpc_res
end

-- 订单发货
function RechargeServlet:on_ss_recharge_order_send_req(rpc_req)
    local rpc_res = {
        code       = KernCode.SUCCESS,
        send_ok    = false,
        order_info = nil,
    }

    log_info("[RechargeServlet][on_ss_recharge_order_send_req] player_id=%s,order_id=%s",
        rpc_req.player_id, rpc_req.order_id)

    if not rpc_req.order_id then
        log_warn("[RechargeServlet][on_ss_recharge_order_send_req] order_id is nil")
        rpc_res.code = RechargeCode.ORDER_ID_NOT_EXIST
        return rpc_res
    end

    -- 查询订单
    local ec
    local oinfo = recharge_mgr:query(rpc_req.order_id)
    if not oinfo then
        -- 尝试重建订单
        ec = self:_rebuild_order_from_steam(rpc_req.order_id, rpc_req.player_id)
        -- 网络错误需要添加重试
        if KernCode.NETWORK_ERROR == ec then
            log_warn("[RechargeServlet][on_ss_recharge_order_send_req] _rebuild_order_from_steam timeout: order_id=%s", rpc_req.order_id)
            -- 重建失败，加入未确认订单
            recharge_mgr:update_unconfirmed_order(rpc_req.order_id, rpc_req.player_id)
        elseif KernCode.SUCCESS == ec then  -- 创建成功，加入未完成订单
            recharge_mgr:update_unfinish_order(rpc_req.order_id, rpc_req.player_id)
        end

        log_warn("[RechargeServlet][on_ss_recharge_order_send_req] order_id not find: order_id=%s", rpc_req.order_id)
        rpc_res.code = ec
        return rpc_res
    end

    -- 根据订单状态查询是否需要支付（到支付平台确认支付）
    if RechargeOrderState.WAIT_PAY == oinfo.state then
        local steam_res
        ec, steam_res = steam_api:finalize_txn(rpc_req.order_id, true)
        if check_failed(ec) then
            log_warn("[RechargeServlet][on_ss_recharge_order_send_req] finalize_txn faild: order_id=%s, ec=%s",
                rpc_req.order_id, ec)
            -- 更新未完成订单
            recharge_mgr:update_unfinish_order(rpc_req.order_id, rpc_req.player_id)

            rpc_res.code = ec
            return rpc_res
        end
        -- 支付未完成
        if not steam_res.pay_ok then
            rpc_res.code = RechargeCode.ORDER_NOT_PAY
            return rpc_res
        end
        -- 在订单系统中标记支付
        ec = recharge_mgr:pay(rpc_req.order_id)
        if check_failed(ec) then
            log_warn("[RechargeServlet][on_ss_recharge_order_send_req] finalize_txn faild: order_id=%s, ec=%s",
                rpc_req.order_id, ec)
            rpc_res.code = ec

            return rpc_res
        end
    end

    -- 发货
    ec, oinfo = recharge_mgr:send(rpc_req.order_id)
    if check_failed(ec) then
        log_warn("[RechargeServlet][on_ss_recharge_order_send_req] send faild: order_id=%s,ec=%s", rpc_req.order_id, ec)
        rpc_res.code = ec
        return rpc_res
    end

    rpc_res.send_ok    = true
    rpc_res.order_info = oinfo

    return rpc_res
end

-- 查询未发货订单列表
function RechargeServlet:on_ss_recharge_ws_order_ids_query_req(rpc_req)
    return {
        code         = KernCode.SUCCESS,
        ws_order_ids = recharge_mgr:get_player_ws_oids(rpc_req.player_id)
    }
end

-- 获取充值商品列表
function RechargeServlet:on_recharge_commodity_sync_req(player, player_id, net_req)
    local net_res = {
        code = KernCode.SUCCESS,
        commodity_infos = {}
    }

    for id, cfg in recharge_db:iterator() do
        local commodity_info = {
            commodity_id   = cfg.commodity_id,
            type           = 0,
            sort_id        = 0,
            recommendation = 0,
            amount         = cfg.amount,
            name           = cfg.name,
            desc           = cfg.desc,
            items          = cfg.items,
        }

        tinsert(net_res.commodity_infos, commodity_info)
    end

    return net_res
end

-- 创建充值订单
function RechargeServlet:on_recharge_order_create_req(player, player_id, net_req)
    local net_res = {
        code = KernCode.SUCCESS
    }

    local open_id = player:get_open_id()
    if not auth_mgr:is_authed(open_id) then
        net_res.code = RechargeCode.RECHARGE_FAILED_UNAUTH
        return
    end

    log_info("[RechargeServlet][on_recharge_order_create_req] player_id=%s", player_id)

    -- 当前只支持一次购买一个物品
    local car_count = tsize(net_req.commodities or {})
    if car_count ~= 1 then
        net_res.code = RechargeCode.COMMODITY_COUNT_ERROR
        return net_res
    end
    -- 获取第一个商品的id和数量
    local item_id    = net_req.commodities[1].commodity_id
    local item_count = net_req.commodities[1].commodity_count
    -- 查询&验证商品信息
    local commodity_info = recharge_db:find_one(item_id)
    -- 目标商品不存在
    if not commodity_info then
        net_res.code = RechargeCode.COMMODITY_NOT_EXIST
        return net_res
    end
    -- 计算总价
    local total_amount = commodity_info.amount * item_count

    if auth_mgr:is_teenager_player() then
        local flag, ret_var = auth_mgr:check_recharge_amount(open_id, total_amount)
        local t_code = {
                            [1] = RechargeCode.RECHARGE_8_MAX_AMOUNT,
                            [2] = RechargeCode.RECHARGE_16_MAX_AMOUNT,
                            [3] = RechargeCode.RECHARGE_18_MAX_AMOUNT,
                            [4] = RechargeCode.RECHARGE_16_TOTAL_AMOUNT,
                            [5] = RechargeCode.RECHARGE_18_TOTAL_AMOUNT,
                      }
        if not flag then
            net_res.code = t_code[ret_var]
            return net_res
        end

        flag, ret_var = auth_mgr:check_month_recharge_amount(open_id, total_amount)
        if not flag then
            net_res.code = t_code[ret_var]
            return net_res
        end
    end

    -- 生成新订单号
    local order_id = new_order_id()

    -- 请求steam支付订单
    local ec, steam_res = steam_api:init_txn(net_req.pay_open_id, tointeger(order_id), car_count,
        item_id, item_count, commodity_info.name, net_req.language, total_amount, "CNY", true)
    if check_failed(ec) then
        net_res.code = ec
        return net_res
    end

    -- steam下单失败返回steam错误信息
    if not steam_res.ok then
        net_res.code = RechargeCode.PAY_PLATFORM_ERR
        net_res.pay_code = steam_res.steam_code
        net_res.pay_msg  = steam_res.steam_msg

        return net_res
    end

    -- 创建本地订单
    ec = recharge_mgr:create(net_req.pay_platform_id, net_req.pay_open_id, steam_res.trans_id,
        player_id, order_id, {[1]=commodity_info}, total_amount)
    if check_failed(ec) then
        net_res.code = ec
        return net_res
    end

    -- 订单创建成功
    net_res.order_id = order_id
    net_res.pay_order_id = steam_res.trans_id


    return net_res
end

-- 尝试从steam重建指定订单
function RechargeServlet:_rebuild_order_from_steam(order_id, player_id)
    log_info("[RechargeServlet][_rebuild_order_from_steam] order_id=%s", order_id)

    -- 到steam查询订单信息
    local ec, steam_res = steam_api:query_txn(order_id, true)
    -- 查询失败
    if check_failed(ec) then
        return ec
    end

    -- 订单不存在
    if not steam_res.order_id then
        return RechargeCode.ORDER_ID_NOT_EXIST
    end

    -- 复查订单信息，防止并发冲突
    local oinfo = recharge_mgr:query(order_id)
    if oinfo then
        return RechargeCode.ORDER_ID_EXIST
    end

    -- 重建商品信息
    local commodities  = {}
    local total_amount = 0
    for _, order_item in pairs(steam_res.items or {}) do
        if order_item.pay_finish then
            local commodity_info = recharge_db:find_one(order_item.item_id)
            if commodity_info then
                tinsert(commodities, commodity_info)
            else
                log_err("[RechargeServlet][_rebuild_order_from_steam] commodity_id not find: player_id=%s,order_id=%s,commodity_id=%s",
                    player_id, order_id, order_item.item_id)
            end
        end
    end

    -- 重建订单
    ec = recharge_mgr:create(RechargePlatformId.STEAM, steam_res.open_id, steam_res.trans_id,
        player_id, order_id, commodities, total_amount)
    if check_failed(ec) then
        log_err("[RechargeServlet][_rebuild_order_from_steam] create order faild: player_id=%s,order_id=%s,ec=%s",
            player_id, order_id, ec)
        return ec
    end

    return ec
end

-- 更新未结束订单
function RechargeServlet:_update_unfinish_orders()
    local unfinish_map = recharge_mgr:get_unfinish_order_map()
    local cur_time = otime()
    for order_id, info in pairs(unfinish_map or {}) do
        if cur_time < (info.last_retry_time + (info.retry_times * 3)) then
            goto lab_next
        end

        -- 内存找不到订单，需要冲数据捞，或者到steam重建
        if not recharge_mgr:query(order_id) then
            log_err("[RechargeServlet][_update_unfinish_orders] need load from db or rebuild from steam")
            goto lab_next
        end

        -- 到steam重新查询
        local ec, steam_res = steam_api:finalize_txn(info.order_id, true)
        -- 网络错误
        if KernCode.NETWORK_ERROR == ec then
            log_warn("[RechargeServlet][_update_unfinish_orders] finalize_txn faild: order_id=%s, ec=%s", info.order_id, ec)
            -- 更新未完成订单
            recharge_mgr:update_unfinish_order(info.order_id, info.player_id)
            goto lab_next
        end

        -- 检查是否已经被其他协程处理了
        if not recharge_mgr:is_unfinish_order(order_id) then
            goto lab_next
        end

        -- 支付完成
        if steam_res.pay_ok then
            -- 在订单系统中标记支付
            ec = recharge_mgr:pay(info.order_id)
            if check_failed(ec) then
                log_err("[RechargeServlet:_update_unfinish_orders] pay faild: order_id=%s,ec=%s", info.order_id, ec)
            end
        end

        -- 从未完成订单中删除
        recharge_mgr:remove_unfinish_order(info.order_id)

        :: lab_next ::
    end
end

-- 更新未确认订单
function RechargeServlet:_update_unconfirmed_orders()
    local unconfirmed_map = recharge_mgr:get_unconfirmed_order_map()
    for order_id, info in pairs(unconfirmed_map or {}) do
        if not recharge_mgr:query(order_id) then
            local ec = self:_rebuild_order_from_steam(info.order_id, info.player_id)
            -- 仍然超时
            if KernCode.NETWORK_ERROR == ec then
                recharge_mgr:update_unconfirmed_order(info.order_id, info.player_id)
            elseif KernCode.SUCCESS == ec then  -- 重建成功
                recharge_mgr:remove_unconfirmed_order(info.order_id)
                recharge_mgr:update_unfinish_order(info.order_id, info.player_id)
            elseif RechargeCode.ORDER_ID_EXIST == ec then  -- 订单不存在
                recharge_mgr:remove_unconfirmed_order(info.order_id)
            end

            goto lab_next
        end

        :: lab_next ::
    end
end

function RechargeServlet:evt_dlog_rechage_update(order_id)
    local rechage_order = recharge_mgr:query(order_id)
    if not rechage_order then return end

    local player = player_mgr:get_player(rechage_order.player_id)
    if not player then return end

    local special_fields = {}
    special_fields.product_id       = rechage_order.commodity_infos[1].commodity_id
    special_fields.product_name     = rechage_order.commodity_infos[1].name
    special_fields.product_num      = rechage_order.amount / (rechage_order.commodity_infos[1].amount or 1)
    special_fields.deposit_amount   = rechage_order.amount / 100
    special_fields.deposit_money    = rechage_order.amount / 100
    special_fields.inner_orderid    = order_id
    special_fields.outer_orderid    = rechage_order.pay_order_id
    special_fields.imei_idfa        = player:get_machine_id()
    special_fields.ip               = player:get_ip()
    special_fields.exstr1           = special_fields.product_id
    if rechage_order.state == RechargeOrderState.WAIT_PAY then
        special_fields.order_status     =  DlogRechageState.NO_PAY
    elseif rechage_order.state == RechargeOrderState.PAY_SUCCESS then
        special_fields.order_status     = DlogRechageState.WAIT_SEND
    elseif rechage_order.state == RechargeOrderState.WAIT_SEND then
        special_fields.order_status     = DlogRechageState.FINISH
    elseif rechage_order.state == RechargeOrderState.CANCLE then
        special_fields.order_status     = DlogRechageState.CANCEL
    elseif rechage_order.state == RechargeOrderState.TIMEOUT then
        special_fields.order_status     = DlogRechageState.CANCEL
    end
    -- todo:需要统计累积充值额度，暂时未提供接口，默认为0
    special_fields.total_cash       = 0
    -- 精分上报-付费充值
    dlog_mgr:send_dlog_game_deposite({public_fields = player:build_dlog_public_fields(), special_fields = special_fields})
end

-- 更新最近5分钟的未确认订单
function RechargeServlet:_update_unpay_orders()
end

-- export
quanta.recharge_servlet     = RechargeServlet()

return RechargeServlet



