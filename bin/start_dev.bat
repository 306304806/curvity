taskkill /f /fi "windowtitle eq plat_*"

start "plat_monitor"     ./quanta.exe ./develop/monitor.conf    --index=1
start "plat_router1"     ./quanta.exe ./develop/router.conf     --index=1
start "plat_router2"     ./quanta.exe ./develop/router.conf     --index=2
start "plat_dbsvr1"      ./quanta.exe ./develop/dbsvr.conf      --index=1
start "plat_gateway1"    ./quanta.exe ./develop/gateway.conf    --index=1
start "plat_platform1"   ./quanta.exe ./develop/platform.conf   --index=1
start "plat_platcenter1" ./quanta.exe ./develop/platcenter.conf --index=1
start "plat_proxy"       ./quanta.exe ./develop/proxy.conf      --index=1
start "plat_cachesvr"    ./quanta.exe ./develop/cachesvr.conf   --index=1

exit
