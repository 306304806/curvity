--order.lua
local log_err       = logger.err
local OrderStatus   = enum("OrderStatus")
local PeriodTime    = enum("PeriodTime")

local shop_mgr      = quanta.get("shop_mgr")

local Order = class()
local prop = property(Order)
prop:accessor("order_id", 0)        -- 订单id
prop:accessor("order_type", 0)      -- 订单类型
prop:accessor("shop_index", 0)      -- 商城序号
prop:accessor("buy_count", 0)       -- 购买数量
prop:accessor("price", 0)           -- 价格
prop:accessor("pay_currency_id", 0) -- 支付方式
prop:accessor("payer_id", 0)        -- 支付方id
prop:accessor("payer_name", nil)    -- 支付方名字
prop:accessor("receiver_id", 0)     -- 接收方id
prop:accessor("receiver_name", nil) -- 接收方名字
prop:accessor("expired", 0)         -- 订单过期时间
prop:accessor("status", 0)          -- 订单状态，订单的动态数据
prop:accessor("items", {})          -- 商品内容

function Order:__init()
end

function Order:setup(context)
    local shop_index = context.shop_index
    local shop_cfg_data = shop_mgr:get_shop_cfg_data(shop_index)
    if not shop_cfg_data then
        log_err("[Order][setup]->failed! get cfg failed! shop_index:%s", shop_index)
        return false
    end
    local goods_id = shop_cfg_data.goods_id
    local goods_cfg = shop_mgr:get_goods_cfg(shop_cfg_data.goods_id)
    if not goods_cfg then
        log_err("[Order][setup]->failed! get goods cfg failed! goods_id:%s", goods_id)
        return false
    end
    self.shop_index     = shop_index
    self.price          = context.price
    self.items          = goods_cfg.items
    self.order_id       = context.order_id
    self.payer_id       = context.payer_id
    self.buy_count      = context.buy_count
    self.order_type     = context.order_type
    self.payer_name     = context.payer_name
    self.receiver_id    = context.receiver_id
    self.receiver_name  = context.receiver_name
    self.pay_currency_id= context.pay_currency_id
    self.status         = context.status or OrderStatus.CREATED
    self.expired        = context.expired or self:calc_expired()
    if not self.price then
        for _, price_info in pairs(shop_cfg_data.now_price) do
            if price_info.currency_id == self.pay_currency_id then
                self.price = price_info.currency_amount
                break
            end
        end
    end
    if self.price <= 0 then
        log_err("[Order][setup]->failed! get price failed! shop_index:%s, pay_currency_id:%s", self.shop_index, self.pay_currency_id)
        return false
    end
    return true
end

-- 是否失效
function Order:is_invalid()
    return self:is_expired() or self:is_received()
end

--是否过期
function Order:is_expired()
    if self.expired > 0 then
        return quanta.now > self.expired
    else
        return false
    end
end

-- 是否已收货
function Order:is_received()
    return self.status == OrderStatus.RECIVED
end

-- 计算过期时间
function Order:calc_expired()
    return quanta.now + PeriodTime.MINUTE_5_S
end

-- 获取支付信息
function Order:get_pay_info()
    return {pay_currency_id = self.pay_currency_id, total = self.price * self.buy_count}
end

-- 获取订单发货信息
function Order:get_shipment_info()
    local shop_cfg_data = shop_mgr:get_shop_cfg_data(self.shop_index)
    return {
        items           = self.items,
        price           = self.price,
        payer_id        = self.payer_id,
        buy_count       = self.buy_count,
        order_type      = self.order_type,
        payer_name      = self.payer_name,
        receiver_id     = self.receiver_id,
        receiver_name   = self.receiver_name,
        pay_currency_id = self.pay_currency_id,
        store_type      = shop_cfg_data.store_type,
    }
end

return Order
