--mail_gm.lua
local otime         = os.time
local tunpack       = table.unpack
local log_info      = logger.info
local log_warn      = logger.warn
local serialize     = logger.serialize
local check_failed  = utility.check_failed
local tinsert       = table_ext.insert

local MailType      = enum("MailType")
local GMType        = enum("GMType")
local SSCmdID       = enum("PlatSSCmdID")

local gm_agent      = quanta.get("gm_agent")
local event_mgr     = quanta.get("event_mgr")

local MailGM = singleton()
function MailGM:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_send_mail")
    event_mgr:add_listener(self, "gm_send_global_mail")
    --注册GM
    self:register()
end

--GM服务器
function MailGM:register()
    local cmd_list = {
        {name = "gm_send_mail", desc = "发送邮件", args = "tar_player_id|number src_player_id|number title|string content|string attach|table"},
        {gm_type = GMType.AREA, name = "gm_send_global_mail", desc = "发送群邮件", args = "area_id|number title|string content|string attach|table"},
    }
    gm_agent:insert_cmd(cmd_list)
end

-- 邮件附件参数转换为附件
-- return:
--   ok: 转换成功
--   attach_items: 转换好的邮件附件
function MailGM:param_attach_2_mail_attachs(attach)
    local attach_items = {}
    for _, item_box in pairs(attach or {}) do
        if (not item_box.item_id) or (not item_box.item_count == nil) then
            return false
        end
        tinsert(attach_items, {
            item_id    = item_box.item_id,
            item_count = item_box.item_count,
        })
    end

    return true, attach_items
end

-- attach_items = {{item_id=1,item_count=1}}
-- 发送邮件
function MailGM:gm_send_mail(tar_player_id, src_player_id, title, content, attach)
    log_info("[MailGM][gm_send_mail] (%s, %s, %s, %s, %s)", tar_player_id, src_player_id, title, content, serialize(attach))
    local ok, attach_items = self:param_attach_2_mail_attachs(attach)
    if not ok then
        return {code = 1, msg = "mail attach item error"}
    end

    local rpc_req = {
        type          = MailType.SYS_MAIL,
        src_player_id = src_player_id,
        tar_player_id = tar_player_id,
        send_time     = otime(),
        title         = title,
        content       = content,
        attach_items  = attach_items
    }

    local rpc_res
    ok, rpc_res = tunpack(event_mgr:notify_command(SSCmdID.NID_SS_MAIL_SEND_REQ, rpc_req))
    if not ok or check_failed(rpc_res.code) then
        log_warn("[MailService][gm_send_mail] system send mail finish: target_id=%s", tar_player_id)
        return {code = 1, msg = " send mail failed"}
    end

    return { code = 0 }
end

-- 全局邮件
function MailGM:gm_send_global_mail(area_id, title, content, attach)
    log_info("[MailGM][gm_send_global_mail] (%s, %s, %s, %s)", area_id, title, content, serialize(attach))
    local ok, attach_items = self:param_attach_2_mail_attachs(attach)
    if not ok then
        return {code = 1, msg = "mail attach item error"}
    end
    local rpc_req = {
        type          = MailType.GLOBAL_MAIL,
        src_player_id = 0,
        tar_player_id = 0,
        send_time     = otime(),
        title         = title,
        content       = content,
        attach_items  = attach_items
    }

    local rpc_res
    ok, rpc_res = tunpack(event_mgr:notify_command(SSCmdID.NID_SS_MAIL_SEND_REQ, rpc_req))
    if not ok or check_failed(rpc_res.code) then
        log_warn("[MailService][gm_send_global_mail] system send mail failed!")
        return { code = 1, msg = " send mail failed" }
    end

    return { code = 0 }
end

-- 导出
quanta.mail_gm = MailGM()

return MailGM
