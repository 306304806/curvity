--subscribe_servlet.lua
local log_info      = logger.info
local serialize     = logger.serialize

local KernCode      = enum("KernCode")
local SSCmdID       = enum("PlatSSCmdID")
local SUCCESS       = KernCode.SUCCESS

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")

local SubscribeServlet = singleton()
function SubscribeServlet:__init()
    self.subscribe = {}
    event_mgr:add_listener(self, "rpc_subscribe_register")
    event_mgr:add_listener(self, "rpc_subscribe_dispatch")
end

function SubscribeServlet:rpc_subscribe_register(gateway, message, node_id)
    log_info("[SubscribeServlet][rpc_subscribe_register] subscribe message:%s, node_id:%s", message, node_id)
    if not self.subscribe[message] then
        self.subscribe[message] = {}
    end
    self.subscribe[message][node_id] = gateway
    return SUCCESS
end

function SubscribeServlet:rpc_subscribe_dispatch(message, data)
    log_info("[SubscribeServlet][rpc_subscribe_dispatch] subscribe message:%s, data:%s", message, serialize(data))
    local subscribers = self.subscribe[message]
    local send_data = {
        message = message,
        data = data,
    }
    for node_id, gateway in pairs(subscribers or {}) do
        router_mgr:call_target(gateway, "rpc_platform_subscribe", node_id, SSCmdID.NID_SERVER_SUBSCRIBE_NTF, send_data)
    end
    return SUCCESS
end

-- export
quanta.subscribe_servlet     = SubscribeServlet()

return SubscribeServlet