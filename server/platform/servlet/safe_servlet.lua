--safe_servlet.lua
local sfind         = string.find
local check_failed  = utility.check_failed

local KernCode      = enum("KernCode")
local SafeCode      = enum("SafeCode")
local SSCmdID       = enum("PlatSSCmdID")
local SUCCESS       = KernCode.SUCCESS

local fword_mgr     = quanta.get("fword_mgr")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local account_dao   = quanta.get("account_dao")

local SafeServlet = singleton()
function SafeServlet:__init()
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_SAFE_FILTERWORD_CHECK_REQ, "on_filterword_check")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_SAFE_FILTERWORD_TRANS_REQ, "on_filterword_trans")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_SAFE_PLAYER_NAME_USED_REQ, "on_player_name_used")
end

--敏感词检测
function SafeServlet:on_filterword_check(rpc_req)
    if fword_mgr:check(rpc_req.content) then
        return SafeCode.HAS_FILTERWORD
    end
    return SUCCESS
end

--敏感词替换
function SafeServlet:on_filterword_trans(rpc_req)
    local _, trans_result = fword_mgr:trans(rpc_req.content, rpc_req.rword)
    return {
        code      = SUCCESS,
        content   = trans_result,
    }
end

function SafeServlet:on_player_name_used(rpc_req)
    local nick = rpc_req.nick
    if utf8.len(nick) >= 32 then
        return { code = SafeCode.NICK_LEN_ERROR }
    end
    -- 空格检查
    if sfind(nick, " ") then
        return { code = SafeCode.NICK_HAS_SPACE }
    end
    -- tab检查
    if sfind(nick, "\t") then
        return { code = SafeCode.NICK_HAS_TAB }
    end
    -- 是否包含敏感词
    local has_filterword = fword_mgr:check(nick)
    if has_filterword then
        return { code = SafeCode.HAS_FILTERWORD }
    end
    -- 是否已经使用
    local ok, res = account_dao:load_player_name(nick)
    if not ok then
        return { code = KernCode.RPC_FAILED }
    end
    if res then
        return { code = SafeCode.NICK_USED }
    end
    -- 是否被锁定
    local lock_ok, lock_res = router_mgr:call_platcenter_master("rpc_lock_nick", { nick = nick })
    if not lock_ok then
        return { code = KernCode.RPC_FAILED }
    end
    if check_failed(lock_res.code) then
        return { code = lock_res.code }
    end
    return { code = SUCCESS }
end

-- export
quanta.safe_servlet     = SafeServlet()

return SafeServlet
