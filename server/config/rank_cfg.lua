--rank_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local rank = config_mgr:get_table("rank")

--导出版本号
rank:set_version(10000)

--导出配置内容
rank:upsert({
    id = 1,
    rank_type = 1,
    db_name = 'stars_rank',
    cfg_name = 'starsrank',
    global_rank = true,
    update_type = 1,
    svr_capacity = 60,
    show_capacity = 50,
    master_field = 'player_id',
})

rank:upsert({
    id = 2,
    rank_type = 1,
    db_name = 'stars_rank',
    cfg_name = 'starsrank',
    global_rank = true,
    update_type = 1,
    svr_capacity = 60,
    show_capacity = 50,
    master_field = 'player_id',
})
