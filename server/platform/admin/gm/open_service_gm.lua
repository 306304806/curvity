--open_service_gm.lua
local log_info          = logger.info

local gm_agent          = quanta.get("gm_agent")
local event_mgr         = quanta.get("event_mgr")
local open_service_mgr  = quanta.get("open_service_mgr")

local GMType            = enum("GMType")

local OpenServiceGM = singleton()
function OpenServiceGM:__init()
    --注册事件
    event_mgr:add_listener(self, "gm_set_login_mode")
    event_mgr:add_listener(self, "gm_get_login_mode")
    event_mgr:add_listener(self, "gm_set_open_time")
    event_mgr:add_listener(self, "gm_get_open_time")

    --注册GM
    self:register()
end

function OpenServiceGM:register()
    local cmd_list = {
        {gm_type = GMType.AREA, name = "gm_set_login_mode", desc = "设置登录模式GM", args = "area_id|number mode|number"},
        {gm_type = GMType.AREA, name = "gm_get_login_mode", desc = "获取登录模式GM", args = "area_id|number"},
        {gm_type = GMType.AREA, name = "gm_set_open_time", desc = "设置开服时间GM", args = "area_id|number time|number"},
        {gm_type = GMType.AREA, name = "gm_get_open_time", desc = "获取开服时间GM", args = "area_id|number"},
    }
    gm_agent:insert_cmd(cmd_list)
end

function OpenServiceGM:gm_set_login_mode(area_id, mode)
    log_info("[OpenServiceGM][gm_set_login_mode]area_id:%s, mode:%s", area_id, mode)
    local ok = open_service_mgr:set_filed_value("login_mode", mode)
    if not ok then
        return { code = 1, msg = "set_login_mode failed" }
    end
    return { code = 0 }
end

function OpenServiceGM:gm_get_login_mode(area_id)
    log_info("[OpenServiceGM][gm_get_login_mode]area_id:%s", area_id)
    local mode = open_service_mgr:get_field_value("login_mode")
    if not mode then
        return { code = 1, msg = "get_field_value failed" }
    end
    return { code = 0, data = mode }
end

function OpenServiceGM:gm_set_open_time(area_id, time)
    log_info("[OpenServiceGM][gm_set_open_time]area_id:%s, time:%s", area_id, time)
    local ok = open_service_mgr:set_filed_value("open_time", time)
    if not ok then
        return { code = 1, msg = "set_filed_value failed" }
    end
    return { code = 0 }
end

function OpenServiceGM:gm_get_open_time(area_id)
    log_info("[OpenServiceGM][gm_get_open_time]area_id:%s", area_id)
    local time = open_service_mgr:get_field_value("open_time")
    if not time then
        return { code = 1, msg = "get_field_value failed" }
    end
    return { code = 0, data = time }
end

-- export
quanta.open_service_gm = OpenServiceGM()

return OpenServiceGM