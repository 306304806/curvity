--rank_obj.lua

local otime         = os.time
local odate         = os.date
local tinsert       = table.insert
local new_guid      = guid.new
local guid_index    = guid.index
local log_err       = logger.err
local log_debug     = logger.debug
local serialize     = logger.serialize
local tcopy         = table_ext.copy

local rank_dao      = quanta.get("rank_dao")
local router_mgr    = quanta.get("router_mgr")
local timer_mgr     = quanta.get("timer_mgr")
local config_mgr    = quanta.get("config_mgr")
local season_mgr    = quanta.get("season_mgr")
local event_mgr     = quanta.get("event_mgr")

local DBLoading      = enum("DBLoading")
local PeriodTime     = enum("PeriodTime")
local RankType       = enum("RankType")
local RankUpdateType = enum("RankUpdateType")
local PlatCommon     = enum("PlatCommon")

local get_rank_season_index = function()
    return season_mgr:get_rank_season()
end

local RANK_DIR_CUSTOM = {
    [RankType.STARS] = get_rank_season_index,
}

local RankObj = class()
local prop = property(RankObj)
-- 排行配置相关数据
prop:accessor("rank_type", nil)      -- 排行榜类型
prop:accessor("config", {})          -- 排行榜配置
prop:accessor("rank_algorithm", nil) -- 排行算法
prop:accessor("cmp_fields", {})      -- 排行算法字段集

-- 排行数据
prop:accessor("rank_dir", {})        -- 排行榜索引数据 eg:{ nodes = {area_id = 1, custom = 1, stable_rank_id = 1234, lateste_rank_id = 5678}}
prop:accessor("rank_list", {})       -- 排行榜数据 eg: [rank_id] = {rank_rows}

-- 标记数据
prop:accessor("dir_loading", DBLoading.INIT)
prop:accessor("dirty_rank_dir", false)
prop:accessor("load_data_status", {})
prop:accessor("dirty_rank_data", {})

function RankObj:__init()
end

-- 初始化函数
function RankObj:setup(config)
    self.config = config
    self.rank_type = config.rank_type
    self.rank_cfg_db = config_mgr:init_table(config.cfg_name, "id")
    if not self.rank_cfg_db then
        return false
    end

    -- 筛选字段
    self.cmp_fields  = {}
    for _, data in self.rank_cfg_db:iterator() do
        if data.rank_factor then
            self.cmp_fields[data.priority] = {field_name = data.field_name, cmp = data.compare}
        end
    end

    -- 构建排行算法
    self:build_rank_algorithm()

    -- 延迟从数据库加载排行数据
    self.load_begin = otime()
    self.load_dir_timer = timer_mgr:loop(500, function()
        self:load_rank_dir_from_db()
    end)

    -- 排行榜更新
    self.last_update_time = otime()
    timer_mgr:loop(PeriodTime.SECOND_5_MS, function()
        self:timer_rank_dir_update()
        self:timer_rank_update()
    end)

    -- 定时检查存储排行索引数据
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:update_rank_dir_2db()
    end)

    -- 最新排行定时落库
    timer_mgr:loop(PeriodTime.SECOND_30_MS, function()
        self:save_latest_rank_data()
    end)

    return true
end

-- 创建新的rank dir node
function RankObj:new_rank_dir_node(area_id, custom)
    log_debug("[RankObj][new_rank_dir_node]->area_id:%s, custom:%s", area_id, custom)
    local nodes = self.rank_dir.nodes
    if not nodes then
        return false
    end

    for _, node in pairs(nodes) do
        if node.area_id == area_id and node.custom == custom then
            return false
        end
    end

    tinsert(nodes, {
                        area_id         = area_id,                          -- 小区id
                        custom          = custom,                           -- 自定义数据
                        stable_rank_id  = self:new_rank_id(area_id),        -- 稳定排行id
                        latest_rank_id  = self:new_rank_id(area_id),        -- 最新排行id
                    })

    self.dirty_rank_dir = true

    return true
end

-- 生成rank_id
function RankObj:new_rank_id(area_id)
    return new_guid(area_id, self.rank_type)
end

-- 获取rank_id
function RankObj:get_rank_id(area_id, custom)
    if self.dir_loading ~= DBLoading.SUCCESS then
        log_err("[RankObj][get_rank_id] failed! area_id:%s, custom:%s, dir_loading:%s", area_id, custom, self.dir_loading)
        return
    end

    if not self.rank_dir.nodes then
        log_err("[RankObj][get_rank_id] nodes empty! area_id:%s, custom:%s", area_id, custom)
        return
    end

    if self:is_whole_rank() then
        return self:get_whole_rank_id(custom)
    else
        return self:get_area_rank_id(area_id)
    end
end

-- 获取全区排行rank_id
function RankObj:get_whole_rank_id(custom)
    for _, data in ipairs(self.rank_dir.nodes) do
        if data.custom == custom then
            return data.stable_rank_id, data.latest_rank_id
        end
    end
end

-- 获取分区排行rank_id
function RankObj:get_area_rank_id(area_id)
    if not self:is_rank_dir_exist(area_id, 0) then
        self:new_rank_dir_node(area_id, 0)
    end

    for _, data in ipairs(self.rank_dir.nodes) do
        if data.area_id == area_id then
            return data.stable_rank_id, data.latest_rank_id
        end
    end
end

-- 获取索引自定义字段数据
function RankObj:get_rank_dir_custom()
    local func = RANK_DIR_CUSTOM[self.rank_type]
    if func then
        return func()
    else
        return 0
    end
end

-- 构建排行算法
function RankObj:build_rank_algorithm()
    self.rank_algorithm = function (cmp_fields, lhs, rhs)
        --log_debug("RankObj:build_rank_algorithm->cmp_fields:%s, lhs:%s, rhs:%s)", serialize(cmp_fields), serialize(lhs), serialize(rhs))
        for _, data in ipairs(cmp_fields) do
            local field = data.field_name
            if lhs[field] and rhs[field] and lhs[field] ~= rhs[field] then
                if data.cmp == "max" then
                    return lhs[field] > rhs[field]
                else
                    return lhs[field] < rhs[field]
                end
            end
        end

        return false
    end
end

-- 整点更新
function RankObj:timer_rank_update()
    if self.dir_loading ~= DBLoading.SUCCESS then
        return
    end

    local cur_time = otime()
    local latest_2_stable = false
    if self.config.update_type == RankUpdateType.HOUR then
        -- 整点更新
        local last_date_info = odate("*t", self.last_update_time)
        local cur_date_info = odate("*t", cur_time)
        if last_date_info.hour ~= cur_date_info.hour then
            latest_2_stable = true
            self.last_update_time = cur_time
        end
    end

    if latest_2_stable then
        log_debug("[RankObj][timer_rank_update]->rank_type:%s", self.rank_type)
        if self:is_whole_rank() then
            self:whole_latest_2_stable(cur_time)
        else
            self:area_latest_2_stable(cur_time)
        end
    end
end

-- 全局排行最新更新到稳定
function RankObj:whole_latest_2_stable(cur_time)
    local custom = self:get_rank_dir_custom()
    local stable_rank_id, latest_rank_id = self:get_rank_id(PlatCommon.GLOBAL_DEFAULT_AREA_ID, custom)
    if not stable_rank_id or not latest_rank_id then
        log_err("[RankObj][whole_latest_2_stable]->get rank_id failed! stable_rank_id:%s, latest_rank_id:%s, nodes:%s",
                 stable_rank_id, latest_rank_id, serialize(self.rank_dir.nodes))
        return
    end

    if self.load_data_status[latest_rank_id] ~= DBLoading.SUCCESS then
        return
    end

    local latest_rank_data = self:get_rank_data(latest_rank_id)
    if not latest_rank_data then
        log_err("[RankObj][whole_latest_2_stable]->get rank_id failed! latest_rank_id:%s", latest_rank_id)
        return
    end
    self.rank_list[stable_rank_id] = { rank_rows = {}, version = cur_time, rank_id = stable_rank_id }

    local latest_rank_rows = self.rank_list[latest_rank_id].rank_rows
    local stable_rank_rows = self.rank_list[stable_rank_id].rank_rows
    tcopy(latest_rank_rows, stable_rank_rows)

    if self:update_rank_data_2db(stable_rank_id) then
        local area_id = guid_index(stable_rank_id)
        router_mgr:call_platform_all("rpc_ntf_rank_update", self.rank_type, area_id, custom, self.rank_list[stable_rank_id])
    end
end

-- 分区排行最新更新到稳定
function RankObj:area_latest_2_stable(cur_time)
    for _, node in pairs(self.rank_dir.nodes) do
        local stable_rank_id = node.stable_rank_id
        local latest_rank_id = node.latest_rank_id
        if not stable_rank_id or not latest_rank_id then
            log_err("[RankObj][area_latest_2_stable]->get rank_id failed! stable_rank_id:%s, latest_rank_id:%s, nodes:%s",
                     stable_rank_id, latest_rank_id, serialize(self.rank_dir.nodes))
            return
        end

        if self.load_data_status[latest_rank_id] ~= DBLoading.SUCCESS then
            return
        end

        local latest_rank_data = self:get_rank_data(latest_rank_id)
        if not latest_rank_data then
            log_err("[RankObj][area_latest_2_stable]->get rank_id failed! latest_rank_id:%s", latest_rank_id)
            return
        end
        self.rank_list[stable_rank_id] = { rank_rows = {}, version = cur_time, rank_id = stable_rank_id }

        local latest_rank_rows = self.rank_list[node.latest_rank_id].rank_rows
        local stable_rank_rows = self.rank_list[stable_rank_id].rank_rows
        tcopy(latest_rank_rows, stable_rank_rows)

        if self:update_rank_data_2db(stable_rank_id) then
            router_mgr:call_platform_all("rpc_ntf_rank_update", self.rank_type, node.area_id, node.custom, self.rank_list[stable_rank_id])
        end

        event_mgr:notify_trigger("evt_update_stable_rank", self.rank_type, node.area_id, self.rank_list[stable_rank_id].rank_rows)
    end
end

-- 更新排行索引
function RankObj:timer_rank_dir_update()
    if self.dir_loading ~= DBLoading.SUCCESS then
        return
    end

    if self:is_whole_rank() then
        local custom = self:get_rank_dir_custom()
        local area_id = PlatCommon.GLOBAL_DEFAULT_AREA_ID
        if not self:is_rank_dir_exist(area_id, custom) then
            self:new_rank_dir_node(area_id, custom)
        end
    end
end

-- 检查排行索引是否存在
function RankObj:is_rank_dir_exist(area_id, custom)
    local stable_rank_id, latest_rank_id
    if self:is_whole_rank() then
        stable_rank_id, latest_rank_id = self:get_rank_id(PlatCommon.GLOBAL_DEFAULT_AREA_ID, custom)
    else
        stable_rank_id, latest_rank_id = self:get_rank_id(area_id, custom)
    end

    if not stable_rank_id or not latest_rank_id then
        return false
    else
        return true
    end
end

-- 插入排行数据
function RankObj:insert_record_to_latest(area_id, new_record)
    local custom = self:get_rank_dir_custom()
    local _, latest_rank_id = self:get_rank_id(area_id, custom)
    if not latest_rank_id then
        log_err("[RankObj][insert_record_to_latest] get latest_rank_id failed! area_id:%s, custom:%s", area_id, custom)
        return false
    end

    local latest_rank_data = self:get_rank_data(latest_rank_id)
    if not latest_rank_data then
        return false
    end

    log_debug("[RankObj][insert_record_to_latest]->rank_type:%s, new_record:%s", self.rank_type, serialize(new_record))

    local rank_rows = latest_rank_data.rank_rows
    local rank_size = #rank_rows
    if rank_size == 0 then
        -- 1.若榜单为空,直接插入
        new_record.rank_pos = rank_size + 1
        tinsert(rank_rows, new_record)
        self.dirty_rank_data[latest_rank_id] = true
    else
        local config = self.config
        local svr_capacity = config.svr_capacity
        local master_field = config.master_field
        local old_record = nil
        for _, row in pairs(rank_rows) do
            if row[master_field] == new_record[master_field] then
                old_record = row
                break
            end
        end

        if old_record then
            -- 2.如果已经在榜内
            local cur_pos = old_record.rank_pos
            new_record.rank_pos = cur_pos
            rank_rows[cur_pos] = new_record

            if self.rank_algorithm(self.cmp_fields, new_record, old_record) then
                -- 若高于当前数据则往前对比
                self:latest_rank_forward(latest_rank_id, cur_pos, cur_pos - 1)
            else
                -- 若低于当前数据则往后对比
                self:latest_rank_back(latest_rank_id, cur_pos, cur_pos + 1)
            end
        else
            -- 3.不在榜内
            if rank_size < svr_capacity then
                new_record.rank_pos = rank_size + 1
                tinsert(rank_rows, new_record)
                -- 从后往前逐一比较
                self:latest_rank_forward(latest_rank_id, rank_size + 1, rank_size)
            else
                if not self.rank_algorithm(self.cmp_fields, rank_rows[rank_size], new_record) then
                    -- 超过最后一名
                    new_record.rank_pos = svr_capacity
                    rank_rows[svr_capacity] = new_record
                    self:latest_rank_forward(latest_rank_id, rank_size, rank_size - 1)
                end
            end
        end
    end

    return true
end

-- 往前
function RankObj:latest_rank_forward(rank_id, cur_pos, pos_f)
    if cur_pos <= pos_f or pos_f <= 0 then
        return
    end

    local rank_data = self.rank_list[rank_id]
    if not rank_data then
        return
    end

    local rank_rows = rank_data.rank_rows
    if self.rank_algorithm(self.cmp_fields, rank_rows[pos_f], rank_rows[cur_pos]) then
        -- 低于前面的排行，不再往前
        return
    end

    -- 交换位置
    local temp_record = {}
    for field, data in pairs(rank_rows[cur_pos]) do
        temp_record[field] = data
    end

    rank_rows[cur_pos] = rank_rows[pos_f]
    rank_rows[cur_pos].rank_pos = cur_pos

    rank_rows[pos_f] = temp_record
    rank_rows[pos_f].rank_pos = pos_f

    self.dirty_rank_data[rank_id] = true

    self:latest_rank_forward(rank_id, pos_f, pos_f - 1)
end

-- 推后
function RankObj:latest_rank_back(rank_id, cur_pos, pos_b)
    local svr_capacity = self.config.svr_capacity
    if cur_pos >= pos_b or pos_b > svr_capacity then
        return
    end

    local rank_data = self.rank_list[rank_id]
    if not rank_data then
        return
    end

    local rank_rows = rank_data.rank_rows
    if not rank_rows[pos_b] then
        return
    end

    if self.rank_algorithm(self.cmp_fields, rank_rows[cur_pos], rank_rows[pos_b]) then
        -- 高于后面的排行，不再推后
        return
    end

    -- 交换位置
    local temp_record = {}
    for field, data in pairs(rank_rows[pos_b]) do
        temp_record[field] = data
    end

    rank_rows[pos_b] = rank_rows[cur_pos]
    rank_rows[pos_b].rank_pos = pos_b

    rank_rows[cur_pos] = temp_record
    rank_rows[cur_pos].rank_pos = cur_pos

    self.dirty_rank_data[rank_id] = true

    if pos_b >= svr_capacity then
        local area_id = guid_index(rank_id)
        router_mgr:call_platform_all("rpc_rank_update_last_one", self.rank_type, area_id, rank_rows[pos_b])
    else
        self:latest_rank_back(rank_id, pos_b, pos_b + 1)
    end
end

-- 获取排行榜数据
function RankObj:get_rank_data(rank_id)
    if not rank_id or rank_id <= 0 then
        return
    end

    local rank_data = self.rank_list[rank_id]
    if not rank_data and self:load_rank_rows_from_db(rank_id) then
        rank_data = self.rank_list[rank_id]
    end

    return rank_data
end

-- 存储最新排行数据
function RankObj:save_latest_rank_data()
    if self:is_whole_rank() then
        local _, latest_rank_id = self:get_rank_id(PlatCommon.GLOBAL_DEFAULT_AREA_ID, self:get_rank_dir_custom())
        if self.dirty_rank_data[latest_rank_id] and self:update_rank_data_2db(latest_rank_id) then
            self.dirty_rank_data[latest_rank_id] = false
        end
    else
        for _, node in pairs(self.rank_dir.nodes) do
            local latest_rank_id = node.latest_rank_id
            if self.dirty_rank_data[latest_rank_id] and self:update_rank_data_2db(latest_rank_id) then
                self.dirty_rank_data[latest_rank_id] = false
            end
        end
    end
end

-- 从数据库加载排行榜索引数据
function RankObj:load_rank_dir_from_db()
    if self.dir_loading == DBLoading.SUCCESS then
        timer_mgr:unregister(self.load_dir_timer)
        return
    end

    if self.dir_loading == DBLoading.LOADING then
        if otime() - self.load_begin > 30 then
            log_err("[RankObj][load_rank_dir_from_db] load long time, maybe not connect mongo!!!")
        end
        return
    end

    self.dir_loading = DBLoading.LOADING
    local rank_type = self.rank_type
    local db_name = self.config.db_name
    local ok, result = rank_dao:load_rank_dir_data(rank_type, db_name, {})
    if not ok then
        --log_err("[RankObj][load_rank_dir_from_db]->load failed! rank_type:%s, db_name:%s", rank_type, db_name)
        self.dir_loading = DBLoading.INIT
        return
    end
    self.dir_loading = DBLoading.SUCCESS

    --log_debug("[RankObj][load_rank_dir_from_db]->load success! db_name:%s, result:%s", db_name, serialize(result))

    if next(result) then
        self.rank_dir = result[1]
    else
        self.rank_dir = {nodes = {}}
    end
end

-- 从数据库加载排行数据
function RankObj:load_rank_rows_from_db(rank_id)
    log_debug("RankObj:load_rank_rows_from_db->rank_id:%s, status:%s", rank_id, self.load_data_status[rank_id])
    if self.load_data_status[rank_id] == DBLoading.SUCCESS then
        return true
    end

    if self.load_data_status[rank_id] == DBLoading.LOADING then
        return false
    end

    self.load_data_status[rank_id] = DBLoading.LOADING
    local db_name = self.config.db_name
    local result = rank_dao:load_rank_data(rank_id, db_name)
    if not result then
        log_err("[RankObj][load_rank_rows_from_db]->load failed! rank_id:%s, db_name:%s", rank_id, db_name)
        self.load_data_status[rank_id] = DBLoading.INIT
        return false
    end

    --log_debug("[RankObj][load_rank_rows_from_db] rank list data! db_name:%s, result:%s", db_name, serialize(result))

    self.load_data_status[rank_id] = DBLoading.SUCCESS

    if next(result) then
        self.rank_list[rank_id] = result
    else
        self.rank_list[rank_id] = {rank_rows = {}, version = 0, rank_id = rank_id}
    end

    log_debug("[RankObj][load_rank_rows_from_db]->rank_id:%s, db_name:%s", rank_id, db_name)
    return true
end

-- 排行榜索引数据落地
function RankObj:update_rank_dir_2db()
    if self.dir_loading ~= DBLoading.SUCCESS or not self.dirty_rank_dir then
        return
    end

    local rank_dir = self.rank_dir
    if not rank_dir.nodes then
        return
    end

    local rank_type = self.rank_type
    local db_name = self.config.db_name
    if not rank_dao:update_rank_dir_data(rank_type, db_name, rank_dir, {}) then
        log_err("[RankObj][load_rank_dir_from_db]->load failed! rank_type:%s, db_name:%s", rank_type, db_name)
        return
    end

    log_debug("[RankObj][update_rank_dir_2db]->db_name:%s", db_name)
    self.dirty_rank_dir = false
end

-- 排行榜数据落地
function RankObj:update_rank_data_2db(rank_id)
    local db_name = self.config.db_name
    local rank_data = self.rank_list[rank_id]
    if not rank_data then
        log_err("[RankObj][save_rank_data_to_db]->get rank data failed! rank_id:%s, db_name:%s", rank_id, db_name)
        return false
    end

    if not rank_dao:update_rank_data(rank_id, db_name, rank_data) then
        log_err("[RankObj][save_rank_data_to_db]->update failed! rank_id:%s, db_name:%s", rank_id, db_name)
        return false
    end

    return true
end

-- 是否全区排行
function RankObj:is_whole_rank()
    return self.config.global_rank
end

return RankObj
