--login_servlet.lua
local log_debug         = logger.debug
local log_warn          = logger.warn
local serialize         = logger.serialize

local KernCode          = enum("KernCode")
local AreaStatus        = enum("AreaStatus")
local AccountCode       = enum("AccountCode")
local SSCmdID           = enum("PlatSSCmdID")
local PeriodTime        = enum("PeriodTime")
local SUCCESS           = KernCode.SUCCESS

local data_mgr          = quanta.get("data_mgr")
local timer_mgr         = quanta.get("timer_mgr")
local event_mgr         = quanta.get("event_mgr")
local router_mgr        = quanta.get("router_mgr")
local player_mgr        = quanta.get("player_mgr")
local account_dao       = quanta.get("account_dao")
local white_list_mgr    = quanta.get("white_list_mgr")
local active_code_mgr   = quanta.get("active_code_mgr")
local account_limit_mgr = quanta.get("account_limit_mgr")

local LoginServlet = singleton()
function LoginServlet:__init()
    self.area_list = {}
    event_mgr:add_cmd_listener(self, SSCmdID.NID_PLAYER_LOGIN_REQ, "on_player_login_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_PLAYER_CREATE_REQ, "on_player_create_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_PLAYER_UPDATE_REQ, "on_player_update_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_PLAYER_LOGOUT_REQ, "on_player_logout_req")
    event_mgr:add_cmd_listener(self, SSCmdID.NID_OFFLINE_UPDATE_REQ, "on_offline_update_req")

    --定时任务
    timer_mgr:loop(PeriodTime.SECOND_30_MS, function()
        local ok, code, areas = router_mgr:call_platcenter_master("rpc_load_area_list")
        if ok and SUCCESS == code then
            self.area_list = areas
        end
    end)
end

--------------------------------------------------------------
--玩家登陆
function LoginServlet:on_player_login_req(data)
    local token, player_data, account_data = data.token, data.player, data.account_data
    local player_id = player_data.player_id
    log_debug("[LoginServlet][on_player_login_req] player_id:%s, token:%s, data:%s", player_id, token, serialize(player_data))
    player_data.online_status = player_data.status
    local player = player_mgr:player_login(player_id, player_data, token, account_data)
    if player then
        return { code = SUCCESS }
    end
    return { code = AccountCode.PLAT_PLAYER_LOAD_ERR }
end

--玩家创建
function LoginServlet:on_player_create_req(data)
    local token, player_data, account_data = data.token, data.player, data.account_data
    local player_id, name = player_data.player_id, player_data.nick
    log_debug("[LoginServlet][on_player_create_req] player_id:%s, token:%s, data:%s", player_id, token, serialize(player_data))
    player_data.online_status = player_data.status
    local player = player_mgr:player_login(player_id, player_data, token, account_data)
    if player then
        --更新名字库
        local name_info = { name = name, player_id = player_id }
        if account_dao:upset_player_name(name, name_info) then
            return { code = SUCCESS }
        end
    end
    return { code = AccountCode.PLAT_PLAYER_LOAD_ERR }
end

--玩家更新
function LoginServlet:on_player_update_req(data)
    local update_data = data.player
    local player_id = update_data.player_id
    log_debug("[LoginServlet][on_player_update_req] player_id:%s, data:%s", player_id, serialize(update_data))
    local player = player_mgr:get_player(player_id)
    if not player then
        return { code = KernCode.PLAYER_NOT_EXIST }
    end

    local old_data = {}
    local player_data = player:get_data()
    for attr in pairs(update_data) do
        old_data[attr] = player_data[attr]
    end
    player:update_data(update_data)
    event_mgr:notify_trigger("ntf_rank_player_update", player_id, player_data, old_data, update_data)
    local name = update_data.nick
    if name then
        --更新名字库
        local name_info = { name = name, player_id = player_id }
        if not account_dao:upset_player_name(name, name_info) then
            log_warn("[LoginServlet][on_player_update_req] upset_player_name failed player_id:%s, name:%s", player_id, name)
        end
    end
    return { code = SUCCESS }
end

--玩家退出
function LoginServlet:on_player_logout_req(data)
    local player_data = data.player
    local player_id = player_data.player_id
    log_debug("[LoginServlet][on_player_logout_req] player_id:%s, data:%s", player_id, serialize(player_data))
    local player = player_mgr:get_player(player_id)
    if not player then
        return { code = KernCode.PLAYER_NOT_EXIST }
    end
    player_data.online_status = player_data.status
    player:update_data(player_data)
    event_mgr:notify_trigger("on_client_logout", player, player_id)
    --删除玩家
    player_mgr:del_player(player, player_id)
    return { code = SUCCESS }
end

-- 更新离线玩家数据
function LoginServlet:on_offline_update_req(data)
    local update_data = data.player
    local player_id = update_data.player_id
    local offline_data = data_mgr:get_player_data(player_id)
    local old_data = {}
    for attr, value in pairs(update_data) do
        old_data[attr] = offline_data[attr]
        offline_data[attr] = value or 0
    end
    event_mgr:notify_trigger("ntf_rank_player_update", player_id, offline_data, old_data, update_data)
    return { code = SUCCESS }
end

-- 登录校验
function LoginServlet:login_check(open_id, player_id, area_id, active_code)
    -- 黑名单永远返回被ban
    if account_limit_mgr:player_is_lock(open_id, player_id) then
        return AccountCode.ACCOUNT_BAN_ERR
    end
    local area_info = self.area_list[area_id]
    if not area_info then
        return AccountCode.SERVER_MAINTAINING
    end
    local area_state = area_info.area_state
    if area_state == AreaStatus.MAINTAIN then       -- 维护中
        return AccountCode.SERVER_MAINTAINING
    elseif area_state == AreaStatus.DEBUG then      -- 只开放白名单注册
        if not white_list_mgr:is_in_white_list(open_id) then
            return AccountCode.SERVER_MAINTAINING
        end
    elseif area_state == AreaStatus.ACTIVE then     -- 激活码模式
        if not active_code_mgr:use_active_code(open_id, active_code) then
            return AccountCode.ACTIVE_CODE_ERR
        end
    end
    if area_info.open_time > quanta.now then        -- 还没到开服时间
        return AccountCode.AREA_OPENTIME_ERR
    end
    return KernCode.SUCCESS
end

-- export
quanta.login_servlet     = LoginServlet()

return LoginServlet
