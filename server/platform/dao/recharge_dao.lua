-- account_data.lua
local log_err       = logger.err
local check_failed  = utility.check_failed

local db_agent      = quanta.get("db_agent")

local RechargeDao = singleton()
function RechargeDao:__init()
end

function RechargeDao:load_order(selector)
    local ok, code, res = db_agent:find(1, {"recharge_order", selector or {}})
    if not ok or check_failed(code) then
        log_err("[RechargeDao][selector] failed: code: %s, res: %s", code, res)
        return false
    end

    return true, res or {}
end

function RechargeDao:update_order(order_id, order_info)
    local ok, code, res = db_agent:update(order_id, {"recharge_order", order_info, {order_id = order_id}, true})
    if not ok or check_failed(code) then
        log_err("[RechargeDao][update_order] failed: code: %s, res: %s", code, res)
        return false
    end

    return true
end



-- export
quanta.recharge_dao = RechargeDao()

return RechargeDao
