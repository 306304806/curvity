--season_servlet.lua

local log_debug         = logger.debug

local event_mgr         = quanta.get("event_mgr")
local season_cache_mgr  = quanta.get("season_cache_mgr")

local SSCmdID           = enum("PlatSSCmdID")
local KernCode          = enum("KernCode")

local SeasonServlet = singleton()
function SeasonServlet:__init()
    event_mgr:add_listener(self, "rpc_season_data_ntf")
    event_mgr:add_listener(self, "rpc_rank_season_update")
    event_mgr:add_listener(self, "rpc_battlepass_season_update")

    event_mgr:add_cmd_listener(self, SSCmdID.NID_SS_GET_SEASON_INFO_REQ, "on_ss_get_season_info_req")
end

function SeasonServlet:rpc_season_data_ntf(rank_season, battlepass_season, rank_times, battlepass_times)
    log_debug("[SeasonServlet][rpc_season_data_ntf]->rank_season:%s, battlepass_season:%s", rank_season, battlepass_season)
    local old_rank_season = season_cache_mgr:get_rank_season()
    if old_rank_season ~= rank_season then
        self:rpc_rank_season_update(old_rank_season, rank_season, rank_times.start_time, rank_times.finish_time)
    end
    local old_bp_season = season_cache_mgr:get_battlepass_season()
    if old_bp_season ~= battlepass_season then
        self:rpc_battlepass_season_update(old_bp_season, battlepass_season, battlepass_times.start_time, battlepass_times.finish_time)
    end
end

function SeasonServlet:on_ss_get_season_info_req(rpc_req)
    local rpc_res = {
        code = KernCode.SUCCESS,
        season_info = {
            rank_season             = season_cache_mgr:get_rank_season(),
            battlepass_season       = season_cache_mgr:get_battlepass_season(),
            rank_season_time        = season_cache_mgr:get_rank_time(),
            battlepass_season_time  = season_cache_mgr:get_battlepass_time(),
        }
    }
    return rpc_res
end

function SeasonServlet:rpc_rank_season_update(old_season, new_season, start_time, finish_time)
    season_cache_mgr:set_rank_season(new_season)
    season_cache_mgr:set_rank_start_time(start_time)
    season_cache_mgr:set_rank_finish_time(finish_time)
    local args = {old_season = old_season, new_season = new_season, start_time = start_time, finish_time = finish_time}
    event_mgr:notify_listener("rpc_subscribe_dispatch", "evt_rank_season_change", args)
end

function SeasonServlet:rpc_battlepass_season_update(old_season, new_season, start_time, finish_time)
    season_cache_mgr:set_battlepass_season(new_season)
    season_cache_mgr:set_battlepass_start_time(start_time)
    season_cache_mgr:set_battlepass_finish_time(finish_time)
    local args = {old_season = old_season, new_season = new_season, start_time = start_time, finish_time = finish_time}
    event_mgr:notify_listener("rpc_subscribe_dispatch", "evt_battlepass_season_change", args)
end

-- export
quanta.reason_servlet   = SeasonServlet()

return SeasonServlet
