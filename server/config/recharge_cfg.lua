--recharge_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local recharge = config_mgr:get_table("recharge")

--导出版本号
recharge:set_version(10000)

--导出配置内容
recharge:upsert({
    commodity_id = 1,
    name = '250理想币',
    icon_pay = '/Game/PaperMan/UI/Atlas/DynamicResource/Pay/T_Dynamic_Pay_1.T_Dynamic_Pay_1',
    amount = 2500,
    currency = 'CNY',
    items = {{item_id=1,item_count=250}},
})

recharge:upsert({
    commodity_id = 2,
    name = '500理想币',
    desc = '+50理想币',
    icon_pay = '/Game/PaperMan/UI/Atlas/DynamicResource/Pay/T_Dynamic_Pay_2.T_Dynamic_Pay_2',
    amount = 5000,
    currency = 'CNY',
    items = {{item_id=1,item_count=550}},
})

recharge:upsert({
    commodity_id = 3,
    name = '1000理想币',
    desc = '+120理想币',
    icon_pay = '/Game/PaperMan/UI/Atlas/DynamicResource/Pay/T_Dynamic_Pay_3.T_Dynamic_Pay_3',
    amount = 10000,
    currency = 'CNY',
    items = {{item_id=1,item_count=1120}},
})

recharge:upsert({
    commodity_id = 4,
    name = '2000理想币',
    desc = '+250理想币',
    icon_pay = '/Game/PaperMan/UI/Atlas/DynamicResource/Pay/T_Dynamic_Pay_4.T_Dynamic_Pay_4',
    amount = 20000,
    currency = 'CNY',
    items = {{item_id=1,item_count=2250}},
})

recharge:upsert({
    commodity_id = 5,
    name = '5000理想币',
    desc = '+700理想币',
    icon_pay = '/Game/PaperMan/UI/Atlas/DynamicResource/Pay/T_Dynamic_Pay_5.T_Dynamic_Pay_5',
    amount = 50000,
    currency = 'CNY',
    items = {{item_id=1,item_count=5700}},
})
