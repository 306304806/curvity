--season_cache_mgr.lua

local SeasonCacheMgr = singleton()
local prop = property(SeasonCacheMgr)
prop:accessor("rank_season", 0)
prop:accessor("battlepass_season", 0)
prop:accessor("rank_start_time", 0)
prop:accessor("rank_finish_time", 0)
prop:accessor("battlepass_start_time", 0)
prop:accessor("battlepass_finish_time", 0)

function SeasonCacheMgr:__init()
end

function SeasonCacheMgr:get_rank_time()
    return {
        start_time = self.rank_start_time,
        finish_time = self.rank_finish_time,
    }
end

function SeasonCacheMgr:get_battlepass_time()
    return {
        start_time = self.battlepass_start_time,
        finish_time = self.battlepass_finish_time,
    }
end

function SeasonCacheMgr:set_rank_time(info)
    self.rank_start_time  = info.start_time
    self.rank_finish_time = info.finish_time
end

function SeasonCacheMgr:set_battlepass_time(info)
    self.battlepass_start_time  = info.start_time
    self.battlepass_finish_time = info.finish_time
end

-- export
quanta.season_cache_mgr = SeasonCacheMgr()

return SeasonCacheMgr
