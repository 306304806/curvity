--chat_group.lua
local tinsert       = table.insert
local tdelete       = table_ext.delete

local ChatType      = enum("ChatType")

local GROUP_CHAT_EXPIRE_TIME    = 60 * 60 * 3  -- 聊天群最多存活三个小时

local chat_mgr      = quanta.get("chat_mgr")

local ChatGroup = class()
local prop = property(ChatGroup)
prop:accessor("area_id", 0)         -- 区id
prop:accessor("group_id", 0)        -- 群id
prop:accessor("group_name", "")     -- 群名称
prop:accessor("group_type", 0)      -- 群类型
prop:accessor("members", {})        -- 群成员列表(辅助性能优化)
prop:accessor("member_ids", {})     -- 群成员ID列表(辅助性能优化)
prop:accessor("team_members", {})   -- 群队伍成员ID列表(辅助性能优化)
prop:accessor("create_time", 0)
prop:accessor("chat_period", 0)
prop:accessor("chat_count", 0)
prop:accessor("reset_time", 0)-- 最近一次聊天频率状态重置时间
prop:accessor("chat_statis", {})    -- 统计玩家发了多少条消息

function ChatGroup:__init(id, type, name)
    self.group_id = id
    self.group_type = type
    self.create_time = quanta.now
    self.group_name = name or ""
end

--是否公共群
function ChatGroup:is_public()
    return self.group_type <= ChatType.SYSTEM
end

--打包数据
function ChatGroup:package()
    return {
        group_id = self.group_id,
        group_name = self.group_name,
        group_type = self.group_type,
        members = self.members
    }
end

-- 添加新成员
function ChatGroup:add_member(player_id, member)
    if not self.members[player_id] then
        self.members[player_id] = member
        tinsert(self.member_ids, player_id)
        self:enter_team(player_id, member.team_id)
        chat_mgr:register_player_group(player_id, self.group_id)
    end
end

-- 批量添加新成员
function ChatGroup:add_members(members)
    for _, member in pairs(members) do
        self:add_member(member.player_id, member)
    end
end

-- 删除成员
function ChatGroup:del_member(player_id)
    local member = self.members[player_id]
    if member then
        self.members[player_id] = nil
        tdelete(self.member_ids, player_id)
        self:exit_team(player_id, member.team_id)
        chat_mgr:unregister_player_group(player_id, self.group_id)
    end
end

-- 批量删除群成员
function ChatGroup:del_members(player_ids)
    for _, player_id in pairs(player_ids) do
        self:del_member(player_id)
    end
end

-- 清空全部成员
function ChatGroup:destory()
    for player_id, _ in pairs(self.members) do
        chat_mgr:unregister_player_group(player_id, self.group_id)
    end
    self.members = {}
    self.member_ids = {}
end

-- 查询指定玩家是否存在
function ChatGroup:has_member(player_id)
    return self.members[player_id]
end

--进入队伍
function ChatGroup:enter_team(player_id, team_id)
    if team_id > 0 then
        if not self.team_members[team_id] then
            self.team_members[team_id] = {}
        end
        tinsert(self.team_members[team_id], player_id)
    end
end

--退出队伍
function ChatGroup:exit_team(player_id, team_id)
    if team_id > 0 then
        local team_members = self.team_members[team_id]
        if team_members then
            tdelete(team_members, player_id)
        end
    end
end

-- 更换队伍
function ChatGroup:update_team(player_id, team_id)
    local member = self.members[player_id]
    if member and member.team_id ~= team_id then
        self:exit_team(player_id, member.team_id)
        self:enter_team(player_id, team_id)
        member.team_id = team_id
    end
    return member
end

-- 获取指定队伍的玩家列表
function ChatGroup:get_team_ids(team_id)
    if not team_id or team_id == 0 then
        return self.member_ids
    end
    -- 队伍广播
    return self.team_members[team_id] or {}
end

-- 是否为空
function ChatGroup:empty()
    return (#self.member_ids == 0)
end

-- 获取生存时间
function ChatGroup:is_expire()
    return (quanta.now - self.create_time > GROUP_CHAT_EXPIRE_TIME)
end

-- 设置发送频率控制
function ChatGroup:set_limit(chat_period, chat_count)
    self.chat_count = chat_count
    self.chat_period = chat_period
    self.reset_time = quanta.now + chat_period
end

-- 玩家发送次数增加
function ChatGroup:static_chat(player_id)
    if not self.chat_statis[player_id] then
        self.chat_statis[player_id] = 1
    else
        self.chat_statis[player_id] = self.chat_statis[player_id] + 1
    end
    return self.chat_statis[player_id]
end

-- 玩家聊天是否超过频率现实
function ChatGroup:check_limit(player_id)
    if self.chat_count > 0 then
        local cur_chat_num = self.chat_statis[player_id]
        if cur_chat_num and cur_chat_num > self.chat_count then
            return false
        end
    end
    return true
end

-- 检查玩家是否在指定队伍
function ChatGroup:check_team(player_id, team_id)
    if not team_id or team_id == 0 then
        return true
    end
    local member = self.members[player_id]
    if member and member.team_id == team_id then
        return true
    end
    return false
end

-- tick回调
function ChatGroup:on_tick(cur_time)
    -- 重置聊天频率控制数据
    if self.reset_time > 0 and cur_time > self.reset_time then
        self.reset_time = cur_time + self.chat_period
        self.chat_statis = {}
    end
end

return ChatGroup
