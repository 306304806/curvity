--plat_code.lua

--平台错误码
local PlatCode = enum("PlatCode", 0)
PlatCode.AREA_ID_ERROR                  = 20000     --area_id错误
PlatCode.ERR_LOGIN_TOKEN                = 20001     --登录token错误
PlatCode.ERR_PLAT_DISCONNECT            = 20002     --平台连接已断开

--好友错误码
local FriendCode = enum("FriendCode", 0)
FriendCode.ERR_FRIEND_AREADY_FRIEND     = 20101     --目标已是好友
FriendCode.ERR_FRIEND_AREADY_BLACK      = 20102     --目标已是黑名单
FriendCode.ERR_FRIEND_AREADY_APPLY      = 20103     --目标已在申请列表
FriendCode.ERR_FRIEND_NOT_IS_FRIEND     = 20104     --目标不是好友
FriendCode.ERR_FRIEND_NOT_IS_BLACK      = 20105     --目标不是黑名单
FriendCode.ERR_FRIEND_NOT_IS_APPLY      = 20106     --目标不在申请列表
FriendCode.ERR_FRIEND_RMSG_FAILED       = 20107     --数据库存储失败
FriendCode.ERR_FRIEND_GROUP_NOT_EXIST   = 20108     --好友分组不存在
FriendCode.ERR_FRIEND_NUMBER_IS_LIMIT   = 20109     --好友数量达到上限
FriendCode.ERR_FRIEND_APPLY_IS_LIMIT    = 20110     --单日申请达到上限

--邮件错误码
local MailCode = enum("MailCode", 0)
MailCode.ERR_MAIL_NOT_EXIST             = 20201     --邮件不存在
MailCode.ERR_MAIL_ATTACH_NOT_EXIST      = 20202     --附件附件不存在
MailCode.ERR_MAIL_ATTACH_IS_TAKED       = 20203     --邮件附件已领取
MailCode.ERR_MAIL_EXIST                 = 20204     --邮件已经存在(服务器出错的时候可能出现邮件重复)
MailCode.ERR_MAIL_READED                = 20205     --邮件已读
MailCode.ERR_MAIL_TAR_NOT_EXIST         = 20206     --邮件接收目标玩家不存在
MailCode.ERR_MAIL_DELETED               = 20207     --邮件已删
MailCode.ERR_MAIL_RMSG_FAILED           = 20208     --数据库存储失败

--聊天错误码
local ChatCode = enum("ChatCode", 0)
ChatCode.ERR_CHAT_GROUP_NOT_EXIST       = 20300     --群不存在
ChatCode.ERR_CHAT_GROUP_NOT_MEMBER      = 20301     --不是群成员
ChatCode.ERR_CHAT_GROUP_CREATE_FAILD    = 20302     --聊天群创建失败
ChatCode.ERR_CHAT_GROUP_HZ_LIMIT        = 20303     --频率限制
ChatCode.ERR_CHAT_GROUP_NOT_IN_TEAM     = 20304     --不再群聊频道中
ChatCode.ERR_CHAT_PLAYER_IS_BAN         = 20305     --禁言

--商城错误码
local ShopCode = enum("ShopCode", 0)
ShopCode.WAITING_PAY_ORDER              = 20501     --有待支付订单
ShopCode.ORDRE_NOT_EXIST                = 20502     --订单不存在
ShopCode.COMMODITY_NOT_EXIST            = 20503     --商品不存在
ShopCode.COMMODITY_LIMIT                = 20504     --当前购买次数已满
ShopCode.COMMODITY_OFF_SHELVES          = 20505     --商品已下架
ShopCode.COMMODITY_NOT_SHELVES          = 20506     --商品未上架
ShopCode.REFUSE_CLAIM                   = 20507     --不接受索要
ShopCode.CLAIM_LV_NOT_ENOUGH            = 20508     --索要等级不满足
ShopCode.CLAIM_CNT_MAX                  = 20509     --索要次数达到上限
ShopCode.GIVE_LV_NOT_ENOUGH             = 20510     --赠送等级不满足
ShopCode.GIVE_TIME_NOT_ENOUGH           = 20511     --赠送时间不满足
ShopCode.COMMODITY_DAY_LIMIT            = 20512     --每日购买次数已满
ShopCode.COMMODITY_WEEK_LIMIT           = 20513     --每周购买次数已满
ShopCode.COMMODITY_MONTH_LIMIT          = 20514     --每月购买次数已满
ShopCode.COMMODITY_FOREVER_LIMIT        = 20515     --永久购买次数已满
ShopCode.COMMODITY_BATTLEPASS_LIMIT     = 20516     --赛季购买次数已满

-- 安全
local SafeCode = enum("SafeCode", 0)
SafeCode.HAS_FILTERWORD                 = 20600  -- 包含敏感词
SafeCode.NICK_USED                      = 20601  -- 昵称已被使用(可能是临时锁定，但是对玩家只需要理解昵称不可用)
SafeCode.NICK_HAS_SPACE                 = 20602  -- 昵称包含空格
SafeCode.NICK_HAS_TAB                   = 20603  -- 昵称包含tab
SafeCode.NICK_LEN_ERROR                 = 20604  -- 昵称长度错误

-- 充值
local RechargeCode = enum("RechargeCode", 0)
RechargeCode.COMMODITY_COUNT_ERROR      = 20701  -- 商品数量错误
RechargeCode.PAY_PLATFORM_ERR           = 20702  -- 支付平台返回错误
RechargeCode.COMMODITY_NOT_EXIST        = 20703  -- 商品不存在
RechargeCode.ORDER_ALREADY_SEND         = 20704  -- 已完成发货(不能重复发货)
RechargeCode.ORDER_ID_EXIST             = 20705  -- 订单号已存在
RechargeCode.ORDER_ID_NOT_EXIST         = 20706  -- 订单号不存在
RechargeCode.ORDER_NOT_PAY              = 20707  -- 订单未支付
RechargeCode.COMMODITY_PRIZE_ERROR      = 20708  -- 商品价格错误
RechargeCode.RECHARGE_FAILED_UNAUTH     = 20709  -- 未实名认证无法充值
RechargeCode.RECHARGE_8_MAX_AMOUNT      = 20710  -- 8周岁单次充值提示
RechargeCode.RECHARGE_16_MAX_AMOUNT     = 20711  -- 16周岁单次充值提示
RechargeCode.RECHARGE_18_MAX_AMOUNT     = 20712  -- 18周岁单次充值提示
RechargeCode.RECHARGE_16_TOTAL_AMOUNT   = 20713  -- 16周岁累计充值提示
RechargeCode.RECHARGE_18_TOTAL_AMOUNT   = 20714  -- 18周岁累计充值提示

-- 账号
local AccountCode = enum("AccountCode", 0)
AccountCode.UNKNOWN_OTHER_PLATFORM      = 20800  -- 无法识别的第三方平台
AccountCode.OTHER_PLATFORM_LOGIN_ERR    = 20801  -- 账号第三方平台登录失败
AccountCode.ACCOUNT_BAN_ERR             = 20802  -- 账号被禁用
AccountCode.SERVER_MAINTAINING          = 20803  -- 服务器维护中
AccountCode.REGISTER_DISABLE            = 20804  -- 关闭注册
AccountCode.TEEN_FORBID_LOGIN           = 20805  -- 22:00-8:00未成年禁止登陆
AccountCode.TODAY_GAME_TIME_OVER        = 20806  -- 今日累计时间已满
AccountCode.PLAT_PLAYER_LOAD_ERR        = 20807  -- 平台角色加载失败
AccountCode.PLAT_OPEN_ID_ERR            = 20808  -- 平台openid错误
AccountCode.ACTIVE_CODE_ERR             = 20809  -- 登录激活码错误
AccountCode.AREA_OPENTIME_ERR           = 20810  -- 还没到开服时间

-- 实名认证
local AuthCode = enum("AuthCode", 0)
AuthCode.AUTH_FAILED                  = 20901  -- 认证失败