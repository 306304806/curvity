--player_dao.lua

local log_err       = logger.err
local guid_group    = guid.group
local tinsert       = table.insert
local check_failed  = utility.check_failed

local cache_agent   = quanta.get("cache_agent")
local db_agent      = quanta.get("db_agent")

local PlayerDao = singleton()
function PlayerDao:__init()
end

-- 更新shop
function PlayerDao:update_player_shop(player_id, shop)
    local row_datas = { player_id = player_id, shop = shop }
    return self:update_player(player_id, "plat_player_shop", row_datas)
end

-- 更新mail
function PlayerDao:update_player_mail(player_id, mail)
    local row_datas = { player_id = player_id, mail = mail }
    return self:update_player(player_id, "plat_player_mail", row_datas)
end

-- 更新reddot
function PlayerDao:update_player_reddot(player_id, reddot)
    local row_datas = { player_id = player_id, reddot = reddot }
    return self:update_player(player_id, "plat_player_reddot", row_datas)
end

-- 更新auth
function PlayerDao:update_player_auth(player_id, auth)
    local row_datas = { player_id = player_id, auth = auth }
    return self:update_player(player_id, "plat_player_auth", row_datas)
end

-- 更新friend
function PlayerDao:update_player_friend(player_id, friend)
    local row_datas = { player_id = player_id, friend = friend }
    return self:update_player(player_id, "plat_player_friend", row_datas)
end

-- 更新player镜像数据
function PlayerDao:update_player_image(player_id, player_data)
    local area_id = guid_group(player_id)
    local row_datas = { player_id = player_id, player = player_data }
    local code = cache_agent:update(player_id, "plat_player", row_datas, "image", false, area_id)
    if check_failed(code) then
        log_err("[PlayerDao][update_player_image] table=plat_player,code=%s", code)
        return false
    end
    return true
end

function PlayerDao:load_player_image(player_id)
    local area_id = guid_group(player_id)
    local code, player_image = cache_agent:load(player_id, "image", area_id)
    if check_failed(code) then
        log_err("[PlayerDao][load_player_image] load image failed: %s", code)
        return false
    end
    return true, player_image
end

function PlayerDao:load_player(player_id)
    local ok, player_image = self:load_player_image(player_id)
    if not ok then
        return false
    end
    local area_id = guid_group(player_id)
    local code2, player_data = cache_agent:load(player_id, "player", area_id)
    if check_failed(code2) then
        log_err("[PlayerDao][load_player] load player failed: %s", code2)
        return false
    end
    return true, player_data, player_image
end

function PlayerDao:update_player(player_id, table_name, table_data)
    local area_id = guid_group(player_id)
    local code = cache_agent:update(player_id, table_name, table_data, "player", false, area_id)
    if check_failed(code) then
        log_err("[PlayerDao][update_player] table=%s,code=%s", table_name, code)
        return false
    end
    return true
end

function PlayerDao:flush_player(player_id)
    local area_id = guid_group(player_id)
    local code2 = cache_agent:flush(player_id, "player", area_id)
    if check_failed(code2) then
        log_err("[PlayerDao][flush_player] flush player failed: %s", code2)
        return false
    end
    return true
end

function PlayerDao:search_friends(player_id, nick_part, limit)
    local friends = {}
    local query = {"plat_player", { ["player.nick"] = { ["$regex"] = nick_part, ["$options"]="i" }}, {_id = 0, player = 1}, limit}
    local ok, code, res = db_agent:collect(player_id, query)
    if not ok or check_failed(code) then
        log_err("[FriendDao][search_friend_data] failed: code: %s, res: %s", code, res)
        return friends
    end
    for _, data in pairs(res) do
        if data.player_id ~= player_id then
            tinsert(friends, data.player)
        end
    end
    return friends
end

quanta.player_dao = PlayerDao()

return PlayerDao
