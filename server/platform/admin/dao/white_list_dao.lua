--white_list_dao.lua

local log_err       = logger.err
local serialize     = logger.serialize
local check_failed  = utility.check_failed

local db_agent      = quanta.get("db_agent")

local WhiteListDao = singleton()
function WhiteListDao:__init()
end

function WhiteListDao:load_white_list()
    local ok, code, res = db_agent:find(quanta.id, { "plat_white_list", {}, {_id = 0}})
    if not ok or check_failed(code) then
        log_err("[WhiteListDao][load_white_list] failed: code: %s", code, serialize(res))
        return false
    end

    return true, res or {}
end

function WhiteListDao:update_white_list(open_id, data)
    local ok, code, res = db_agent:update(quanta.id, { "plat_white_list", data, {open_id = open_id}, true})
    if not ok or check_failed(code) then
        log_err("[WhiteListDao][update_white_list] failed! code: %s, res: %s", code, serialize(res))
        return false
    end

    return true
end

function WhiteListDao:delete_white_list(open_id)
    local ok, code, res = db_agent:delete(quanta.id, { "plat_white_list", {open_id = open_id}, true})
    if not ok or check_failed(code) then
        log_err("[WhiteListDao][delete_white_list] failed! code: %s, res: %s", code, serialize(res))
        return false
    end

    return true
end

quanta.white_list_dao = WhiteListDao()

return WhiteListDao
