--active_code_mgr.lua
local otime                 = os.time
local tinsert               = table.insert
local log_err               = logger.err
local log_debug             = logger.debug
local serialize             = logger.serialize

local active_code_dao       = quanta.get("active_code_dao")
local timer_mgr             = quanta.get("timer_mgr")
local router_mgr            = quanta.get("router_mgr")

local DBLoading             = enum("DBLoading")
local PeriodTime            = enum("PeriodTime")

local ActiveCodeMgr = singleton()
local prop = property(ActiveCodeMgr)
prop:accessor("data_loading", DBLoading.INIT)
prop:accessor("indexs", {})
prop:accessor("dirty_key_map", {})
prop:reader("dbsvr_online", false)

function ActiveCodeMgr:__init()
    self:setup()

    router_mgr:watch_service_ready(self, "dbsvr")
    router_mgr:watch_service_close(self, "dbsvr")
end

-- 初始化函数
function ActiveCodeMgr:setup()
    -- 延迟从数据库加载排行数据
    self.load_timer = timer_mgr:loop(PeriodTime.SECOND_5_MS, function()
        self:load_active_code_data()
    end)

    -- 定时检查存储排行索引数据
    timer_mgr:loop(PeriodTime.SECOND_MS, function()
        self:update_data_2db()
    end)
end

function ActiveCodeMgr:on_service_ready(quanta_id, service_name)
    self.dbsvr_online = true
end

function ActiveCodeMgr:on_service_close(quanta_id, service_name)
    self.dbsvr_online = false
end

-- 从数据库加载数据
function ActiveCodeMgr:load_active_code_data()
    log_debug("[ActiveCodeMgr][load_active_code_data]")
    if not self.dbsvr_online or self.data_loading ~= DBLoading.INIT then
        return
    end
    self.data_loading = DBLoading.LOADING
    local ok, result = active_code_dao:load_active_code()
    if not ok then
        self.data_loading = DBLoading.INIT
        log_err("[ActiveCodeMgr][load_active_code_data]->load failed!")
        return
    end

    self.data_loading = DBLoading.SUCCESS
    for _, item in pairs(result) do
        self.indexs[item.active_code] = item
    end

    timer_mgr:unregister(self.load_timer)

    return true
end

-- 数据落地
function ActiveCodeMgr:update_data_2db()
    if self.data_loading ~= DBLoading.SUCCESS then
        return
    end
    for active_code, _ in pairs(self.dirty_key_map) do
        local data = self.indexs[active_code]
        if not data then
            local ok = active_code_dao:delete_active_code(active_code)
            if ok then
                self.dirty_key_map[active_code] = nil
            end
        else
            local ok = active_code_dao:update_active_code(active_code, data)
            if ok then
                self.dirty_key_map[active_code] = nil
            end
        end
    end
end

-- 导入激活码
function ActiveCodeMgr:import_active_code(active_codes)
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[ActiveCodeMgr][import_active_code] active_codes = %s", serialize(active_codes))
        return false
    end

    local fail_codes = {}
    for _, code in pairs(active_codes) do
        if not self.indexs[code] then
            self.indexs[code] = {active_code = code, state = 0, time = otime()}
            self.dirty_key_map[code] = true
        else
            tinsert(fail_codes, code)
        end
    end
    return true, fail_codes
end

-- 查询所有激活码
function ActiveCodeMgr:search_active_code(state)
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[ActiveCodeMgr][search_active_code] state = %s", state)
        return false
    end
    local ret = {}
    for _, item in pairs(self.indexs) do
        if item.state == state then
            tinsert(ret, item)
        end
    end
    return true, ret
end

-- 删除激活码
function ActiveCodeMgr:delete_active_code(active_codes)
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[ActiveCodeMgr][delete_active_code] active_codes = %s", serialize(active_codes))
        return false
    end
    local fail_codes = {}
    for _, code in pairs(active_codes) do
        if self.indexs[code] then
            self.indexs[code] = nil
            self.dirty_key_map[code] = true
        else
            tinsert(fail_codes, code)
        end
    end
    return true, fail_codes
end

-- 使用激活码
function ActiveCodeMgr:use_active_code(player_id, active_code)
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[ActiveCodeMgr][use_active_code] player_id = %s, active_code = %s", player_id, active_code)
        return false
    end
    local code_data = self.indexs[active_code]
    if not code_data then
        return false
    end
    if code_data.player_id == player_id then
        return true
    end
    code_data.time = otime()
    code_data.player_id = player_id
    self.dirty_key_map[active_code] = true
    return true
end

-- export
quanta.active_code_mgr = ActiveCodeMgr()

return ActiveCodeMgr