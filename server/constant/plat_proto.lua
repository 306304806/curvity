--plat_proto.lua

local PlatSSCmdID = enum("PlatSSCmdID", 0)


-- 登录
PlatSSCmdID.NID_SERVER_HEART_REQ                    = 1001        -- 服务器心跳
PlatSSCmdID.NID_SERVER_HEART_RES                    = 1002
PlatSSCmdID.NID_SERVER_REGISTER_REQ                 = 12001        -- 服务器注册
PlatSSCmdID.NID_SERVER_REGISTER_RES                 = 12002
PlatSSCmdID.NID_SERVER_SUBSCRIBE_REQ                = 12003        -- 服务器订阅
PlatSSCmdID.NID_SERVER_SUBSCRIBE_RES                = 12004
PlatSSCmdID.NID_SERVER_SUBSCRIBE_NTF                = 12005


PlatSSCmdID.NID_PLAYER_LOGIN_REQ                    = 12051        -- player登录
PlatSSCmdID.NID_PLAYER_LOGIN_RES                    = 12052
PlatSSCmdID.NID_PLAYER_UPDATE_REQ                   = 12053        -- player登录
PlatSSCmdID.NID_PLAYER_UPDATE_RES                   = 12054
PlatSSCmdID.NID_PLAYER_LOGOUT_REQ                   = 12055        -- player登出
PlatSSCmdID.NID_PLAYER_LOGOUT_RES                   = 12056
PlatSSCmdID.NID_PLAYER_CREATE_REQ                   = 12057        -- player创建
PlatSSCmdID.NID_PLAYER_CREATE_RES                   = 12058
PlatSSCmdID.NID_OFFLINE_UPDATE_REQ                  = 12059
PlatSSCmdID.NID_OFFLINE_UPDATE_RES                  = 12060

-- 好友
PlatSSCmdID.NID_SS_FRIEND_BUILD_RECENT_REQ          = 12101        -- 同一场战斗
PlatSSCmdID.NID_SS_FRIEND_BUILD_RECENT_RES          = 12102
PlatSSCmdID.NID_SS_FRIEND_ADD_INTIMACY_REQ          = 12103        -- 添加友好度
PlatSSCmdID.NID_SS_FRIEND_ADD_INTIMACY_RES          = 12104
PlatSSCmdID.NID_SS_FRIEND_SECRET_REQ                = 12105        -- 查询团队隐私
PlatSSCmdID.NID_SS_FRIEND_SECRET_RES                = 12106        -- 查询团队隐私
PlatSSCmdID.NID_SS_FRIENDSHIP_QUERY_REQ             = 12107        -- 查询好友关系

-- 红点
PlatSSCmdID.NID_SS_REDDOT_ADD_REQ                   = 12151        -- 添加红点
PlatSSCmdID.NID_SS_REDDOT_ADD_RES                   = 12152

-- 邮件
PlatSSCmdID.NID_SS_MAIL_SEND_REQ                    = 12301 -- 发送邮件
PlatSSCmdID.NID_SS_MAIL_SEND_RES                    = 12302
PlatSSCmdID.NID_SS_MAIL_ATTACH_GET_REQ              = 12303 -- 邮件附件领取
PlatSSCmdID.NID_SS_MAIL_ATTACH_GET_RES              = 12304
PlatSSCmdID.NID_SS_MAIL_ATTACH_DEL_REQ              = 12305 -- 邮件附件删除
PlatSSCmdID.NID_SS_MAIL_ATTACH_DEL_RES              = 12306

-- 聊天
PlatSSCmdID.NID_SS_CHAT_GROUP_CREATE_REQ            = 12400  -- 创建聊天群
PlatSSCmdID.NID_SS_CHAT_GROUP_CREATE_RES            = 12401
PlatSSCmdID.NID_SS_CHAT_GROUP_DESTORY_REQ           = 12402  -- 销毁聊天群
PlatSSCmdID.NID_SS_CHAT_GROUP_DESTORY_RES           = 12403
PlatSSCmdID.NID_SS_CHAT_GROUP_MEMBER_JOIN_REQ       = 12404  -- 添加群成员
PlatSSCmdID.NID_SS_CHAT_GROUP_MEMBER_JOIN_RES       = 12405
PlatSSCmdID.NID_SS_CHAT_GROUP_MEMBER_QUIT_REQ       = 12406  -- 退出群
PlatSSCmdID.NID_SS_CHAT_GROUP_MEMBER_QUIT_RES       = 12407
PlatSSCmdID.NID_SS_CHAT_GROUP_MEMBER_UPDATE_REQ     = 12408  -- 群玩家信息修改
PlatSSCmdID.NID_SS_CHAT_GROUP_MEMBER_UPDATE_RES     = 12409  -- 群玩家信息修改

-- 安全服务
PlatSSCmdID.NID_SS_SAFE_FILTERWORD_CHECK_REQ        = 12600    -- 敏感词检测
PlatSSCmdID.NID_SS_SAFE_FILTERWORD_CHECK_RES        = 12601
PlatSSCmdID.NID_SS_SAFE_FILTERWORD_TRANS_REQ        = 12602    -- 敏感词转换
PlatSSCmdID.NID_SS_SAFE_FILTERWORD_TRANS_RES        = 12603
PlatSSCmdID.NID_SS_SAFE_PLAYER_NAME_USED_REQ        = 12604    -- 检查昵称是否可用
PlatSSCmdID.NID_SS_SAFE_PLAYER_NAME_USED_RES        = 12605

-- 商城
PlatSSCmdID.NID_SS_ORDER_PAY_INFO_REQ               = 12700    -- 获取订单支付信息请求
PlatSSCmdID.NID_SS_ORDER_PAY_INFO_RES               = 12701    -- 获取订单支付信息请求回复
PlatSSCmdID.NID_SS_ORDER_PAY_FINISH_REQ             = 12702    -- 支付完成请求
PlatSSCmdID.NID_SS_ORDER_PAY_FINISH_RES             = 12703    -- 支付完成请求回复

-- 充值
PlatSSCmdID.NID_SS_RECHARGE_ORDER_SEND_REQ          = 12804  -- 充值订单发货
PlatSSCmdID.NID_SS_RECHARGE_ORDER_SEND_RES          = 12805
PlatSSCmdID.NID_SS_RECHARGE_ORDER_QUERY_REQ         = 12820  -- 充值订单查询
PlatSSCmdID.NID_SS_RECHARGE_ORDER_QUERY_RES         = 12822
PlatSSCmdID.NID_SS_RECHARGE_WS_ORDER_IDS_QUERY_REQ  = 12823  -- 查询未发货订单列表
PlatSSCmdID.NID_SS_RECHARGE_WS_ORDER_IDS_QUERY_RES  = 12824

-- 账号
PlatSSCmdID.NID_SS_LOGIN_ACCOUNT_REQ                = 12900  -- 获取账号信息
PlatSSCmdID.NID_SS_LOGIN_ACCOUNT_RES                = 12901
-- 赛季
PlatSSCmdID.NID_SS_GET_SEASON_INFO_REQ              = 13000
PlatSSCmdID.NID_SS_GET_SEASON_INFO_RES              = 13001