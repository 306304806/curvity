#!./quanta
import("kernel.lua")
local log_info      = logger.info
local qxpcall       = quanta.xpcall
local quanta_update = quanta.update
local qxpcall_quit  = quanta.xpcall_quit

quanta.run = function()
    qxpcall(quanta_update, "quanta_update error: %s")
end

if not quanta.init_flag then
    local function startup()
        --初始化quanta
        quanta.init()
        --初始化gm
        quanta.init_gm("platcenter")
        --初始化platform
        local RmsgMgr = import("kernel/store/rmsg_mgr.lua")
        quanta.rmsg_friend = RmsgMgr("plat_friend")
        quanta.rmsg_mail = RmsgMgr("plat_mail")
        quanta.rmsg_chat = RmsgMgr("plat_chat")

        import("platform/init.lua")
        log_info("platform %d now startup!", quanta.id)
    end
    qxpcall_quit(startup, "quanta startup error: %s")
    quanta.init_flag = true
end
