--address_mgr.lua
local log_err       = logger.err
local log_info      = logger.info
local serialize     = logger.serialize
local tinsert       = table.insert
local tmapv2array   = table_ext.mapv2array
local tdeep_copy    = table_ext.deep_copy

local web_mgr       = quanta.get("web_mgr")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local thread_mgr    = quanta.get("thread_mgr")
local cooperate_dao = quanta.get("cooperate_dao")

local KernCode      = enum("KernCode")
local DBLoading     = enum("DBLoading")
local PeriodTime    = enum("PeriodTime")

local SUCCESS       = KernCode.SUCCESS

local AddressMgr = singleton()
local prop = property(AddressMgr)
prop:accessor("data_loading", DBLoading.INIT)
prop:accessor("gate_servers", {})
prop:accessor("gate_clients", {})
prop:accessor("areas", {})

function AddressMgr:__init()
    --web请求
    web_mgr:register_post("/platform_addr", "on_platform_addr", self)
    --gm指令
    event_mgr:add_listener(self, "gm_update_area_list")
    event_mgr:add_listener(self, "gm_search_area_list")
    event_mgr:add_listener(self, "gm_delete_area_list")
    --rpc
    event_mgr:add_listener(self, "rpc_load_area_list")
    event_mgr:add_listener(self, "rpc_gateway_report")
    --服务发现
    router_mgr:watch_service_close(self, "gateway")

    -- 从数据库加载数据
    self.data_loading = DBLoading.LOADING
    thread_mgr:success_call(PeriodTime.SECOND_MS, function()
        local ok, result = cooperate_dao:load_area_list()
        if ok then
            for _, item in pairs(result) do
                self.areas[item.area_id] = item
            end
            self.data_loading = DBLoading.SUCCESS
            log_info("[AddressMgr][success_call] load area list success!")
            return true
        end
        log_err("[AddressMgr][success_call] load area list failed!")
        return false
    end)
end

-- 获取服务器列表/gateway列表
function AddressMgr:on_platform_addr()
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[AddressMgr][search_area_list]")
        return { code = 1 }
    end
    local datas = {
        areas = tmapv2array(self.areas),
        gate_clients = tmapv2array(self.gate_clients),
        gate_servers = tmapv2array(self.gate_servers),
    }
    return { code = 0, data = datas}
end

--load_area_list
function AddressMgr:rpc_load_area_list()
    return SUCCESS, self.areas
end

--gateway上报
function AddressMgr:rpc_gateway_report(quanta_id, caddr, saddr)
    log_info("[AddressMgr][rpc_gateway_report] id: %s, client:%s, server:%s", quanta_id, caddr, saddr)
    self.gate_clients[quanta_id] = caddr
    self.gate_servers[quanta_id] = saddr
    return SUCCESS
end

--节点断开
function AddressMgr:on_service_close(quanta_id)
    self.gate_clients[quanta_id] = nil
    self.gate_servers[quanta_id] = nil
end

-- 更新小区列表
function AddressMgr:update_area_list(area_list)
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[AddressMgr][update_area_list] area_list = %s", serialize(area_list))
        return false
    end
    local fail_list = {}
    for _, area_data in pairs(area_list) do
        local old_data = self.areas[area_data.area_id]
        if not old_data then
            if area_data.area_id and area_data.area_name and area_data.area_state then
                old_data = {}
            else
                tinsert(fail_list, area_data)
            end
        end
        if old_data then
            local new_data = tdeep_copy(old_data)
            tdeep_copy(area_data, new_data)
            local ok = cooperate_dao:update_area_info(new_data.area_id, new_data)
            if ok then
                self.areas[area_data.area_id] = new_data
            else
                tinsert(fail_list, area_data)
            end
        end
    end
    return true, fail_list
end

-- 搜索小区列表
function AddressMgr:search_area_list()
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[AddressMgr][search_area_list]")
        return false
    end
    local ret = tmapv2array(self.areas)
    return true, ret
end

-- 删除小区列表
function AddressMgr:delete_area_list(area_list)
    if self.data_loading ~= DBLoading.SUCCESS then
        log_err("[AddressMgr][delete_area_list] area_list = %s", serialize(area_list))
        return false
    end
    local fail_list = {}
    for _, area_id in pairs(area_list) do
        if self.areas[area_id] then
            local ok = cooperate_dao:delete_area(area_id)
            if ok then
                self.areas[area_id] = nil
            else
                tinsert(fail_list, area_id)
            end
        else
            tinsert(fail_list, area_id)
        end
    end
    return true, fail_list
end

function AddressMgr:gm_update_area_list(area_list)
    local ok, data = self:update_area_list(area_list)
    if not ok then
        return { code = 1, msg = "update_area_list fail"}
    end
    return { code = 0, data = data}
end

function AddressMgr:gm_search_area_list()
    local ok, data = self:search_area_list()
    if not ok then
        return { code = 1, msg = "search_area_list fail"}
    end
    return { code = 0, data = data}
end

function AddressMgr:gm_delete_area_list(area_list)
    local ok, data = self:delete_area_list(area_list)
    if not ok then
        return { code = 1, msg = "delete_area_list fail"}
    end
    return { code = 0, data = data}
end

-- export
quanta.address_mgr = AddressMgr()

return AddressMgr