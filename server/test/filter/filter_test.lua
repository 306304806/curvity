--filter_test.lua
local tconcat       = table.concat
local tinsert       = table.insert
local tremove       = table.remove
local log_err       = logger.err

local timer_mgr     = quanta.get("timer_mgr")
local fword_mgr     = quanta.get("fword_mgr")
local config_mgr    = quanta.get("config_mgr")
local name_db       = config_mgr:init_table("name", "id")

local name_frist    = name_db:select({type = 1})
local name_second   = name_db:select({type = 2})
local name_three    = name_db:select({type = 3})

local function dfs(node, idx, prefix, vis)
    local pre_str = tconcat(prefix)
    local next_node = node[idx + 1]
    for _, item in pairs(node[idx]) do
        local str = pre_str .. item.name
        if vis[item.name] or vis[str] then
            goto continue
        end
        if fword_mgr:check(item.name) then
            vis[item.name] = true
            goto continue
        end
        tinsert(prefix, item.name)
        if idx > 1 and fword_mgr:check(str) then
            local sub_str = item.name
            local sub_flag = false
            local sub_path = {item.name}
            -- 查倒序子串
            for i = idx - 1, 2, -1 do
                sub_str = prefix[i] .. sub_str
                if vis[sub_str] then
                    sub_flag = true
                    break
                end
                tinsert(sub_path, 1, prefix[i])
                if fword_mgr:check(sub_str) then
                    vis[sub_str] = true
                    sub_flag = true
                    log_err("string:%s, from:%s", sub_str, tconcat(sub_path, '+'))
                    break
                end
            end
            if not sub_flag then
                vis[str] = true
                log_err("string:%s, from:%s", str, tconcat(prefix, '+'))
            end

            tremove(prefix)
            goto continue
        end
        if next_node then
            dfs(node, idx + 1, prefix, vis)
        end
        tremove(prefix)

        ::continue::
    end
end

local function filter_name()
    log_err("begin filter_name")
    local node = {name_frist, name_second, name_three}
    local vis = {}
    dfs(node, 1, {}, vis)
    for _, item in name_db:iterator() do
        if vis[item.name] then
            log_err(item.name)
        end
    end
end

-- 需要先将calabi-yau/service/config/name_cfg.lua放到curvity/service/config下
timer_mgr:once(10, function() filter_name() end)