--gate_client.lua
import("constant/plat_code.lua")
import("constant/plat_const.lua")
import("constant/plat_proto.lua")

local pairs         = pairs
local log_err       = logger.err
local log_info      = logger.info
local log_warn      = logger.warn
local log_debug     = logger.debug
local tunpack       = table.unpack
local sformat       = string.format
local env_get       = environ.get
local serialize     = logger.serialize
local tdeep_copy    = table_ext.deep_copy
local check_success = utility.check_success

local PlatCode      = enum("PlatCode")
local KernCode      = enum("KernCode")
local PeriodTime    = enum("PeriodTime")
local SSCmdID       = enum("PlatSSCmdID")

local CSCmdID       = ncmd_plat.CCmdId
local SUCCESS       = KernCode.SUCCESS

local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")
local thread_mgr    = quanta.get("thread_mgr")
local client_mgr    = quanta.get("client_mgr")
local server_mgr    = quanta.get("server_mgr")

local Gateway = singleton()
local prop = property(Gateway)
--平台服务器
prop:accessor("platforms", {})
prop:accessor("index_platforms", {})
--client
prop:accessor("clients", {})
prop:accessor("index_clients", {})
--servers
prop:accessor("servers", {})

function Gateway:__init()
    --初始化
    self:setup()
end

--初始化
function Gateway:setup()
    --监听网络消息
    event_mgr:add_listener(self, "on_session_accept")
    event_mgr:add_listener(self, "on_session_cmd")
    event_mgr:add_listener(self, "on_session_sync")
    event_mgr:add_listener(self, "on_session_err")
    --监听节点消息
    event_mgr:add_listener(self, "rpc_platform_query")
    event_mgr:add_listener(self, "rpc_platform_subscribe")
    event_mgr:add_listener(self, "rpc_platform_register")
    --客户端消息转发
    event_mgr:add_listener(self, "rpc_board_message")
    event_mgr:add_listener(self, "rpc_group_message")
    event_mgr:add_listener(self, "rpc_trans_message")
    event_mgr:add_listener(self, "rpc_client_kickout")
    --监控平台掉线
    router_mgr:watch_service_close(self, "platform")
    router_mgr:watch_service_ready(self, "platcenter")
end

--客户端注册
function Gateway:on_client_login_req(session, body, session_id)
    local area_id, player_id = body.area_id, body.player_id
    local platform = self:find_platform(area_id)
    if platform <= 0 then
        log_warn("[Gateway][on_client_login_req] failed no platform : area_id:%s, player_id:%s!", area_id, player_id)
        client_mgr:callback_dx(session, CSCmdID.NID_CLIENT_LOGIN_RES, {code = PlatCode.AREA_ID_ERROR}, session_id)
        return
    end

    local ok, code, plat_data = router_mgr:call_target(platform, "rpc_client_login", quanta.id, player_id, body)
    if ok and check_success(code) then
        session.area_id  = area_id
        session.player_id = player_id
        --踢掉老的连接
        self:rpc_client_kickout(player_id)
        --建立新的索引
        self.clients[player_id] = session
        if not self.index_clients[area_id] then
            self.index_clients[area_id] = {}
        end
        self.index_clients[area_id][player_id] = session
        log_info("[Gateway][on_client_login_req] %s login success!", player_id)
    else
        log_err("[Gateway][on_client_login_req] %s login failed!", player_id)
    end
    local data = {code = ok and code or KernCode.RPC_FAILED, player = plat_data}
    client_mgr:callback_dx(session, CSCmdID.NID_CLIENT_LOGIN_RES, data, session_id)
end

function Gateway:on_session_accept(session)
    log_info("[Gateway][on_session_accept] token %s", session.token)
end

--服务器注册
function Gateway:on_server_register_req(session, body, session_id)
    local node_id = body.node_id
    log_info("[Gateway][on_server_register_req] server %s register", node_id)
    session.node_id  = node_id
    self.servers[node_id] = session
    server_mgr:callback_dx(session, SSCmdID.NID_SERVER_REGISTER_RES, { code = SUCCESS }, session_id)
end

--服务器订阅
function Gateway:on_server_subscribe_req(session, body, session_id)
    local area_id, message, node_id = body.area_id, body.message, session.node_id
    if area_id > 0 then
        --向指定节点订阅
        local platform = self:find_session_platform(session, area_id)
        if platform > 0 then
            local ok, code = router_mgr:call_target(platform, "rpc_subscribe_register", quanta.id, message, node_id)
            if ok and check_success(code) then
                server_mgr:callback_dx(session, SSCmdID.NID_SERVER_SUBSCRIBE_RES, { code = SUCCESS }, session_id)
                log_info("[Gateway][on_server_subscribe_req] subscribe area_id:%s, message:%s, node_id:%s", area_id, message, node_id)
                return
            end
        end
    else
        --向所有节点订阅
        router_mgr:call_platform_all("rpc_subscribe_register", quanta.id, message, node_id)
        server_mgr:callback_dx(session, SSCmdID.NID_SERVER_SUBSCRIBE_RES, { code = SUCCESS }, session_id)
        log_info("[Gateway][on_server_subscribe_req] subscribe area_id:%s, message:%s, node_id:%s", area_id, message, node_id)
        return
    end
    log_err("[Gateway][on_server_subscribe_req] subscribe failed area_id:%s, message:%s", area_id, message)
    server_mgr:callback_dx(session, SSCmdID.NID_SERVER_SUBSCRIBE_RES, { code = KernCode.RPC_FAILED }, session_id)
end

--心跳处理
function Gateway:on_gateway_heart_req(session, body, session_id)
    local cserial = body.serial
    local player_id = session.player_id
    if player_id then
        local sserial = client_mgr:check_serial(session, cserial)
        local data_res = { serial = sserial, time = quanta.now }
        client_mgr:callback_dx(session, SSCmdID.NID_SERVER_HEART_RES, data_res, session_id)
    else
        local sserial = server_mgr:check_serial(session, cserial)
        local data_res = { serial = sserial, time = quanta.now }
        server_mgr:callback_dx(session, SSCmdID.NID_SERVER_HEART_RES, data_res, session_id)
    end
end

--路由表
local cmd_route_table = {
    [CSCmdID.NID_CLIENT_LOGIN_REQ]      = Gateway.on_client_login_req,
    [SSCmdID.NID_SERVER_HEART_REQ]      = Gateway.on_gateway_heart_req,
    [CSCmdID.NID_CHEART_BEAT_REQ]       = Gateway.on_gateway_heart_req,
    [SSCmdID.NID_SERVER_REGISTER_REQ]   = Gateway.on_server_register_req,
    [SSCmdID.NID_SERVER_SUBSCRIBE_REQ]  = Gateway.on_server_subscribe_req,
}
-- cmd消息
function Gateway:on_session_cmd(session, cmd_id, body, session_id)
    if cmd_route_table[cmd_id] then
        cmd_route_table[cmd_id](self, session, body, session_id)
        return
    end
    local area_id = session.area_id or body.area_id
    local platform = self:find_session_platform(session, area_id)
    if platform <= 0 then
        log_err("[Gateway][on_session_cmd] cmdid %s area_id %s has no platform", cmd_id, area_id)
        server_mgr:callback_dx(session, cmd_id + 1, {code = KernCode.RPC_FAILED}, session_id)
        return
    end
    local player_id = session.player_id
    local trans_data = tdeep_copy(body)
    if session_id > 0 or player_id then
        --注：客户端未使用rpc方式调用，这里需要添加player_id的判断
        local ok, cmd_res = router_mgr:call_target(platform, "rpc_forward_message", session.player_id, cmd_id, trans_data)
        local call_ok, call_err_or_data = tunpack(cmd_res)
        local res = (ok and call_ok) and call_err_or_data or {code = KernCode.RPC_FAILED}
        if player_id then
            log_debug("[Gateway][on_client_cmd] send res: player_id: %s, cmdid: %s, data: %s", player_id, cmd_id + 1, serialize(res))
            client_mgr:callback_dx(session, cmd_id + 1, res, session_id)
        else
            log_debug("[Gateway][on_server_cmd] send res: cmdid: %s, data: %s", cmd_id + 1, serialize(res))
            server_mgr:callback_dx(session, cmd_id + 1, res, session_id)
        end
    else
        router_mgr:send_target(platform, "rpc_forward_message", player_id, cmd_id, trans_data)
    end
end

function Gateway:on_session_sync(session)
    local player_id = session.player_id
    local platform = self:find_session_platform(session)
    if platform > 0 and player_id then
        router_mgr:call_target(platform, "rpc_client_sync", player_id)
    end
end

-- 客户端关闭
function Gateway:on_session_err(session)
    local player_id = session.player_id
    if player_id then
        local platform = self:find_session_platform(session)
        if platform and self.clients[player_id] then
            local area_id = session.area_id
            self.clients[player_id] = nil
            self.index_clients[area_id][player_id] = nil
            router_mgr:send_target(platform, "rpc_client_close", player_id)
            log_info("[Gateway][on_session_err] client %s closed", player_id)
        end
    end
    local node_id = session.node_id
    if node_id then
        self.servers[node_id] = nil
    end
end

--获取一个节点
function Gateway:find_platform(area_id)
    if area_id then
        return self.index_platforms[area_id] or 0
    end
    return 0
end

function Gateway:find_session_platform(session, area_id)
    return self:find_platform(area_id or session.area_id)
end

-- 踢掉客户端
function Gateway:rpc_client_kickout(player_id)
    local session = self.clients[player_id]
    if session then
        log_info("[Gateway][rpc_client_kickout] kick session: player_id:%s!", session.player_id)
        session.area_id = nil
        session.player_id = nil
        client_mgr:close_session(session)
    end
end

--广播信息
function Gateway:rpc_board_message(area_id, cmd_id, data)
    if area_id == 0 then
        --全服广播
        for _, players in pairs(self.index_clients) do
            for _, session in pairs(players) do
                client_mgr:send_dx(session, cmd_id, data)
            end
        end
        return
    end
    --分区广播
    local players = self.index_clients[area_id]
    for _, session in pairs(players or {}) do
        client_mgr:send_dx(session, cmd_id, data)
    end
end

--群发信息
function Gateway:rpc_group_message(players, cmd_id, data)
    log_debug("[Gateway][rpc_group_message] players: %s, cmdid: %s, data: %s", serialize(players), cmd_id, serialize(data))
    for _, player_id in pairs(players) do
        self:rpc_trans_message(player_id, cmd_id, data)
    end
end

--发送信息
function Gateway:rpc_trans_message(player_id, cmd_id, data)
    local session = self.clients[player_id]
    if session then
        log_debug("[Gateway][rpc_trans_message] player_id: %s, cmdid: %s, data: %s", player_id, cmd_id, serialize(data))
        client_mgr:send_dx(session, cmd_id, data)
    end
end

--节点查询
function Gateway:rpc_platform_query(area_id)
    return self.index_platforms[area_id]
end

--订阅发布
function Gateway:rpc_platform_subscribe(node_id, cmd_id, data)
    local session = self.servers[node_id]
    if session then
        server_mgr:send_dx(session, cmd_id, data)
    end
end

--节点上报
function Gateway:rpc_platform_register(area_id, quanta_id)
    log_info("[Gateway][rpc_platform_register] platform %s (area_id:%s) register", quanta_id, area_id)
    self.platforms[quanta_id] = area_id
    self.index_platforms[area_id] = quanta_id
    return SUCCESS
end

--上报节点
function Gateway:on_service_ready(quanta_id)
    local host_ip = env_get("QUANTA_HOST_IP")
    local caddr = sformat("%s:%s", host_ip, client_mgr:get_port())
    local saddr = sformat("%s:%s", host_ip, server_mgr:get_port())
    thread_mgr:success_call(PeriodTime.SECOND_MS, function()
        local ok, code = router_mgr:call_target(quanta_id, "rpc_gateway_report", quanta.id, caddr, saddr)
        if ok and check_success(code) then
            log_info("[DataProxy][on_service_ready] gateway_report success!")
            return true
        end
        log_err("[DataProxy][on_service_ready] gateway_report failed!")
        return false
    end)
end

--节点断开
function Gateway:on_service_close(quanta_id)
    local area_id = self.platforms[quanta_id]
    if area_id then
        log_info("[Gateway][on_server_close] platform %s(area_id:%d) close", quanta_id, area_id)
        self.index_platforms[area_id] = nil
        self.platforms[quanta_id] = nil
    end
end

quanta.gateway = Gateway()

return Gateway
