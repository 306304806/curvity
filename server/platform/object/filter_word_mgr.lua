--fword_mgr.lua
-- 敏感词管理
local log_info      = logger.info
local tinsert       = table.insert
local schars        = string_ext.chars

local FilterMgr = singleton()
local prop = property(FilterMgr)
prop:accessor("dict_tree_root", {})  -- 字典树根节点

----------------------------------------------------------------------------
-- 加载敏感词配置
local load_filter_cfg = function(file_name)
    local words     = {}
    local file = io.open(file_name)
    if not file then
        return words
    end

    local line = file:read()
    while line do
        if line then
            tinsert(words, line)
        end
        line = file:read()
    end

    return words
end

-- 字典树相关代码
--[[
    -- 字典树节点原型
    dict_tree_node = {
        char        = "",
        is_word     = false,
        parent_node = nil,
        child_map   = nil,
    }
]]

-- 构造一个字典树节点
local build_dict_tree_node = function(key, parent, children)
    local node = {char = key, is_word = false, parent = parent, child_map = {}}
    for _, child in pairs(children or {}) do
        parent.child_map[child.char] = child
    end

    return node
end

-- 从单词列表构建字典树
local build_dict_tree = function(words)
    local root_node = build_dict_tree_node(nil, nil, nil)
    for _, word in pairs(words or {}) do
        local last_node = root_node
        for _, char in pairs(schars(word) or {}) do
            local cur_node = last_node.child_map[char]
            if cur_node then
                last_node = cur_node
            else
                cur_node = build_dict_tree_node(char, last_node)
                last_node.child_map[cur_node.char] = cur_node
                last_node = cur_node
            end
        end
        last_node.is_word = true
    end

    return root_node
end

-- 检测过程中需要skip的单字
local skip_word_map = {
    [" "] = true,
    [","] = true,
}

-- 检查给定字符串是否包含字典树中的单词
local has_word_in_dict_tree = function(dict_root, str)
    local last_node = dict_root
    local chars = schars(str)
    local last_match_index = 0
    local char_cnt = #chars
    local n = 1
    while n <= char_cnt + 1 do
        local char = chars[n]
        local cur_node = last_node.child_map[char]
        if cur_node then  -- 开始一次单词匹配
            if 0 == last_match_index then
                last_match_index = n
            end
            last_node = cur_node

            if last_node.is_word then
                return true
            end
            n = n + 1
        else  -- 当前单词匹配失败
            if 0 ~= last_match_index then
                n = last_match_index + 1
                last_match_index = 0
                last_node = dict_root
            else
                n = n + 1
            end
        end

        -- 检查下一个字符串是否需要直接跳过去
        while skip_word_map[chars[n]] do
            n = n + 1
        end
    end

    return false
end

-- 用给定的字符替换源字符串中存在于字典树中的单词
local transfer_word_in_dict_tree = function(dict_root, str, rword)
    local match_cnt = 0
    local last_node = dict_root
    local chars = schars(str)
    local last_match_index = 0
    local char_cnt = #chars
    local sindexs = {}  -- 敏感词索引
    local n = 1
    while n <= char_cnt + 1 do
        local char = chars[n]
        local cur_node = last_node.child_map[char]
        if cur_node then  -- 开始一次单词匹配
            if 0 == last_match_index then
                last_match_index = n
            end
            tinsert(sindexs, n)
            if cur_node.is_word then
                match_cnt      = match_cnt + 1
                for _, index in pairs(sindexs or {}) do
                    chars[index] = rword
                end
            end
            last_node = cur_node
            n = n + 1
        else  -- 当前单词匹配失败
            if 0 ~= last_match_index then
                n = last_match_index + 1
                last_match_index = 0
                last_node = dict_root
                sindexs = {}
            else
                n = n + 1
            end
        end

        -- 检查下一个字符串是否需要直接跳过去
        while skip_word_map[chars[n]] do
            n = n + 1
        end
    end
    local result = table.concat(chars);

    return match_cnt, result
end
----------------------------------------------------------------------------
function FilterMgr:__init()
    local words = load_filter_cfg("../server/config/filter_word.txt")
    log_info("[FilterMgr][setup] filter word load: cnt=%s", #words)
    self.dict_tree_root = build_dict_tree(words)
end

function FilterMgr:check(str)
    return has_word_in_dict_tree(self.dict_tree_root, str)
end

function FilterMgr:trans(str, rword)
    return transfer_word_in_dict_tree(self.dict_tree_root, str, rword)
end


-- export
quanta.fword_mgr = FilterMgr()

return FilterMgr